#!/usr/bin/python

import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)
        
class TreeNode:
    def __init__(self, val, left = None, right = None):
        self.val = val
        self.left = left
        self.right = right
        self.total = -1

    def __repr__(self):
        return f"({self.val}:{self.sum}, {self.left}, {self.right})"

# Samllest difference from splitting an edge
def split_tree_min(root: TreeNode) -> int:

    def dfs_total(n):
        if n is None:
            return 0
        t = n.val
        t += dfs_total(n.left)
        t += dfs_total(n.right)
        n.total = t
        return t

    dfs_total(root)
    
    m = []
    
    def dfs_delta(n, other):
        if n is None:
            return
        
        d = abs(n.total - other)
        log(f"d {d} m {m}")
        if not m:
            m.append(d)
        elif d < m[0]:
            m[0] = d

        rt = n.right.total if n.right else 0
        assert rt != -1
        lt = n.left.total if n.left else 0
        assert lt != -1
        dfs_delta(n.left, other + n.val + rt)
        dfs_delta(n.right, other + n.val + lt)
            
    dfs_delta(root, 0)

    return m[0]

tn = TreeNode

print(split_tree_min(tn(1,tn(6,tn(2),tn(5)),tn(7,tn(3),tn(4))))) # 0
print(split_tree_min(tn(4,tn(-4),tn(1)))) # 1
print(split_tree_min(tn(3,tn(14,tn(-4),tn(5)), tn(7, tn(6), tn(9))))) # 4
