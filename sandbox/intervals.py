#!/usr/bin/python

from interval import *

class Intervals:
    """
   Intervals default to merge=True
    >>> ivls = Intervals()
    >>> ivls
    Intervals(None)
    
    >>> ivls.add(Interval(0, 3))
    Intervals(Interval(0, 3))

    >>> ivls.intervals
    [Interval(0, 3)]

    >>> ivls.add(Interval(7, 10))
    Intervals(Interval(0, 10))

    >>> ivls.intervals
    [Interval(0, 3), Interval(7, 10)]

    >>> ivls.add(Interval(-3, 13))
    Intervals(Interval(-3, 13))

    >>> ivls.intervals
    [Interval(-3, 13)]

    >>> ivls = Intervals()
    >>> ivls.add(Interval(0, 3))
    Intervals(Interval(0, 3))

    >>> ivls.add(Interval(5, 7))
    Intervals(Interval(0, 7))

    >>> ivls.intervals
    [Interval(0, 3), Interval(5, 7)]

    >>> ivls.add(Interval(4, 6))
    Intervals(Interval(0, 7))

    >>> ivls.intervals
    [Interval(0, 7)]

    >>> ivls.add(Interval(4, 10))
    Intervals(Interval(0, 10))

    >>> ivls.intervals
    [Interval(0, 10)]

    >>> ivls.add(Interval(2, 3))
    Intervals(Interval(0, 10))

    >>> ivls.intervals
    [Interval(0, 10)]

    >>> ivls = Intervals()
    >>> ivls.add(Interval(1, 3))
    Intervals(Interval(1, 3))
    >>> ivls.add(Interval(6, 9))
    Intervals(Interval(1, 9))
    >>> ivls.add(Interval(2, 5))
    Intervals(Interval(1, 9))
    >>> ivls.intervals
    [Interval(1, 5), Interval(6, 9)]
    """
    
    def __init__(self, merge = True):
        """If merge, then combine together if possible
         Not merge makes more sense when each Interval has a value
        """
        self.intervals = []
        self.merge = merge

    def __repr__(self):
        return f"Intervals({self.range()})"
    
    def range(self):
        if not self.intervals:
            return None

        return Interval(self.intervals[0].s, self.intervals[-1].e)

    # Can add with union or intersection (ie not union)
    def add(self, interval):
        if not self.intervals:
            self.intervals.append(interval)
        else:
            from bisect import bisect_left

            i = bisect_left(self.intervals, interval.s, key = lambda x: x.s)

            # Backup and check previous if possible...
            # Keep track, since even if fail to union with previous interval,
            # we still continue...
            backed_up = i - 1 >= 0
            if backed_up:
                i -= 1

            while i < len(self.intervals):
                curr = self.intervals[i]
                log(f"i {i} curr {curr} interval {interval}")
                union = curr.union(interval)
                if union:
                    assert(self.merge) # Not handling other case yet!
                    self.intervals.pop(i)
                    interval = union
                    log(f"interval {interval} intervals {self.intervals}")
                else:
                    i += 1
                    log(f"not intersects")
                    if backed_up:
                        backed_up = False
                    else:
                        break

            # If ended, or ran out, will have interval left...
            if interval:
                self.intervals.insert(i, interval)
                
        return self
        
if __name__ == "__main__":
    import doctest
    doctest.testmod()
