#!/usr/bin/python
import sys

for i, line in enumerate(sys.stdin):
    s = line.rstrip()
    print(f"Found {s} at line {i}")
