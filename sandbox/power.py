#!/usr/bin/python

import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)

def power(boot: list[int], proc: list[int], total: int) -> int:
    """
    >>> power([203,105,530,3567,1092,3], [992,1453,854,152,2377,4], 15)
    1

    The following verifies the BUG in original submission, discussed below
    >>> power([2] * 2 + [1000] + [2] * 3, [2] * 6, 20)
    3
    """
    log(f"boot {boot} proc {proc} total {total}")
    
    l = len(boot)
    assert l == len(proc)
    
    b, e = 0, 0
    mk = 0
    s = proc[0]

    def ms(b, e):
        return max(boot[b:e+1]), sum(proc[b:e+1])
    
    while e < l:
        k = e - b + 1
        #p = max(boot[b:e+1]) + (sum(proc[b:e+1]) * k)
        
        #p = max(boot[b:e+1]) + (s * k)
        m, s = ms(b, e)
        p = m + (s * k)
        assert s == sum(proc[b:e+1])

        log(f"b {b} e {e} k {k} m {m} s {s} p {p}")
        
        # BUG was the following commented line *sigh* ...
        # if p <= total and k > mk:
        # ...instead of now currently in place....
        # if p <= total: if k > mk:
        # 
        # So, what's the difference...
        # if not k > mk, but p <= total, then we increment beginning, possibly missing out out a window
        # m 2, s = 4, k = 2
        # Here's an example of trailing 3 2s that will be skipped, compared to initial 2 2s...
        # [2, 2, 1000, 2, 2, 2], [2] * 6, 20 should be k = 3, but BUG gives k = 2
        if p <= total:
            if k > mk:
                mk = k
            e += 1
            if e < l:
                s += proc[e]
        else:
            s -= proc[b]
            b += 1
            if b < l:
                s += proc[b]
                
            #  Should back up e?  I don't think so...
            if e < b:
                e = b
                
    return mk

if __name__ == "__main__":
    import doctest
    doctest.testmod()
