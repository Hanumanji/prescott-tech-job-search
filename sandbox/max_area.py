from typing import List

def maxArea(height: List[int]) -> int:
    i = 0
    j = len(height) - 1
    
    n = height[i]
    m = height[j]
    
    a = min(n, m) * abs(j - i)

    for x in range(1, len(height)):
        hx = height[x]
        print(f"x {x}, i {i}, j {j}, hx {hx}, n {n}, m {m}, a {a}")
        
        if hx > min(n, m):
            if m > n:
                n = hx
                i = x
                
                # Replace n i
                p = min(m, hx) * abs(x - j)
                if p > a:
                    a = p
            else:
                m = hx
                j = x
                
                # Replace m j
                p = min(n, hx) * abs(x - i)
                if p > a:
                    a = p
        
    return a

print(maxArea([2,3,4,5,18,17,6]))
