#!/usr/bin/python

"""

>>> add([[1, 5]], [0, 3])
[[0, 5]]

>>> add([[1,3],[6,9]], [2,5])
[[1, 5], [6, 9]]

>>> add([[1, 5]], [6, 8])
[[1, 5], [6, 8]]

>>> add([[1, 5]], [0, 0])
[[0, 0], [1, 5]]

>>> add([[2,4],[5,7],[8,10],[11,13]], [3,6])
[[2, 7], [8, 10], [11, 13]]
"""

import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)

def inside(self, v):
    ss, se = self
    return v >= ss and v <= se

def intersects(self, other):
    ss, se = self
    os, oe = other
    return (inside(self, os) or inside(self, oe) or
            inside(other, ss) or inside(other, se))

def union(self, other, touching = False):
    if not intersects(self, other):
        if not touching:
            return None
    
        # but they might be right next to each other...

        mi, mx = sorted([self, other])

        _, mie = mi
        mxs, _ = mx
        
        log(f"union not intersect mi {mi} mx {mx}")
        if mie + 1 != mxs:
            return None

    ss, se = self
    os, oe = other
        
    return [min(ss, os), max(se, oe)]

def add(intervals, interval):
    from bisect import bisect_left

    i = bisect_left(intervals, interval)
    log(f"bisect i {i}")
    
    # Backup and check previous if possible...
    # Keep track, since even if fail to union with previous interval,
    # we still continue...
    j = i
    backed_up = j - 1 >= 0
    if backed_up:
        log(f"backed up")
        j -= 1

    while j < len(intervals):
        curr = intervals[j]
        log(f"j {j} curr {curr} interval {interval}")
        u = union(curr, interval)
        if u:
            intervals.pop(j)
            interval = u
            i = j
            log(f"union interval {interval} remaining intervals {intervals}")
        else:
            log(f"no union")
            if backed_up:
                backed_up = False
            else:
                log("break")
                break
            j += 1

    # If ended, or ran out, will have interval left...
    if interval:
        log(f"insert i {i} interval {interval}")            
        intervals.insert(i, interval)
 
    return intervals
        
if __name__ == "__main__":
    import doctest
    doctest.testmod()
