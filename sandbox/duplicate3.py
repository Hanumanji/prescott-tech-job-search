#!/usr/bin/python3.10

from bisect import bisect_left, insort
from typing import List

inputs = [
        ([1, 2, 3, 1], 3, 0),
        ([1, 0, 1, 1], 1, 2),
        ([1, 5, 9, 1, 5, 9], 2, 3),
        ([1, 2, 1, 1], 1, 0)
    ]

def duplicate3(nums: List[int], k: int, t: int) -> bool:
        print(nums, k, t)
        key = lambda v: v[0]
        h = [] 
        for i, n in enumerate(nums):
            s = bisect_left(h, n - t, key = key)
            e = bisect_left(h, n + t, key = key)
            print(h, s, e)
            for j in range(s, e + 1):
                print("index", j)
                if j >= len(h):
                    continue
                v, indices = h[j]
                print("entry", j, v, indices)
                if abs(n - v) <= t:
                    for x in indices:
                        if abs(i - x) <= k:
                            return True
                        
        # Now just need to insert everything properly...*grrr*  
            added = False
            j = bisect_left(h, n, key = key)
            if j < len(h):
                v, indices = h[j]
                if v == n:
                # Add to the indices
                    indices.add(i)
                    added = True
                    
            if not added:
                insort(h, (n, set((i,))), key = key)
        
        return False

if __name__ == "__main__":
#    for input in inputs:
#        nums, k, t = input
#        print(nums, k, t, duplicate3(nums, k, t))
#
        nums, k, t = inputs[3]
        print(nums, k, t, duplicate3(nums, k, t))        
