#!/usr/bin/python

import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)
  
class Interval:
    """
    >>> i = Interval(3, -3)
    >>> i
    Interval(-3, 3)
    
    >>> i.inside(0)
    True
    >>> i.inside(3)
    True
    >>> i.inside(-3)
    True
    >>> i.inside(100)
    False
    
    >>> k = Interval(10, 20)
    >>> k.intersects(i)
    False
    
    >>> k.intersection(i)
    
    >>> i.intersects(k)
    False
    
    >>> j = Interval(6, 0)
    >>> j
    Interval(0, 6)
    
    >>> j.sorted(i)
    [Interval(-3, 3), Interval(0, 6)]
    
    >>> i.intersects(j)
    True
    
    >>> j.intersects(i)
    True
    
    >>> i.intersection(j)
    Interval(0, 3)
    
    >>> i.difference(j)
    [Interval(-3, -1), Interval(4, 6)]
    
    >>> i.difference(j, True)
    [Interval(-3, -1), Interval(0, 3), Interval(4, 6)]
    
    >>> i
    Interval(-3, 3)
    
    >>> i.difference(Interval(0, 3))
    [Interval(-3, -1)]
    
    >>> i.difference(i)
    []
    
    >>> i.union(i)
    Interval(-3, 3)

    >>> j
    Interval(0, 6)

    >>> i.union(j)
    Interval(-3, 6)

    >>> k
    Interval(10, 20)

    >>> k.union(i)

    >>> k.contains(i)
    False

    >>> k.contains(Interval(10, 10))
    True

    >>> k.contains(Interval(15, 25))
    False

    >>> k.intersects(Interval(15, 25))
    True

    >>> Interval(0, 3).union(Interval(4, 6))
    Interval(0, 6)

    """
        
    def __init__(self, s, e):
        self.s, self.e = min(s,e), max(s,e)

    def __repr__(self):
        return f"Interval({self.s}, {self.e})"
    
    def inside(self, v):
        return v >= self.s and v <= self.e

    def contains(self, other):
        return self.inside(other.s) and self.inside(other.e)
    
    def intersects(self, other):
        return (self.inside(other.s) or self.inside(other.e) or
                other.inside(self.s) or other.inside(self.e))

    def sorted(self, other):
        return sorted([self, other], key=lambda i: i.s)
    
    def intersection(self, other):
        if not self.intersects(other):
            return None
        
        return Interval(max(self.s, other.s), min(self.e, other.e))

    def differs(self, other, withIntersection = False):
        "Returns (None, None, None, None, None) if no difference, otherwise tuple of bools (b, i, a, mi, mx) used for determining which of before, intersection, after the difference will have"
        i = self.intersection(other)
        if not i:
            return (None, None, None, None, None)
        
        mi, mx = self.sorted(other)

        return (mi.s != i.s, i, mx.e != i.e, mi, mx)
            
    def difference(self, other, withIntersection = False):
        d = []
        b, i, a, mi, mx = self.differs(other, withIntersection)
        if b is None:
            return d
        
        if b:
            d.append(Interval(mi.s, i.s - 1))
            
        if withIntersection:
            d.append(i)

        if a:
            d.append(Interval(i.e + 1, mx.e))
            
        return d

    def union(self, other, touching = True):
        if not self.intersects(other):
            if not touching:
                return None
            
            # but they might be right next to each other...
            mi, mx = self.sorted(other)
            log(f"union not intersect mi {mi} mx {mx}")
            if mi.e + 1 != mx.s:
                return None

        return Interval(min(self.s, other.s), max(self.e, other.e))
        
if __name__ == "__main__":
    import doctest
    doctest.testmod()
