def long_cons(nums):
    h = {}
    
    for n in nums:
        print(n)
        p = n + 1
        m = n - 1

        pr = mr = None

        if n in h:
            continue

        if p in h:
            pr = h[p]

        if m in h:
            mr = h[m]

        if pr and mr:
            ps, pe = pr
            del h[ps]
            if pe != ps:
                del h[pe]

            ms, me = mr
            if ms in h:
                del h[ms]
            if me != ms and me in h:
                del h[me]

            ms = min(ms, ps)
            pe = max(pe, me) 
            r = ms, pe
            h[ms] = r
            h[pe] = r
        elif pr:
            ps, pe = pr
            del h[ps]
            if pe != ps:
                del h[pe]

            if n < ps:
                pr = n, pe
                h[n] = pr
            else:
                h[ps] = pr
            h[pe] = pr
            print(n, pr)
        elif mr:
            ms, me = mr
            del h[ms]
            if me != ms:
                del h[me]

            if n > me:
                mr = ms, n
                h[n] = mr
            else:
                h[me] = mr
            h[ms] = mr

            print(n, mr)
        else:
            h[n] = [n, n]

        print(h)

    m = 0
    for r in h.values():
        s, e = r
        l = (e - s) + 1
        if l > m:
            m = l
    return m



#l = [-3,2,8,5,1,7,-8,2,-8,-4,-1,6,-6,9,6,0,-7,4,5,-4,8,2,0,-2,-6,9,-4,-1]
#print(l)
#l.sort()
#print(l)
#print(long_cons([0,3,7,2,5,8,4,6,0,1]))
inputs = [
#    [100, 4, 200, 1, 3, 2],
#    [2, 1, 5, 4, 3],
#    [0,3,7,2,5,8,4,6,0,1],
#    [1,2,0,1],
#    [-3,2,8,5,1,7,-8,2,-8,-4,-1,6,-6,9,6,0,-7,4,5,-4,8,2,0,-2,-6,9,-4,-1],
#    [-9,-4,9,-7,0,7,3,-1,6,2,-2,8,-2,0,2,-7,-5,-2,6,-5,0,-8,8,1,0,6,8,-8,-1],
    [-5,-5,-4,8,3,1,-4,-6,-7,4,8,-1,8,-3,-8,-9,-7,-3,2,-8,5,-1,3,-8,-6],
    ]
for input in inputs:
    print(long_cons(input))
    print(input)
    input.sort()
    print(input)
