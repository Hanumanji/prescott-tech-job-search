
def prod(arr):
    l = len(arr)
    prefix = [1] * l
    suffix = [1] * l
    product = [1] * l

#    print("array", arr)
    
    for i in range(1, l):
        prefix[i] = prefix[i-1] * arr[i-1]

#    print("prefix", prefix)
    
    for i in range(l-2, -1, -1):
        suffix[i] = suffix[i+1] * arr[i+1]

#    print("suffix", suffix)
    
    for i in range(l):
        product[i] = prefix[i] * suffix[i]

    return product


if __name__ == "__main__":
    inputs = (
        [1,2,3,4],
        [-1,1,0,-3,3]
        )
    for input in inputs:
        print(prod(input))

        

    
