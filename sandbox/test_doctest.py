#!/usr/bin/python

"""
>>> 23
23
"""

class Mine:
    """
    >>> "duck"
    'duck'
    """
    
class Other:
    """
    >>> "cow"
    'cow'
    """

"""
>>> this will do nothiner...only one test string allowed, apparently...
"""

if __name__ == "__main__":
    import doctest
    doctest.testmod()
