#!/usr/bin/perl

use strict 'vars';

sub isBalanced {
    my ($s) = @_;
    my $l = [];
    my $h = { # open => close
	'[' => ']',
	    '(' => ')',
	    '{' => '}',
    };

    my $r = "NO"; # Default, or YES if balanced

    foreach my $c (split //, $s){
	print "in ", $c, "\n";
	my $close = $$h{$c};
	if ($close){
	    # Found a close, so this is an open, so just push
	    push(@$l, $c);
	    print "l ", @$l, "\n";
	} else {
	    # This is close, test if matches open...
	    my $e = $$l[-1];
	    $close = $$h{$e};
	    
	    if ($close != $c){
		print("e ", $e, " close ", $close, " c ", $c);
		return "NO";
	    } else {
		pop(@$l);
	    }

	}
	print $c, " l ", @$l, "\n";
    }
    print "l ", @$l, " length ", $#$l, "\n";
    return (scalar @$l ? "NO" : "YES");
}

print(isBalanced('[[]]'));
print("\nShould be YES...\n");
print(isBalanced('[[}]]'));
print("\nShould be NO...\n");
