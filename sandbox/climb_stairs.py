#!/usr/bin/python3

from typing import List
from dataclasses import dataclass
from traceback import print_exception

from math import factorial


@dataclass
class Logger(object):
    scope: str
    debug: bool = False
    
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, tb):
        if exc_type is not None:
            print_exception(exc_type, exc_value, tb)
        return
    
    def log(self, *args):
        if self.debug:
            print(self.scope, *args)
    
class Solution:
    def preprocess(self, input):
        return input

    def postprocess(self, output):
        return output
    
    def climbStairs(self, n: int) -> int:
        n1, n2 = 1, 0
        for _ in range(n):
            n1, n2 = n1 + n2, n1
        return n1
    
#    def climbStairs(self, n: int) -> int:
#        with Logger("climbStairs") as logger:
#            logger.log(f"n {n}")
#
#            p = 1 # Permutations for all 1s...
#
#            # Now iterate from one 2, to all but one 2s...
#            for i in range(n // 2):
#                ones = n - ((i + 1) * 2)
#                twos = i + 1
#                logger.log(f"ones {ones} twos {twos}")
#                
#                m = ones + twos
#                r = min(ones, twos)
#                mCr = factorial(m)//(factorial(r)*factorial(m - r))
#                p += mCr
#                logger.log(f"i {i} m {m} r {r} mCr {mCr} p {p}")
#
#            return p
        
    # (2,2,1,1,1)
    # (2,?...) = 4
    # (1,2,?...) = 3
    # (1,1,?...) = 3
    # 5P2 = 5!/(5-2)! = 5*4*3*2/
    # 5C2 = 5!/2!(5-2)!= 5*4*3*2/2*3*2=10
    #n!/r!(n-r)! = 6 4*3*2/2(2) = 6 4C2
    # 5C2 = 5*4*3*2/2*3*2 = 10
    # 1  1^1
    # 2  1^2, 2^1
    # 3  1^3, P(1, 2)=2C1=2
    # 4  1^4, 2^2, P(1^2, 2)=3C2
    # 5  1^5, P(2^2, 1) = 3, P(1^3, 2)=4
    # 6  1^6, 2^3, P(2^2,1^2)=6, P(2,1^4)=5
    # 7  1^7, P(2^3,1)=4=4C1, P(2^2,1^3)=5C2=10,P(2,1^5)=6
    # ...
    # 9  1^9, P(2^4,1)=5C1=5, P(2^3,1^3)=6C3=6!/3!(6-3)!=6*5*4*3*2/3*2*3*2=6*5*4/3*2=5*4=20
    #      P(2^2,1^5)=7C2=7!/2!(7-2)!=7*6/2=21
    #      P(2,1^6)=6C1
    # 3P2 = 3!/(3 - 2)! = 6  (1, 2, 2), (2, 1, 2), (2, 2, 1)
    #
    # 1 + for i in n/2: m = n - (i*2), mC2= m!/2!(m-2)!

if __name__ == "__main__":
    soln = Solution()
    fn_name = "climbStairs"
    assert fn_name, "Set fn_name to valid Solution method"
    fn = getattr(soln, fn_name)
    assert fn, f"No Solution method attr named {fn_name}"
    assert callable(fn), f"Solution attr {fn_name} is not callable"
    expects = [
        ((2,), 2),
        ((3,), 3),
        ((4,), 5),
        ((5,), 8),
    ]
    assert expects, "expects is empty list of (input, output) pairs, please set!"
    for input, output in expects:
        pinput = soln.preprocess(input)
        actual = fn(*pinput)
        pactual = soln.postprocess(actual)
        print("SUCCESS" if pactual == output else "FAILURE", input, "gave", pactual, "expected", output)
       
    
