#!/usr/bin/python3

from typing import List

class Solution:
    def debug(self, *args):
        # print(*args)
        pass


    def search(self, nums: List[int], target: int) -> int:
        return self.binsearch(target, nums, 0, len(nums) - 1)

    def binsearch(self, t, nums, s, e):
        self.debug("binsearch", t, s, e)
        if s == e:
            return s if nums[s] == t else -1

        m = (s + e) // 2
        n = nums[m]
        
        if t == n:
            return m
        elif t < n:
            return self.binsearch(t, nums, s, m)
        else:
            return self.binsearch(t, nums, m + 1, e)

if __name__ == "__main__":
   expects = [
       (([0, 1, 2, 4,5,6,7,], 0), 0),
       (([0, 1, 2, 4,5,6,7,], 5), 4),       
       (([1,3], 3), 1),
        ]
   for input, output in expects:
       a = Solution().search(*input)
       print(*input, "gave", a, "expected", output)
       
    
