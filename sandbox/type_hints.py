def check(number: str) -> bool:
    def digits_of(nr: str) -> list[int]:
        return [int(d) for d in nr]


if __name__ == "__main__":
    print(check(27))
