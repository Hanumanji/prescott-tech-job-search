#!/usr/bin/python



def count_swaps(arr):
    vis = [(v, i) for i, v in enumerate(arr)]
    vis.sort()
    j = 0
    l = len(vis)
    swaps = 0
    while j < l:
        v, i = vis[j]
        if i == j:
            j += 1
        else:
            swaps += 1
            vis[j], vis[i] = vis[i], vis[j]
    return swaps

arr = [1, 5, 4, 3, 2]
print(arr)
print(count_swaps(arr))

r = list(reversed(arr))
print(r)
print(count_swaps(r))
