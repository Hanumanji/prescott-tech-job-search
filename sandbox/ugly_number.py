#!/usr/bin/python3

from typing import List
from dataclasses import dataclass
import traceback

@dataclass
class Logger(object):
    scope: str
    debug: bool = False
    
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, tb):
        if exc_type is not None:
            traceback.print_exception(exc_type, exc_value, tb)
        return

    def log(self, *args):
        if self.debug:
            print(*args)
    
class Solution:
    def preprocess(self, input):
        return input

    def postprocess(self, output):
        return output
    

    def getNthUglyNumber(self, n: int) -> int:
        assert(n > 0)

        with Logger("getNthUglyNumber") as logger:
            logger.log("starting...")

            ugly = [0] * n
            ugly[0] = 1
            i2 = i3 = i5 = 0
            peeks = (ugly[i2] * 2, ugly[i3] * 3, ugly[i5] * 5)
            
            # Peeks...
            for i in range(1, n):
                peeks = (ugly[i2] * 2, ugly[i3] * 3, ugly[i5] * 5)
                
                m = min(peeks)
                logger.log(f"Found {i} ugly number value {m}")

                ugly[i] = m
                
                if m == ugly[i2] * 2:
                    i2 += 1
                    
                if m == ugly[i3] * 3:
                    i3 += 1
                    
                if m == ugly[i5] * 5:
                    i5 += 1

            return ugly[n - 1]
    
if __name__ == "__main__":
    soln = Solution()
    fn_name = "getNthUglyNumber"
    assert fn_name, "Set fn_name to valid Solution method"
    fn = getattr(soln, fn_name)
    assert fn, f"No Solution method attr named {fn_name}"
    assert callable(fn), f"Solution attr {fn_name} is not callable"
    expects = [
        ((1,), 1),
        ((2,), 2),
        ((11,), 15),
        ((150,), 5832),
    ]
    assert expects, "expects is empty list of (input, output) pairs, please set!"
    for input, output in expects:
        pinput = soln.preprocess(input)
        actual = fn(*pinput)
        pactual = soln.postprocess(actual)
        print("SUCCESS" if pactual == output else "FAILURE", input, "gave", pactual, "expected", output)
       
    
