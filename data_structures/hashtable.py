#!/usr/bin/python

class HT:
    """
    >>> h = HT()
    >>> h
    HT{}

    >>> h["duck"] = 27
    >>> h
    HT{('duck', 27)}

    >>> h["cat"] = -3
    >>> h
    HT{('cat', -3), ('duck', 27)}

    >>> del h["duck"]
    >>> h
    HT{('cat', -3)}

    >>> del h["zombie"]
    Traceback (most recent call last):
      ...
    KeyError: 'zombie'
    """
    def __init__(self, count = 10):
        self.buckets = [[] for _ in range(count)]

    def __repr__(self):
        # sorted() just deals with hash() nondeterminism for doctests
        return "HT{" + ", ".join(sorted(str(pair) for b in self.buckets for pair in b)) + "}"
    
    def __getitem__(self, key):
        i = hash(key) % len(self.buckets)
        for pk, pv in self.buckets[i]:
            if pk == key:
                return pv
            else:
                raise KeyError(key)

    def __setitem__(self, key, value):
        i = hash(key) % len(self.buckets)
        for j, (pk, pv) in enumerate(self.buckets[i]):
            if pk == key:
                self.buckets[i][j] = (key, value)
                return
        self.buckets[i].append((key, value))

    def __delitem__(self, key):
        i = hash(key) % len(self.buckets)
        d = -1
        for j, (pk, pv) in enumerate(self.buckets[i]):
            if pk == key:
                d = j
                break
        if d == -1:
            raise KeyError(key)
        
        del self.buckets[i][j]
        

if __name__ == "__main__":
    import doctest
    doctest.testmod()
