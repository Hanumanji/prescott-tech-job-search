package main

import (
	"fmt"
	"strings"
)

var (
  WORD_SCORES = map[int64][]string{
    1:  {"A", "E", "I", "O", "U", "L", "N", "R", "S", "T"},
    2:  {"D", "G"},
    3:  {"B", "C", "M", "P"},
    4:  {"F", "H", "V", "W", "Y"},
    5:  {"K"},
    8:  {"J", "X"},
    10: {"Q", "Z"},
  }
	CHAR_SCORES = map[string]int64{}
)

func init() {
	for k, v := range WORD_SCORES {
		for _, c := range v {
			CHAR_SCORES[c] = k
		}
	}
	// fmt.Println(CHAR_SCORES)
}

func main() {
  wordsAndScores := map[string]int64{
    "cabbage":   14,
    "apples":    10,
    "beach":     12,
    "zebra":     16,
    "xylophone": 24,
  }

  for key, value := range wordsAndScores {
    answer := score(key)
    if answer != value {
      panic(fmt.Sprintf("Expected %d got %d instead for word %s", value, answer, key))
    }
  }
}

func score(word string) int64 {
	s := int64(0)
	for _, c := range(word) {
		s += CHAR_SCORES[strings.ToUpper(string(c))]
	}
	return s
}
