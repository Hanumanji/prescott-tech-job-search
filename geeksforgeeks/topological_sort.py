#!/usr/bin/python

def topological_sort(n, el):

    # Directed graph...
    adj = {i:set() for i in range(n)}
    for a, b in el:
        adj[a].add(b)
        
    stack = []
    v = set()  # visited
    
    def dfs(x):
        if x in v:
            return
        
        v.add(x)
        for c in adj[x]:
            dfs(c)
        stack.append(x)

    for i in range(n):
        dfs(i)

    return stack[::-1]
    


el = [(5,2), (5,0), (4,0), (4,1), (2, 3), (3, 1)]
v = 6
print(topological_sort(v, el))
