#!/usr/bin/python

import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)



class Trie:
    def __init__(self):
        self.children = {}
        self.word = None

    def __repr__(self):
        return str(self.children)
    
    def add(self, word: str):
        n = self
        for c in word:
            if c not in n.children:
                n.children[c] = Trie()
            n = n.children[c]
        n.word = word

    def contains(self, word: str) -> bool:
        n = self
        for c in word:
            if c not in n.children:
                return False
            n = n.children[c]
        return n.word == word

    def prefix(self, word: str) -> bool:
        n = self
        for c in word:
            if c not in n.children:
                return False
            n = n.children[c]
        return True
    
def boggle(d: list[str], b: list[list[str]]) -> list[str]:
    """
#    >>> boggle(["cat"], [list("cap"), list("and"), list("tie")])
#    ['cat']
    >>> boggle(["geeks", "for", "quiz", "go"], [list("giz"), list("uek"), list("qse")])
    ['geeks', 'quiz']
    """
    t = Trie()
    for w in d:
        t.add(w)
        assert t.prefix(w)
        assert t.contains(w)
        
    log(f"t {t}")
    
    def dfs(s, r, c, v):
        if (r, c) in v or r < 0 or c < 0 or r >= len(b) or c >= len(b[0]):
            return []
        
        s += b[r][c]
        log(f"s {s} r {r} c {c}")

        if t.contains(s):
            return [s]

        if not t.prefix(s):
            log(f"not prefix {s}")
            return []

        v.add((r, c))
        
#        if s in d:
#            return [s]

        f = [] # found
        f += dfs(s, r + 1, c + 1, v)
        f += dfs(s, r + 1, c + 0, v)
        f += dfs(s, r + 1, c - 1, v)
        f += dfs(s, r + 0, c + 1, v)
        # f += dfs(s, r + 0, c + 0, v)  Same as (r, c)!
        f += dfs(s, r + 0, c - 1, v)        
        f += dfs(s, r - 1, c + 1, v)
        f += dfs(s, r - 1, c + 0, v)
        f += dfs(s, r - 1, c - 1, v)        

        v.remove((r, c))
        
        return f

    f = []
    for r, row in enumerate(b):
        for c, s in enumerate(row):
            f += dfs("", r, c, set())

    return f

if __name__ == "__main__":
    import doctest
    doctest.testmod()
