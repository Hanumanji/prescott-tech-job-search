#!/usr/bin/python

# Shortest path from source to all vertices

# import sys
import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)
        
def dijkstras(src, el):
    adj = {} # { node: {(neighbor,cost),...} }
    ns = set() # node set, just to get count…
    for a, b, c in el:
        a_costs = adj.setdefault(a, dict())
        b_costs = adj.setdefault(b, dict())
        a_costs[b] = c
        b_costs[a] = c
        ns.update((a,b))
    n = len(ns)

    # dists = [sys.maxsize] * n
    dists = {}
    
    spt = set() # shortest path tree set

    def minDist():
        return min((v, i) for i, v in dists.items() if i not in spt)[1]
    
#        cm = sys.maxsize
#        mi = -1
#        for i, d in enumerate(dists):
#            if dists[i] < cm and i not in spt:
#                cm = dists[i]
#                mi = i
#        assert mi != -1
#        return mi

    dists[src] = 0
    while len(spt) < n:
        x = minDist()
        log(f"x {x} d {dists[x]}")
        
        spt.add(x)
        for c, cost in adj[x].items(): 
            # if c not in spt and dists[x] + cost < dists[c]:
            if c not in spt and (c not in dists or dists[x] + cost < dists[c]):
                dists[c] = dists[x] + cost

        log(f"dists {dists}")
        
    return [dists[i] for i in range(n)]
        
# edge list [(a,b,cost), …]
el = [(0,1,4),(0,7,8),(1,7,11),(1,2,8),(7,8,7),(7,6,1),(2,8,2),(8,6,6),
(2,3,7),(2,5,4),(6,5,2),(3,5,14),(3,4,9),(5,4,10)]

print(dijkstras(0, el))
