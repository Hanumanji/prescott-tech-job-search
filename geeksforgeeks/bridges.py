#!/usr/bin/python

import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)

def bridges(n: int, el: list[list[int]]) -> list[list[int]]:
    adj = {i:set() for i in range(n)}
    for a, b in el:
        adj[a].add(b)
        adj[b].add(a)

    log(f"adj {adj}")
    
    visited = set()
    parent = {}
    discovery = {}
    low = {}

    step = 0
    
    b = []
    
    #def bridge(u, visited, parent, discovery, low, step):
    def bridge(u, step):
        if u in visited:
            return
        
        visited.add(u)

        discovery[u] = step
        low[u] = step
        
#        step += 1

        for v in adj[u]:
            if v not in visited:
                parent[v] = u
                #bridge(v, visited, parent, discovery, low, step + 1)
                bridge(v, step + 1)

                # Check if subtree with v has connection to ancestors of u
                log(f"update u {u} low[u] {low[u]} from v {v} low[v] {low[v]}")                
                low[u] = min(low[u], low[v])

                if low[v] > discovery[u]:
                    log(f"lowest vertex under v {v} is below u {u} in DFS tree"
                    b.append((u, v))
                    
            elif u not in parent or v != parent[u]:
                # Update low value of u for parent function call...
                log(f"update u {u} low[u] {low[u]} from v {v} discovery[v] {discovery[v]}")
                low[u] = min(low[u], discovery[v])
                    
    for i in range(n):
        #bridge(i, visited, parent, discovery, low, step)
        bridge(i, step)

    return b


#print(bridges(5, [(1, 0), (0, 2), (2, 1), (0, 3), (3, 4)])) # [(3, 4), (0, 3)]
print(bridges(4, [(0, 1), (1, 2), (2, 3)])) # [(2, 3), (1, 2), (0, 1)]
#print(bridges(7, [(0, 1), (1, 2), (2, 0), (1, 3), (1, 4), (1, 6), (3, 5), (4, 5)]) # [(1, 6)]
