#!/usr/bin/python

import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)
        
class node:
    def __init__(self):
        self.data = None
        self.next = None
    def __repr__(self):
        return f"{self.data}({self.next})"

def make(arr):
    h = node()
    h.data = arr.pop(0)

    n = h
    for a in arr:
        n.next = node()
        n.next.data = a
        n = n.next

    return h

def max_palindrome(head: node) -> int:
    if not head:
        return 0

    v = [head.data]
    
    pc, pi = 1, 0 # palindrome count, index
    
    mpc = pc # max palindrome count
    
    n = head.next
    i = 1
    while n:
        d = n.data

        if pc > 1 and d == v[pi - 1]:
            pi -= 1
            pc += 2
        elif d == v[-1]:
            pc = 2
            pi = i - 1
        elif len(v) > 1 and d == v[-2]:
            pc = 3
            pi = i - 2
        else:
            pc = 1
            pi = i

        if pc > mpc:
            mpc = pc

        log(f"d {d} pi {pi} pc {pc} mpc {mpc}")
        
        v.append(n.data)

        n = n.next
        i += 1
        
    return mpc

print(max_palindrome(make([2,3,7,3,2,12,24]))) # 5
print(max_palindrome(make([2,3,7,3,2,12,24,1,9,2,8,2,9,1]))) # 7
print(max_palindrome(make([2,3,3,7,2,12,24]))) # 2
print(max_palindrome(make([2,3,7,2,12,24]))) # 1
print(max_palindrome(make([2,3,7,3,2,2,12,24]))) # 1
