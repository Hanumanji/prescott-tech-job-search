package main

import (
	"fmt"
)

var ()

type Tree[T any] struct {
	value       T
	left, right *Tree[T]
}

func New[T any](v T, l *Tree[T], r *Tree[T]) *Tree[T] {
	return &Tree[T]{v, l, r}
}

func (t *Tree[T]) Lookup(x T) *Tree[T] {
	// TODO Look through Tree for x
	return t
}

func (t *Tree[T]) String() string {
	return fmt.Sprintf("%v(%v, %v)", t.value, t.left, t.right)
}

type Goo struct {
	A, B int
}

type Ber struct {
	Goo
	C, D float64
}

type Walk interface {
	Walk()
}

type Mix interface {
	Goo
	Walk
}

func main() {
	goo := &Goo{A: 27}
	fmt.Println(goo)

	// Grr!
	// unknown field 'A' in struct literal of type Ber
	// ber := &Ber{A: 27, C: 1.09}

	ber := &Ber{Goo: *goo, C: 1.09}

	// ...but here it is known!
	ber.A = 14

	fmt.Printf("%v %+v", ber, ber)

	stringTree := &Tree[string]{value: "Duck"}
	fmt.Println(stringTree)

	root := New("27", New("13", nil, nil), New("-3", nil, nil))
	fmt.Printf("%+v\n", root)

	// invalid composite literal type Mix
	// m := &Mix{goo}
	// fmt.Println(m)
}
