package main

// https://go.dev/doc/effective_go#embedding

import (
	"fmt"
	"log"
	"os"
)

type Job struct {
	Command string
	*log.Logger
}

func (job *Job) Printf(format string, args ...interface{}) {
	job.Logger.Printf("%q: %s", job.Command, fmt.Sprintf(format, args...))
}

type First struct {
	a, b int
}

type Second struct {
	a, c int
}

type Combo struct {
	*First
	*Second
}

func main(){
	c := &Combo{}

	c.First = &First{1,2}
	c.Second = &Second{3,4}
	
	fmt.Println(c)

	// AMBIGUOUS
	// fmt.Println(c.a)
	
	// EXPLICIT to resolve a
	fmt.Println(c.First.a)
	
	fmt.Println(c.b)
	fmt.Println(c.c)

	job := &Job{"test", log.New(os.Stderr, "Job: ", log.Ldate)}
	fmt.Println(job)
	
	job.Println("starting now")
	
	job.Printf("and this")
}
