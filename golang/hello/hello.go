package main

import (
	"example.com/greetings"
	"fmt"
	"log"
)

func main() {
	log.SetPrefix("greetings: ")
	log.SetFlags(0)

	messages, err := greetings.Hellos([]string{"Hoopty Boy", "Paulicka", "Dust"})
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(messages)
}
