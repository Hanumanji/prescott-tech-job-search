package main

import (
	"bufio"
	"errors"
	"fmt"
	"log"
	"net"
	"net/http"
	"net/rpc"
	"os"
	"os/exec"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"
)

var (
	notYourTurn            = "not your turn"
	positionOccupied       = "position occupied"
	OutOfBoundsError       = errors.New("position out of bounds")
	OccupiedError          = errors.New(positionOccupied)
	WaitingForPartnerError = errors.New("waiting for partner")
	NotYourTurnError       = errors.New(notYourTurn)
	GameOverError          = errors.New("game over")
	NoWinMoveError         = errors.New("no win move possible")
	NoMoveError            = errors.New("no moves available")
)

const listenAddr = "localhost:4000"

type PlayerId int

const (
	invalidPlayerId = PlayerId(0)
)

type XO string

const (
	E = XO("")  // Empty
	X = XO("X")
	O = XO("O")
)

func (xo XO)Other() (other XO) {
	if xo == X {
		other = O
	} else {
		other = X
	}
	return
}

type PlayerXO struct {
	PlayerId
	XO
}

type PlayerXOPos struct {
	PlayerXO
	Row, Column int
}

type TicTacToe struct {
	Last   PlayerId
	Grid   [3][3]XO
	Winner XO
}

func clear() {
	var cmds []string
	switch runtime.GOOS {
	case "linux":
		cmds = []string{"clear"}
	case "windows":
		cmds = []string{"cmd", "/c", "cls"}
	default:
		panic("Unknown OS, so no screen clear for you!")
	}
	cmd := exec.Command(cmds[0], cmds[1:]...)
	cmd.Stdout = os.Stdout
	cmd.Run()
}

func input(format string, a ...any) string {
	fmt.Printf(format, a...)

	reader := bufio.NewReader(os.Stdin)

	text, _ := reader.ReadString('\n')
	text = strings.Replace(text, "\n", "", -1)

	return text
}

func (t *TicTacToe) Display() {
	clear()
	for i := 0; i < 3; i++ {
		fmt.Print("|")
		for j := 0; j < 3; j++ {
			fmt.Printf("%2s |", t.Grid[i][j])
		}
		fmt.Println()
	}
}

func (t *TicTacToe) checkWinner() XO {
	for i := 0; i < 3; i++ {
		// row check
		a, b, c := t.Grid[i][0], t.Grid[i][1], t.Grid[i][2]
		if a != E && a == b && b == c {
			return a
		}

		// column check
		a, b, c = t.Grid[0][i], t.Grid[1][i], t.Grid[2][i]
		if a != E && a == b && b == c {
			return a
		}
	}

	// diagonal ul -> lr
	a, b, c := t.Grid[0][0], t.Grid[1][1], t.Grid[2][2]
	if a != E && a == b && b == c {
		return a
	}

	// diagonal ll -> ur
	a, b, c = t.Grid[2][0], t.Grid[1][1], t.Grid[0][2]
	if a != E && a == b && b == c {
		return a
	}
	return E
}

//type TicTacToes map[PlayerId]*TicTacToe
type TicTacToes struct {
	sync.Map
}

var lastPlayerId PlayerId

func (t *TicTacToes) Go(xop PlayerXOPos, after *TicTacToe) error {
	if xop.Row < 1 || xop.Row > 3 || xop.Column < 1 || xop.Column > 3 {
		return OutOfBoundsError
	}
	//ttt, ok := t[xop.PlayerId]
	a, ok := t.Load(xop.PlayerId)
	if !ok {
		fmt.Println("Go Load failed", xop.PlayerId)
		return WaitingForPartnerError
	}
	ttt, ok := a.(*TicTacToe)
	if !ok {
		log.Fatal("WTF?")
	}
	if ttt.Winner != E {
		return GameOverError
	}

	if ttt.Last == xop.PlayerId {
		return NotYourTurnError
	}

	if ttt.Grid[xop.Row-1][xop.Column-1] != E {
		return OccupiedError
	}

	ttt.Grid[xop.Row-1][xop.Column-1] = xop.XO
	ttt.Winner = ttt.checkWinner()
	ttt.Last = xop.PlayerId

	*after = *ttt

	return nil
}

func (t *TicTacToes) Get(xop PlayerXO, board *TicTacToe) error {
	// ttt, ok := t[xop.PlayerId]
	a, ok := t.Load(xop.PlayerId)
	if !ok {
		fmt.Println("Failed Get Load", xop.PlayerId)
		return WaitingForPartnerError
	}
	ttt, ok := a.(*TicTacToe)
	if !ok {
		log.Fatal("WTF?")
	}

	*board = *ttt

	return nil
}

var partner = make(chan PlayerId)

func (t *TicTacToes) Connect(name string, playerXO *PlayerXO) error {
	lastPlayerId++

	playerXO.PlayerId = lastPlayerId

	select {
	case partner <- playerXO.PlayerId:
		// now handled by other go routine
		playerXO.XO = X
	case otherId := <-partner:
		ttt := new(TicTacToe)
		ttt.Last = playerXO.PlayerId // Want X to go first, so set Last to O
		playerXO.XO = O
		//t[playerXO.PlayerId] = ttt
		//t[otherId] = ttt
		fmt.Println("Store", playerXO.PlayerId, otherId)
		t.Store(playerXO.PlayerId, ttt)
		if _, ok := t.Load(playerXO.PlayerId); !ok {
			fmt.Println("Failed to store..WTF?")
		}
		t.Store(otherId, ttt)
		if _, ok := t.Load(otherId); !ok {
			fmt.Println("Failed to store..WTF?")
		}
		//fmt.Println("Connect", t)
	}
	return nil
}

func Server() {
	ttt := &TicTacToes{}
	rpc.Register(ttt)
	rpc.HandleHTTP()
	l, err := net.Listen("tcp", listenAddr)
	if err != nil {
		log.Fatal(err)
	}
//	go func(){
//		for {
//			log.Print(ttt)
//			time.Sleep(1 * time.Second)			
//		}
//	}()
	http.Serve(l, nil)
}

func connect(name string, client *rpc.Client) PlayerXO {
	var playerXO PlayerXO

	for playerXO.PlayerId == invalidPlayerId {
		err := client.Call("TicTacToes.Connect", name, &playerXO)
		if err != nil {
			fmt.Println("connect Call Connect failed")
			log.Fatal(err)
		}
		fmt.Println(playerXO)
	}
	return playerXO
}

func Client() {
	client, err := rpc.DialHTTP("tcp", listenAddr)
	if err != nil {
		log.Fatal(err)
	}

	msg := input("name? > ")
	fmt.Printf("Thanks, %v...waiting for another player...", msg)

	playerXO := connect(msg, client)
	fmt.Printf("You are %v.\n", playerXO.XO)

	fmt.Println("Input 'row column' 1-3 (ie '2 2' for middle square)")
	input("Hit return to continue...")

	board := TicTacToe{}
	err = client.Call("TicTacToes.Get", playerXO, &board)
	if err != nil {
		fmt.Println(err)
		input("error?")
	}

	for {
		board.Display()
		if board.Winner == playerXO.XO {
			fmt.Println("YOU WIN!!")
			return
		} else if board.Winner != E {
			fmt.Println("You lost...:-(")
			return
		}
		if _, _, err := anyMove(board); err == NoMoveError {
			fmt.Println("Ha!  Draw.")
			return
		}

		// If not your turn, then sleep and refresh the board every second
		if board.Last == playerXO.PlayerId {
			time.Sleep(1 * time.Second)
			err = client.Call("TicTacToes.Get", playerXO, &board)
			if err != nil {
				fmt.Println(err)
				input("error?")
			}
			continue
		}
		msg := input("place %v > ", playerXO.XO)
		f := strings.Fields(msg)
		if len(f) != 2 {
			fmt.Println("Enter two fields exactly")
			input("error?")
			continue
		}
		row, err := strconv.Atoi(f[0])
		if err != nil {
			fmt.Println(err)
			input("error?")
			continue
		}
		column, err := strconv.Atoi(f[1])
		if err != nil {
			fmt.Println(err)
			input("error?")
			continue
		}

		xoPos := PlayerXOPos{playerXO, row, column}
		err = client.Call("TicTacToes.Go", xoPos, &board)
		if err != nil {
			fmt.Println(err)
			input("error?")
			continue
		}
	}
}

func winMove(board TicTacToe, xo XO) (row, column int, err error) {
	for i := 1; i <= 3; i++ {
		for j := 1; j <= 3; j++ {
			check := board.Grid[i-1][j-1]
			if check != E {
				continue
			}

			board.Grid[i-1][j-1] = xo
			if board.checkWinner() == xo {
				row, column = i, j
				board.Grid[i-1][j-1] = check
				fmt.Println("win move", i, j)
				return
			}
			board.Grid[i-1][j-1] = check
		}
	}
	err = NoWinMoveError
	return
}

func anyMove(board TicTacToe) (row, column int, err error) {
	// Try middle first, since blocks 4 possible win sets
	if board.Grid[1][1] == E {
		row, column = 2, 2
		return
	}
	// Try the corners next...
	if board.Grid[0][0] == E {
		row, column = 1, 1
		return
	}
	if board.Grid[0][2] == E {
		row, column = 1, 3
		return
	}
	if board.Grid[2][0] == E {
		row, column = 3, 1
		return
	}
	if board.Grid[2][2] == E {
		row, column = 3, 3
		return
	}
	// Check evertything else...
	for i := 1; i <= 3; i++ {
		for j := 1; j <= 3; j++ {
			if board.Grid[i-1][j-1] != E {
				continue
			}
			row, column = i, j
			return
		}
	}
	err = NoMoveError
	return
}

func Robot(display bool) (XO, error) {
	client, err := rpc.DialHTTP("tcp", listenAddr)
	if err != nil {
		// log.Fatal(err)
		return E, err
	}

	playerXO := connect("Robot", client)

	board := TicTacToe{}
	for {
		err = client.Call("TicTacToes.Get", playerXO, &board)
		if err != nil {
			//fmt.Println("Get failed", playerXO, err)
			// log.Fatal(err)
			return E, nil
			time.Sleep(1 * time.Second)			
			continue
		}
		if display {
			board.Display()
		}

		if board.Winner != E {
			// log.Fatal(board)
			return board.Winner, nil
		}
		if _, _, err = anyMove(board); err != nil {
			// log.Fatal(err)
			return E, err
		}

		if board.Last == playerXO.PlayerId {
			time.Sleep(1 * time.Second)
			continue
		}

		xo := playerXO.XO

		// Look for my win move
		row, col, err := winMove(board, xo)
		if err != nil {
			// If can't find win, then block their win move
			row, col, err = winMove(board, xo.Other())
			if err != nil {
				row, col, err = anyMove(board)
				if err != nil {
					// log.Fatal(err)
					return E, err
				}
			}
		}
		if display {
			fmt.Println("trying", row, col)
		}
		xoPos := PlayerXOPos{playerXO, row, col}
		err = client.Call("TicTacToes.Go", xoPos, &board)
		if err != nil {
			fmt.Println("Go failed")			
			fmt.Println(err)
			switch err.Error() {
			case notYourTurn:
				continue
			case positionOccupied:
				continue
			default:
				// log.Fatal(err)
				return E, err
			}
		}
	}
}

func Local() {
	// But the goal is to make it have two Players, so use same interface for Server, Client, and Local
	// along with parsing input() accordingly...

	go Server()
	time.Sleep(1 * time.Second)
	Client()
}

var invasionCount = 10

func Invasion() {
	go Server()
	time.Sleep(1 * time.Second)

	wg := sync.WaitGroup{}
	c := make(chan int)
	for i := 1; i < invasionCount; i++ {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			if winner, err := Robot(false); err == nil {
				fmt.Println("winner", winner, i)
			} else {
				fmt.Println("robot err", err, i)
			}
			c <- i
		}(i)
	}
	//	wg.Wait()
	//	close(c)
	j := 0
	for i := range c {
		fmt.Printf("robot %d done: %d\n", i, j)
		j++
	}
}

func main() {
	// server  - 0 Players
	// client  - 1 Player
	// local   - 2 Players
	if len(os.Args) > 1 {
		switch os.Args[1] {
		case "server":
			Server()
		case "client":
			Client()
		case "local":
			Local()
		case "robot":
			if winner, err := Robot(true); err == nil {
				if winner != E {
					fmt.Println("winner", winner)
				} else {
					fmt.Println("draw")
				}
			} else {
				fmt.Println("robot err", err)
			}
		case "invasion":
			Invasion()
		default:
			log.Fatalf("Invalid switch '%v', try 'server', 'client', 'local', 'robot', 'invasion' (or none = 'local')", os.Args[1])
		}
	} else {
		Local()
	}
}
