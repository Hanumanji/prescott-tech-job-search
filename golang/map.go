package main

import (
	"fmt"
	"sort"
)

var ()

func GuessingGame() {
	var s string
	fmt.Printf("Pick an integer from 0 to 100.\n")
	answer := sort.Search(100, func(i int) bool {
		fmt.Printf("Is your number <= %d? ", i)
		fmt.Scanf("%s", &s)
		return s != "" && s[0] == 'y'
	})
	fmt.Printf("Your number is %d.\n", answer)
}

func main() {
	m := map[string]int{"Duck": 23}
	s := []int{7, -11, -3, 0, 4}

	fmt.Println("m", m)
	fmt.Println("s", s)

	
	q := m
	q["Pickle"] = -17
	
	fmt.Println("q", q)
	fmt.Println("m", m)

	// sort.Ints(s)
	// sort.IntSlice(s).Sort()
	sort.Slice(s, func(i, j int) bool { return s[i] < s[j] })

	fmt.Println(s)

	GuessingGame()
}
