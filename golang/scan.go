package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

var ()

const (
	maxCapacity int = 256
)

func main() {
	file, err := os.Open("scan.go")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	buf := make([]byte, maxCapacity)
	scanner.Buffer(buf, maxCapacity)
	
	// optionally, resize scanner's capacity for lines over 64K, see next example
	for i := 1; scanner.Scan(); i++ {
		fmt.Println(i, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}
