# Basics

for, if, else, switch, defer
defer, panic, recover
structs

array
slice
  make - creating slice/array in one
  append - autogrow arrays
  range - iterating
    i, _ := range arr
    _, v := range arr
    i := range arr

map
  make 

# [Go Module](https://go.dev/doc/tutorial/create-module)

- Created hello/ and greetings/ dir as per tutorial
- Reading links now...lots to read!



