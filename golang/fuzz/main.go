package main

import (
	"fmt"
	"errors"
	"unicode/utf8"	
)

var ()

func Reverse(s string) (string, error) {
	if !utf8.ValidString(s){
		return s, errors.New("input is not valid UTF-8")
	}
	r := []rune(s)
	l := len(r)
	for i, j := 0, l - 1; i < l / 2; i, j = i + 1, j - 1 {
		r[i], r[j] = r[j], r[i]
	}
	return string(r), nil
}

func main(){
	input := "The quick brown fox jumped over the lazy dog"
	rev, revErr := Reverse(input)
	doubleRev, doubleRevErr := Reverse(rev)
	fmt.Println(input, rev, revErr, doubleRev, doubleRevErr)
}
