package main

import (
	"fmt"
)

var ()

func init() { }

func main() {
	// THING:
	a := 17

	// No LABEL: inside of switch...or rather, goto LABEL will give warning
	// "...goto THING jumps into block starting at..."
	switch a {
	case 17:
		fmt.Println(17)
		//goto THING
	case 3:
		//THING:
		fmt.Println(3)
		//	goto OTHER
	case -1:
		//	OTHER:
		fmt.Println(-1)
	default:
		fmt.Println("default")
	}

	// goto THING
}
