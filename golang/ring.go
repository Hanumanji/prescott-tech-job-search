package main

import (
	"container/ring"
	"fmt"
)

type Data struct {
	name string
	count int
}

func main() {
	// Create a new ring of size 6
	r := ring.New(6)

	// Get the length of the ring
	n := r.Len()

	// Initialize the ring with some integer values
	for i := 0; i < n; i++ {
		r.Value = &Data{"blah", i}
		r = r.Next()
	}
	
	// Unlink three elements from r, starting from r.Next()
	o := r.Unlink(3)

	p := func(p any) { fmt.Println(p.(* Data)) }
	
	// Iterate through the remaining ring and print its contents
	fmt.Println("r")
	r.Do(p)

	fmt.Println("o")
	o.Do(p)


	i := 0
	o.Do(func(p any){
		data := p.(* Data)
		i += 1
		data.count += i
	})

	fmt.Println("o++")
	o.Do(p)	
}
