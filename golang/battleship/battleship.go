package main

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"runtime"
	"strconv"
	"strings"
)

type Orientation int

const (
	UNORIENTED Orientation = iota
	VERTICAL
	HORIZONTAL
)

type Pos struct {
	x, y int
}

type Shot int

const (
	UNKNOWN Shot = iota
	MISS
	HIT
	ALREADY_MISS
	ALREADY_HIT
)

type Ship struct {
	size, health int
	label        string
}

var (
	AircraftCarrier = Ship{5, 5, "AircraftCarrier"}
	Battleship      = Ship{4, 4, "Battleship"}
	Submarine       = Ship{3, 3, "Submarine"}
	Destroyer       = Ship{3, 3, "Destroyer"}
	PatrolBoat      = Ship{2, 2, "PatrolBoat"}
)

func (s *Ship) String() string {
	return fmt.Sprintf("%s(%d/%d)", s.label, s.size, s.health)
}

func (s *Ship) isSunk() bool { return s.health == 0 }
func (s *Ship) hit()         { s.health-- }

type Grid struct {
	Pos
	ships map[Pos]*Ship
	shots map[Pos]Shot
}

func (g Grid) validPos(p Pos) bool { return p.x >= 0 && p.x <= g.x && p.y >= 0 && p.y <= g.y }
func (g Grid) shipAt(p Pos) *Ship  { return g.ships[p] }
func (g Grid) shotAt(p Pos) Shot   { return g.shots[p] }
func (g Grid) shipPositions(ship *Ship, p Pos, o Orientation) []Pos {
	res := make([]Pos, ship.size)
	x, y := p.x, p.y
	for i := range res {
		if o == HORIZONTAL {
			res[i] = Pos{x + i, y}
		} else if o == VERTICAL {
			res[i] = Pos{x, y + i}
		}
	}
	return res
}

func (g Grid) addShip(ship *Ship, p Pos, o Orientation) bool {
	positions := g.shipPositions(ship, p, o)
	for _, pos := range positions {
		if !g.validPos(pos) || g.shipAt(pos) != nil {
			return false
		}
	}
	for _, pos := range positions {
		g.ships[pos] = ship
	}

	return true
}

func (g Grid) shoot(p Pos) Shot {
	if !g.validPos(p) {
		panic(fmt.Sprintf("Invalid pos %v", p))
	}

	shot := MISS
	if shot, ok := g.shots[p]; ok {
		if shot == HIT {
			return ALREADY_HIT
		}
		if shot == MISS {
			return ALREADY_MISS
		}
		panic("Should never get here!")
	}
	// shot will be MISS if get here...
	if shot != MISS {
		panic(fmt.Sprintf("shot %v SHOULD be miss", shot))
	}

	if ship := g.shipAt(p); ship != nil {
		ship.hit()
		shot = HIT
	}
	g.shots[p] = shot
	return shot
}

type Player struct {
	grid  Grid
	ships []Ship
}

type Gamer interface {
	init(x, y int, newGrid func(x, y int) Grid)
	setupPlayers()
	setupPlayer(i int)
	playerAllSunk(i int) bool
	displayPlayer(i int)
	displayWinner(i int)
	playerShoot(i int) Shot
	passPlayer()
	newGrid(x, y int) Grid
}

type Game struct {
	players []Player
}

func (g *Game) init(x, y int, newGrid func(x, y int) Grid) {
	g.players = []Player{
		Player{grid: newGrid(x, y), ships: []Ship{AircraftCarrier, Battleship, Submarine, Destroyer, Destroyer, PatrolBoat, PatrolBoat}},
		Player{grid: newGrid(x, y), ships: []Ship{AircraftCarrier, Battleship, Submarine, Destroyer, Destroyer, PatrolBoat, PatrolBoat}},
	}
}

func (g *Game) setupPlayers() {
	g.setupPlayer(0)
	g.passPlayer()
	g.setupPlayer(1)
	g.passPlayer()
}

func (g *Game) setupPlayer(i int) {
	for n := range g.players[i].ships {
		ship := &(g.players[i].ships[n])
		g.placePlayerShip(i, ship)
	}
}

func (g *Game) placePlayerShip(i int, ship *Ship) {
	grid := g.players[i].grid
	for {
		g.displayPlayer(i)
		p := g.input("Where would you like to place ship %v (H)orizontal/(V)ertical? ie 5 3 H > ", ship)
		parts := strings.Fields(p)
		if len(parts) != 3 {
			continue
		}

		x, err := strconv.Atoi(parts[0])
		if err != nil {
			continue
		}
		y, err := strconv.Atoi(parts[1])
		if err != nil {
			continue
		}
		var o Orientation
		switch parts[2] {
		case "H":
			o = HORIZONTAL
		case "V":
			o = VERTICAL
		default:
			o = UNORIENTED
		}
		if o == UNORIENTED {
			continue
		}
		pos := Pos{x, y}
		if !grid.addShip(ship, pos, o) {
			g.input("Collision or out of bounds at %v, %v for %v...try again", pos, o, ship)
		} else {
			break
		}
	}
}

func (g *Game) playerAllSunk(i int) bool {
	player := g.players[i]
	for _, ship := range player.ships {
		if !ship.isSunk() {
			return false
		}
	}
	return true
}

func (g *Game) displayPlayer(i int) {
	g.clear()

	o := (i + 1) % 2
	grid := g.players[i].grid

	fmt.Println("Player Grid", i)

	fmt.Print("   ")
	for x := 0; x < grid.x; x++ {
		fmt.Printf("%02d ", x)
	}
	fmt.Print("|")
	for x := 0; x < grid.x; x++ {
		fmt.Printf("%02d ", x)
	}
	fmt.Println()

	for y := 0; y < grid.y; y++ {
		fmt.Printf("%02d ", y)

		// Print my grid, ships, and their hits/misses on me
		grid = g.players[i].grid
		for x := 0; x < grid.x; x++ {
			c := '.'
			pos := Pos{x, y}
			ship := grid.shipAt(pos)
			if ship != nil {
				c = []rune(ship.label)[0]
			}
			shot := grid.shots[pos]
			if shot == HIT {
				c = 'X'
			} else if shot == MISS {
				c = 'M'
			}
			fmt.Printf(" %c ", c)
		}
		fmt.Print("|")

		// Print their grid, and my hits/misses on them
		// BUT NOT THEIR SHIPS!
		grid = g.players[o].grid
		for x := 0; x < grid.x; x++ {
			c := '.'
			pos := Pos{x, y}
			shot := grid.shots[pos]
			if shot == HIT {
				c = 'X'
			} else if shot == MISS {
				c = 'M'
			}
			fmt.Printf(" %c ", c)
		}
		fmt.Printf("%02d ", y)
		fmt.Println()
	}

	fmt.Print("   ")
	for x := 0; x < grid.x; x++ {
		fmt.Printf("%02d ", x)
	}
	fmt.Print("|")
	for x := 0; x < grid.x; x++ {
		fmt.Printf("%02d ", x)
	}
	fmt.Println()
}

func (g *Game) displayWinner(i int) {
	fmt.Printf("Player %d, you win!!!\n", i)
}

func (g *Game) input(format string, a ...any) string {
	fmt.Printf(format, a...)

	reader := bufio.NewReader(os.Stdin)

	text, _ := reader.ReadString('\n')
	text = strings.Replace(text, "\n", "", -1)

	return text
}

func (g *Game) clear() {
	var cmds []string
	switch runtime.GOOS {
	case "linux":
		cmds = []string{"clear"}
	case "windows":
		cmds = []string{"cmd", "/c", "cls"}
	default:
		panic("Unknown OS, so no screen clear for you!")
	}
	cmd := exec.Command(cmds[0], cmds[1:]...)
	cmd.Stdout = os.Stdout
	cmd.Run()
}

func (g *Game) passPlayer() {
	g.clear()
	g.input("PASS NEXT PLAYER")
}

func (g *Game) playerShoot(i int) Shot {
	o := (i + 1) % 2

	grid := g.players[o].grid

	for {
		shoot := g.input("Where to shoot?  X Y ie 5 3 ")
		posStrings := strings.Split(shoot, " ")
		if len(posStrings) != 2 {
			continue
		}
		x, err := strconv.Atoi(posStrings[0])
		if err != nil {
			continue
		}
		y, err := strconv.Atoi(posStrings[1])
		if err != nil {
			continue
		}

		pos := Pos{x, y}
		shot := grid.shoot(pos)
		switch shot {
		case ALREADY_HIT:
			g.input("Already hit at %v...try again", pos)
		case ALREADY_MISS:
			g.input("Already miss at %v...try again", pos)
		case HIT:
			g.displayPlayer(i)
			g.input("HIT at %v...woohoo!", pos)
			if ship := grid.shipAt(pos); ship.isSunk() {
				g.input("You sunk their %v", ship)
			}
			return shot
		case MISS:
			g.displayPlayer(i)
			g.input("Aw...you missed at %v", pos)
			return shot
		}
	}

	return UNKNOWN
}

func (g *Game) newGrid(x, y int) Grid {
	grid := Grid{Pos: Pos{x, y}}
	grid.ships = make(map[Pos]*Ship)
	grid.shots = make(map[Pos]Shot)
	return grid
}

func Run(g Gamer, x, y int) {
	g.init(x, y, g.newGrid)
	g.setupPlayers()

	winner := -1
	current, other := 0, 1
	for winner == -1 {
		g.displayPlayer(current)
		if g.playerShoot(current) == HIT && g.playerAllSunk(other) {
			g.displayWinner(current)
			break
		}
		current, other = other, current
		g.passPlayer()
	}
}

type TestGame struct {
	Game
}

func (g *TestGame) setupPlayers() {
	dataSet := [][]struct {
		Ship
		Pos
		Orientation
	}{
		{
			{AircraftCarrier, Pos{0, 0}, HORIZONTAL},
			{Battleship, Pos{6, 0}, VERTICAL},
			{Submarine, Pos{0, 3}, HORIZONTAL},
			{Destroyer, Pos{8, 3}, VERTICAL},
			{Destroyer, Pos{0, 2}, HORIZONTAL},
			{PatrolBoat, Pos{0, 7}, VERTICAL},
			{PatrolBoat, Pos{6, 8}, HORIZONTAL},
		},
		{
			{AircraftCarrier, Pos{9, 4}, VERTICAL},
			{Battleship, Pos{1, 1}, HORIZONTAL},
			{Submarine, Pos{3, 3}, VERTICAL},
			{Destroyer, Pos{5, 5}, HORIZONTAL},
			{Destroyer, Pos{0, 0}, VERTICAL},
			{PatrolBoat, Pos{0, 5}, HORIZONTAL},
			{PatrolBoat, Pos{4, 6}, VERTICAL},
		},
	}
	for index, data := range dataSet {
		player := g.players[index]
		grid := player.grid
		for i, d := range data {
			player.ships[i] = d.Ship
			ship := &(player.ships[i])
			if success := grid.addShip(ship, d.Pos, d.Orientation); !success {
				panic(fmt.Sprintf("failed to addShip %v for player %d", d, index))
			}
		}
	}
}

type SunkGame struct {
	TestGame
}

func (g *SunkGame) setupPlayers() {
	g.TestGame.setupPlayers()
	g.almostAllSunk()
}

func (g *SunkGame) almostAllSunk() {
	for i := 0; i < 2; i++ {
		var lastPos Pos
		var lastShip *Ship

		grid := g.players[i].grid
		for pos, ship := range grid.ships {
			grid.shoot(pos)
			lastPos, lastShip = pos, ship
		}

		if !g.playerAllSunk(i) {
			panic("should be all sunk!")
		}
		lastShip.health += 1
		delete(grid.shots, lastPos)
		if g.playerAllSunk(i) {
			panic("should NOT be all sunk!")
		}
	}
}

func main() {
	var gamer Gamer
	if len(os.Args) > 1 {
		switch os.Args[1] {
		case "test":
			gamer = &TestGame{}
		case "sunk":
			gamer = &SunkGame{}
		default:
			panic(fmt.Sprint("Invalid arg '", os.Args[1], "', try 'test' or 'sunk' or none."))
		}
	} else {
		gamer = &Game{}
	}

	Run(gamer, 10, 10)
}
