package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"time"
)

type Pos struct {
	x, y int
}

var (
	invalidPos = Pos{-1, -1}
)

type Shot int

const (
	UNKNOWN Shot = iota
	MISS
	HIT
	ALREADY_MISS
	ALREADY_HIT
)

type Orientation int

const (
	UNORIENTED Orientation = iota
	VERTICAL
	HORIZONTAL
)

type Ship struct {
	health, size int
	label        string
	positions    []Pos
}

var (
	AircraftCarrier = Ship{5, 5, "AircraftCarrier", nil}
	Battleship      = Ship{4, 4, "Battleship", nil}
	Submarine       = Ship{3, 3, "Submarine", nil}
	Destroyer       = Ship{3, 3, "Destroyer", nil}
	PatrolBoat      = Ship{2, 2, "PatrolBoat", nil}
)

func (s *Ship) String() string {
	return fmt.Sprintf("%s(%d/%d)", s.label, s.health, s.size)
}

func (s Ship) onBoard() bool { return len(s.positions) > 0 }
func (s *Ship) isSunk() bool { return s.health == 0 }
func (s *Ship) hit()         { s.health-- }

// Gob communicated during play...
type ShotPos struct {
	Shot
	Pos
}

type Message struct {
	player  string
	message string
	time    time.Time
}

type Enemy interface {
	GetName() string

	GridSize() Pos

	Shoot(Pos) (Shot, error)
	ShotPositions() map[Pos]Shot

	SunkShips() []Ship
}

type Player interface {
	Enemy

	Enemy() Enemy

	SetName(string) error

	GetShips() []Ship
	//	GetShip(int) Ship
	//	GetShipCount() int

	AddShip(int, Pos, Orientation) error
	RemoveShip(int) error

	ShipAt(Pos) (Ship, error)

	ShipPositions() map[Pos]Ship

	//	OnTurn(func (Player))
	//
	//	Start() bool // true if no UnaddedShips, and SetName() been called...
	//	IsStarted() bool // true if Start successful, can RemoveShip(), SetName() and becomes false again
	//	IsPlaying() bool // true if Player and Enemy IsStarted()

	//	Shoot(Pos) (Shot, error)
	//	Shots() []ShotPos // Successful Shots with Pos in order...
	//	ShotPositions() map[Pos]Shot
	//
	//	Version() int // Number of changes to this Player...used to sync...-1 if unimplemented
	//	Chat(string) // For chatting...
	//	ChatHistory() []Message
}

type OutOfBoundsError struct {
	pos    Pos
	bounds Pos
}

func (o OutOfBoundsError) Error() string {
	return fmt.Sprintf("position %v out of bounds %v", o.pos, o.bounds)
}

type NoShipError struct {
	pos Pos
}

func (n NoShipError) Error() string {
	return fmt.Sprintf("no ship at %v", n.pos)
}

type Board struct {
	Pos
	ships         []Ship
	shipPositions map[Pos]Ship
	shotPositions map[Pos]Shot
	enemy         Player
	name          string
}

// Enemy impl
func (b Board) GetName() string { return b.name }

func (b Board) GridSize() Pos { return b.Pos }

func (b Board) Shoot(pos Pos) (Shot, error) {
	shot := MISS
	if shot, ok := b.shotPositions[pos]; ok {
		if shot == HIT {
			return UNKNOWN, fmt.Errorf("already hit at position %v", pos)
		}
		if shot == MISS {
			return UNKNOWN, fmt.Errorf("already miss at position %v", pos)
		}
		panic("Should never get here!")
	}

	var noShipError *NoShipError
	ship, err := b.ShipAt(pos)
	if errors.As(err, &noShipError) {
		shot = MISS
	} else if err == nil {
		ship.hit()
		shot = HIT
	} else {
		return UNKNOWN, err
	}

	b.shotPositions[pos] = shot

	return shot, nil
}

func (b Board) ShotPositions() map[Pos]Shot {
	return b.shotPositions
}

func (b Board) SunkShips() []Ship {
	sunk := []Ship{}
	for _, ship := range b.ships {
		if ship.isSunk() {
			sunk = append(sunk, ship)
		}
	}
	return sunk
}

// Player impl
func (b Board) Enemy() Enemy { return b.enemy }
func (b Board) SetName(s string) error {
	// TODO Implement so cannot SetName after Start()
	b.name = s
	return nil
}

func (b Board) GetShips() []Ship { return b.ships }

func (b Board) AddShip(i int, p Pos, o Orientation) error {
	ship := b.ships[i]
	if positions, err := b.addShipPositions(ship.size, p, o); err != nil {
		return err
	} else {
		for _, pos := range positions {
			b.shipPositions[pos] = ship
		}
		ship.positions = positions
	}

	return nil
}

func (b Board) RemoveShip(i int) error {
	ship := b.ships[i]
	if !ship.onBoard() {
		return fmt.Errorf("ship %d %v not added, cannot be removed", i, ship)
	}

	for _, pos := range ship.positions {
		delete(b.shipPositions, pos)
	}
	ship.positions = ship.positions[:0]

	return nil
}

func (b Board) ShipAt(pos Pos) (Ship, error) {
	if err := b.validatePos(pos); err != nil {
		return Ship{}, err
	}
	if ship, ok := b.shipPositions[pos]; ok {
		return ship, nil
	} else {
		return Ship{}, &NoShipError{pos}
	}
}

func (b Board) ShipPositions() map[Pos]Ship {
	return b.shipPositions
}

func (b Board) validatePos(p Pos) error {
	if p.x >= 0 && p.x <= b.x && p.y >= 0 && p.y <= b.y {
		return &OutOfBoundsError{p, b.Pos}
	}
	return nil
}

func (b Board) shotAt(p Pos) Shot { return b.shotPositions[p] }
func (b Board) addShipPositions(size int, start Pos, o Orientation) ([]Pos, error) {
	res := make([]Pos, size)

	for i := range res {
		pos := start
		if o == HORIZONTAL {
			pos.x += i
		} else if o == VERTICAL {
			pos.y += i
		}
		if err := b.validatePos(pos); err != nil {
			return nil, err
		}
		if ship, error := b.ShipAt(pos); error == nil {
			return nil, fmt.Errorf("position %v collided with ship %v", pos, ship)
		}
		res[i] = pos
	}

	return res, nil
}

type Game struct {
	player0, player1 Board
}

func NewGame() Game {
	size := Pos{10, 10}
	ships := []Ship{AircraftCarrier, Battleship, Submarine, Destroyer, Destroyer, PatrolBoat, PatrolBoat}
	player := Board{size, ships, map[Pos]Ship{}, map[Pos]Shot{}, nil, ""}
	g := Game{
		player0: player,
		player1: player,
	}
	g.player0.enemy = g.player1
	g.player1.enemy = g.player0
	return g
}

func (g Game) Run() {
	fmt.Println("Game Run NYI")
}

const listenAddr = "localhost:4000"

var partner = make(chan io.ReadWriteCloser)

func input(format string, a ...any) string {
	fmt.Printf(format, a...)

	reader := bufio.NewReader(os.Stdin)

	text, _ := reader.ReadString('\n')
	text = strings.Replace(text, "\n", "", -1)

	return text
}

func clear() {
	var cmds []string
	switch runtime.GOOS {
	case "linux":
		cmds = []string{"clear"}
	case "windows":
		cmds = []string{"cmd", "/c", "cls"}
	default:
		panic("Unknown OS, so no screen clear for you!")
	}
	cmd := exec.Command(cmds[0], cmds[1:]...)
	cmd.Stdout = os.Stdout
	cmd.Run()
}

func play(a, b io.ReadWriteCloser) {
	fmt.Fprintln(a, "Found one! Say hi.")
	fmt.Fprintln(b, "Found one! Say hi.")
	go io.Copy(a, b)
	io.Copy(b, a)
}

func match(c io.ReadWriteCloser) {
	fmt.Fprint(c, "Waiting for a partner...")
	select {
	case partner <- c:
		// now handled by other goroutine
	case p := <-partner:
		play(p, c)
	}
}

func Server() {
	l, err := net.Listen("tcp", listenAddr)
	if err != nil {
		log.Fatal(err)
	}
	for {
		c, err := l.Accept()
		if err != nil {
			log.Fatal(err)
		}
		go match(c)
	}
}

func Client() {
	l, err := net.Dial("tcp", listenAddr)
	if err != nil {
		log.Fatal(err)
	}
	go io.Copy(os.Stdout, l) // l as Reader
	for {
		msg := input(" > ")
		fmt.Fprintf(l, "%s\n", msg) // l as Writer
	}
}

func Local() {
	// But the goal is to make it have two Players, so use same interface for Server, Client, and Local
	// along with parsing input() accordingly...

	g := NewGame()
	g.Run()
}

func display(player Player) {
	clear()

	enemy := player.Enemy()
	grid := player.GridSize()

	enemyShotPositions := enemy.ShotPositions()

	shipPositions := player.ShipPositions()
	playerShotPositions := player.ShotPositions()

	fmt.Println("Player Grid", player.GetName())

	fmt.Print("   ")
	for x := 0; x < grid.x; x++ {
		fmt.Printf("%02d ", x)
	}
	fmt.Print("|")
	for x := 0; x < grid.x; x++ {
		fmt.Printf("%02d ", x)
	}
	fmt.Println()

	for y := 0; y < grid.y; y++ {
		fmt.Printf("%02d ", y)

		// Print my grid, ships, and their hits/misses on me
		for x := 0; x < grid.x; x++ {
			c := '.'
			pos := Pos{x, y}
			shot := enemyShotPositions[pos]
			if shot == HIT {
				c = 'X'
			} else if shot == MISS {
				c = 'M'
			} else if ship, ok := shipPositions[pos]; ok {
				c = []rune(ship.label)[0]
			}
			fmt.Printf(" %c ", c)
		}
		fmt.Print("|")

		// Print their grid, and my hits/misses on them
		// BUT NOT THEIR SHIPS!
		for x := 0; x < grid.x; x++ {
			c := '.'
			pos := Pos{x, y}
			shot := playerShotPositions[pos]
			if shot == HIT {
				c = 'X'
			} else if shot == MISS {
				c = 'M'
			}
			fmt.Printf(" %c ", c)
		}
		fmt.Println()
	}
}

func main() {
	// server  - 0 Players
	// client  - 1 Player
	// local   - 2 Players
	if len(os.Args) > 1 {
		switch os.Args[1] {
		case "server":
			Server()
		case "client":
			Client()
		case "local":
			Local()
		default:
			log.Fatalf("Invalid switch '%v', try 'server', 'client', 'local' (or none = 'local')", os.Args[1])
		}
	} else {
		Local()
	}
}
