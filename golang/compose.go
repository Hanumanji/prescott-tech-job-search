package main

import (
	"fmt"
	"math"
)

const ()

var ()

func Compose(f, g func(x float64) float64) func(x float64) float64 {
	return func(x float64) float64 {
		return f(g(x))
	}
}

func sin(x float64) (y float64) {
	y, _ = math.Sincos(x)
	return
}

func cos(x float64) (y float64) {
	_, y = math.Sincos(x)
	return
}

func main() {
	fmt.Println(Compose(sin, cos)(0.5))
}
