package main

import (
	"encoding/json"
	"fmt"
)

type FamilyMember struct {
	Name       string
	Age        int
	Parents    []string
}

type Command struct {}
type Message struct {}

type IncomingMessage struct {
	Cmd *Command
	Msg *Message
}

func main(){
	
	b := []byte(`{"Name":"Wednesday","Age":6,"Parents":["Gomez","Morticia"]}`)

	fmt.Println(string(b))

	var fm FamilyMember
	err := json.Unmarshal(b, &fm)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(fm)
	
	var f interface{}
	err = json.Unmarshal(b, &f)
	if err != nil {
		fmt.Println(err)
	}

	m := f.(map[string]interface{})
	
	for k, v := range m {
		switch vv := v.(type) {
		case string:
			fmt.Println(k, "is string", vv)
		case float64:
			fmt.Println(k, "is float64", vv)
		case []interface{}:
			fmt.Println(k, "is an array:")
			for i, u := range vv {
				fmt.Println(i, u)
			}
		default:
			fmt.Println(k, "is of a type I don't know how to handle")
		}
	}
}
