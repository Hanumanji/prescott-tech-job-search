package main

import (
	"fmt"
)

type Base struct{}
type Baser interface {
	Get() float32
}

type TypeOne struct {
	value float32
	Base
}

type TypeTwo struct {
	value float32
	Base
}

type TypeThree struct {
	value float32
	Base
}

type TypeFour struct {
	stuff float32
	TypeThree
}

func (t *Base) Get() float32 { return -17.3 }

func (t *TypeOne) Get() float32 {
	return t.value
}

type TypeFive TypeFour

//func (t *TypeTwo) Get() float32 {
//    return t.value * t.value
//}

func (t *TypeThree) Get() float32 {
	return t.value + t.value
}

//func (t *TypeFive) Get() float32 {
//	return 101
//}

func doIt(b Baser){
	fmt.Println("doIt ", b.Get())
}

func main() {
	base := Base{}
	t1 := &TypeOne{1, base}
	t2 := &TypeTwo{2, base}
	t3 := &TypeThree{4, base}
	t4 := &TypeFour{-1, TypeThree{-2, base}}
	t5 := &TypeFive{17, TypeThree{101, base}}
	
	bases := []Baser{Baser(&base), Baser(t1), Baser(t2), Baser(t3), Baser(t4), Baser(t5)}

	for s, _ := range bases {
		switch bases[s].(type) {
		case *TypeOne:
			fmt.Println("TypeOne")
		case *TypeTwo:
			fmt.Println("TypeTwo")
		case *TypeThree:
			fmt.Println("TypeThree")
		case *Base:
			fmt.Println("Base")
		default:
			fmt.Printf("default %T\n", bases[s])
		}

		fmt.Printf("The value is:  %f\n", bases[s].Get())
		doIt(bases[s])
	}
}
