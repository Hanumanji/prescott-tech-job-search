package main

// Building basic server from net/http
// Playing with interfaces...
// https://go.dev/doc/effective_go#interface_methods

import (
	"net/http"
	"fmt"
	"html"
	"log"
	"os"

	_ "net/http/pprof"   // TODO Just for the sideeffects!
)

// Simple counter server.
type Counter struct {
	n int
}

func (ctr *Counter) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	ctr.n++
	fmt.Fprintf(w, "counter = %d\n", ctr.n)
}

type Cntr int

func (ctr *Cntr) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	*ctr++
	fmt.Fprintf(w, "counter = %d\n", *ctr)
}

// A channel that sends a notification on each visit.
// (Probably want the channel to be buffered.)
type Chan chan *http.Request

func (ch Chan) ServeHTTP(w http.ResponseWriter, req *http.Request) {
    ch <- req
    fmt.Fprint(w, "notification sent")
}

type HandlerFunc func(http.ResponseWriter, *http.Request)

func (f HandlerFunc) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	f(w, req)
}

func ArgServer(w http.ResponseWriter, req *http.Request){
	fmt.Fprint(w, os.Args)
}

func main(){
	//http.Handle("/foo", fooHandler)

	http.HandleFunc("/bar", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello, %q", html.EscapeString(r.URL.Path))
	})

	ctr := &Counter{}
	http.Handle("/counter", ctr)

	ch := make(Chan)
	http.Handle("/chan", ch)
	go func(){
		for {
			fmt.Println(<-ch)
		}
	}()

	http.Handle("/args", http.HandlerFunc(ArgServer))
	
	addr_port := ":8080"
	fmt.Println("addr_port", addr_port)


	if fd, err := os.Open("server.go"); err != nil {
		log.Fatal(err)
	} else {
		// TODO: use fd  XXX !!!
		_ = fd
	}

	// MUST BE LAST!!!
	log.Fatal(http.ListenAndServe(addr_port, nil))
}
