package greetings

import (
       "fmt"
       "errors"
       "math/rand"
       "time"
       "rsc.io/quote"
       quoteV3 "rsc.io/quote/v3"       
       
       )

func Hello(name string)(string, error) {
     if name == "" {
     	return "", errors.New("empty name")
     }
     return fmt.Sprintf("%v -- %v", quote.Hello(), fmt.Sprintf(randomFormat(), name)), nil
}

func Hellos(names []string)(map[string]string, error){
     messages := make(map[string]string)
     for _, name := range names {
     	 message, err := Hello(name)
	 if err != nil {
	    return nil, err
	 }
	 messages[name] = message
     }
     return messages, nil
}

func Proverb() string {
     return quoteV3.Concurrency()
}

func init() {
     rand.Seed(time.Now().UnixNano())
}

func randomFormat() string {
     formats := []string{
          "Hi, %v. Welcome!",
	  "Great to see you, %v!",
	  "Hail, %v!  Well met!",
	  }
  return formats[rand.Intn(len(formats))]
}