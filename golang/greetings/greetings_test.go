package greetings

import (
       "testing"
       "regexp"
)

func TestHelloName(t *testing.T){
     name := "Gladys"
     want := regexp.MustCompile(`\b`+name+`\b`)
     msg, err := Hello("Gladys")
     if !want.MatchString(msg) || err != nil {
     	t.Fatalf(`Hello("Gladys") = %q, %v, want match for %#q, nil`, msg, err, want)
     }
}

func TestHelloEmpty(t *testing.T){
     msg, err := Hello("")
     if msg != "" || err == nil {
     	t.Fatalf(`Hello("") = %q, %v, want "", error`, msg, err)
     }
}


func TestProverb(t *testing.T){
     want := "Concurrency is not parallelism."
     if got := Proverb(); got != want {
     	t.Errorf("Proverb() = %q, want %q", got, want)
     }
}