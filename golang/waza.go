package main

// https://go.dev/blog/waza-talk
// Concurrency is not parallelism

import ()

var ()

const ()


// First Example... worker with channels
type Work struct {
	x, y, x
}

func worker(in <-chan *Work, out chan<- *Work) {
	for w := range in {
		w.z = w.x * w.y
		Sleep(w.z)
		out <- w
	}
}

func Run() {
	in, out := make(chan *Work), make(chan *Work)
	for i := 0; i < NumWorkers; i++ {
		go worker(in, out)
	}
	go sendLotsOfWork(in)
	receiveLotsOfResults(out)
}

// Second Example ... Load Balancer

type Request struct {
	fn func() int  // the operation to perform
	c  chan int    // the channel to return the result
}

func requester(work chan<- Request) {
	c := make(chan int)
	for {
		// Kill some time (fake load)
		Sleep(rand.Int63n(nWorker * 2))
		work <- Request{workFn, c}
		result := <-c
		futherProcess(result)
	}
}

type Worker struct {
	requests chan Request   // work to do (buffered channel)
	pending int             // count of pending tasks
	index int               // index in the heap
}

func (w *Worker) work(done chan *Worker) {
	for {
		req := <-w.requests // get Request from balancer
		req.c <- req.fn()   // call fn and send result
		done <- w           // we've finished the work
	}
}

type Pool []*Worker

// Make Pool implement Heap interface
func (p Pool) Len() int           { return len(p) }
func (p Pool) Less(i, j int) bool { return p[i].pending < p[j].pending }
func (p Pool) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

type Balancer struct {
	pool Pool
	done chan *Worker
}

func (b *Balancer) balance(work chan Request) {
	for {
		select {
		case req := <-work:
			b.dispatch(req)
		case w := <-b.done:
			b.completed(w)
		}
	}
}

// Send Request to worker
func (b *Balancer) dispatch(req Request) {
	// Grab the least loaded worker
	w := heap.Pop(&b.pool).(*Worker)
	//...sent it the task
	w.requests <- req
	// one more in it's work queue
	w.pending++
	// Put it back into the heap
	heap.Push(&b.pool, w)
}

// Job is complete; update heap
func (b *Balancer) completed(w *Worker) {
	// one fewer in the queue
	w.pending--
	// Remove it from the heap
	heap.Remove(&b.pool, w.index)
	// Put it into its place on the heap
	heap.Push(&b.pool, w)
}

// Third Example ... query a replicated database

func Query(conns []Conn, query string) Result {
	ch := make(chan Result, len(conns)) // buffered
	for _, conn := range conns {
		go func(c Conn) {
			ch <- c.DoQuery(query)
		}(conn)
	}
	return <- ch
}


func main() {


}
