package main

import (
	"fmt"
)

var ()

func main() {
	a := [...]int{1, -3, 5, -7, 9, -11}

	fmt.Printf("a %+v\n", a)

	s := a[1:4]
	fmt.Printf("s %+v len %d cap %d\n", s, len(s), cap(s))

	s = append(s, []int{-2, -4}...)
	fmt.Printf("s %+v len %d cap %d\n", s, len(s), cap(s))

	s = append(s, []int{-6, -8}...)
	fmt.Printf("s %+v len %d cap %d\n", s, len(s), cap(s))

	s = append(s, []int{12, 14, 18, 20}...)
	fmt.Printf("s %+v len %d cap %d\n", s, len(s), cap(s))

	s = s[3:7]
	fmt.Printf("s %+v len %d cap %d\n", s, len(s), cap(s))	
}
