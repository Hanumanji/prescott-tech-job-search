#include <iostream>
#include <bits/stdc++.h>

using namespace std;

void dump(vector<string> matrix, int n = 0){
  cout << "------" << n << "------" << endl;
  for (auto s : matrix){
    cout << s << endl;
  }
  cout << "-------------" << endl; 
}

char getAt(vector<string> grid, int i, int j){
  return grid[i][j];
}

vector<string> setAt(vector<string> grid, int i, int j, char c){
  string row = grid[i].replace(j, 1, 1, c);
  
  grid[i] = row;

  return grid;
}

vector<string> explode(const vector<string> grid, int i, int j){
  cout << "explode " << i << " " << j << endl;
  vector<string> newGrid = setAt(grid, i, j, '.');

  if (i < grid.size() - 1 && getAt(grid, i + 1, j) != '0'){
    newGrid = setAt(newGrid, i + 1, j, '.');
  }

  if (i >= 1 && getAt(grid, i - 1, j) != '0'){
    newGrid = setAt(newGrid, i - 1, j, '.');
  }

  cout << "grid length...";
  if (j < (grid[0].length() - 1) && getAt(grid, i, j + 1) != '0'){
    newGrid = setAt(newGrid, i, j + 1, '.');
  }
  cout << "ok" << endl;

  if (j >= 1 && getAt(grid, i, j - 1) != '0'){
    newGrid = setAt(newGrid, i, j - 1, '.');
  }
  
  return newGrid;
}

vector<string> blowUpZeroes(vector<string> grid){
  cout << "blowUpZeroes" << endl;    
  vector<string> newGrid = grid;

  int i = 0;
  for (auto s : grid){
    int j = 0;
    for (auto c : s){
      if (c == '0'){
	newGrid = explode(newGrid, i, j);
      }
      j++;
    }
    i++;
  }
  return newGrid;
}

vector<string> fillBombs(vector<string> grid){
  cout << "fillBombs" << endl;  
  vector<string> newGrid = grid;

  int i = 0;
  for (auto s : grid){
    int j = 0;
    for (auto c : s){
      if (c == '.'){
	newGrid = setAt(newGrid, i, j, '3');
      }
      j++;
    }
    i++;
  }
  return newGrid;
}


vector<string> fromRaw(vector<string> grid){
  cout << "fromRaw" << endl;  
  vector<string> newGrid = grid;

  int i = 0;
  for (auto s : grid){
    int j = 0;
    for (auto c : s){
      if (c == 'O'){
	newGrid = setAt(newGrid, i, j, '3');
      }
      j++;
    }
    i++;
  }
  return newGrid;
}


vector<string> toRaw(vector<string> grid){
  cout << "toRaw" << endl;
  vector<string> newGrid = grid;

  int i = 0;
  for (auto s : grid){
    int j = 0;
    for (auto c : s){
      if (c != '.'){
	newGrid = setAt(newGrid, i, j, 'O');
      }
      j++;
    }
    i++;
  }
  return newGrid;
}

vector<string> countDown(vector<string> grid){
  cout << "countDown" << endl;
  vector<string> newGrid = grid;

  int i = 0;
  for (auto s : grid){
    int j = 0;
    for (auto c : s){
      switch(c){
      case '3':
	newGrid = setAt(newGrid, i, j, '2');
	break;
      case '2':
	newGrid = setAt(newGrid, i, j, '1');
	break;
      case '1':
	newGrid = setAt(newGrid, i, j, '0');
	break;	
      case '0':
	cout << "WARNING: Found 0 at " << i << ", " << j << endl;
	break;
      case '.':
	// Do nothing
	break;
      default:
	cout << "WARNING: Found " << c << " at " << i << ", " << j << endl;
      }
      j++;
    }
    i++;
  }
  return newGrid;
}

vector<string> bomberMan(int n, vector<string> grid){
  dump(grid, n);
  
  // Initially, Bomberman arbitrarily plants bombs in some of the cells,
  // the initial state.
  vector<string> newGrid = fromRaw(grid);
  dump(newGrid, n);

  // After one second, Bomberman does nothing.
  n -= 1;
  newGrid = countDown(newGrid);
  dump(newGrid, n);

  if (0 == n){
    newGrid = toRaw(newGrid);
    dump(newGrid, n);
    return newGrid;
  }

  // After one more second, Bomberman plants bombs in all cells without bombs,
  // thus filling the whole grid with bombs. No bombs detonate at this point.
  n -= 1;
  newGrid = countDown(newGrid);
  dump(newGrid, n);

  newGrid = fillBombs(newGrid);
  dump(newGrid, n);

  if (0 == n){
    newGrid = toRaw(newGrid);
    dump(newGrid, n);
    return newGrid;
  }

  // After one more second, any bombs planted exactly three seconds ago
  // will detonate. Here, Bomberman stands back and observes.
  n -= 1;
  newGrid = countDown(newGrid);
  dump(newGrid, n);

  newGrid = blowUpZeroes(newGrid);
  dump(newGrid, n);

  if (0 == n){
    newGrid = toRaw(newGrid);
    dump(newGrid, n);
    return newGrid;
  }

  // Bomberman then repeats steps 3 and 4 indefinitely.
  bool filling = true;
  while (n > 0){
    n -= 1;
    newGrid = countDown(newGrid);
    if (filling){
      newGrid = fillBombs(newGrid);
      filling = false;
    } else {
      newGrid = blowUpZeroes(newGrid);
      filling = true;
    }
    dump(newGrid, n);
  }
  
  newGrid = toRaw(newGrid);
  dump(newGrid, n);
  return newGrid;
}


int main()
{
  vector<string> grid =
    {
     ".......",
     "...O...",
     "....O..",
     ".......",
     "OO.....",
     "OO....."
    };

  bomberMan(3, grid);
  
  return 0;
}
