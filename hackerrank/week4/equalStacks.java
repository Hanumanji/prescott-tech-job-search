import java.util.*;

class EqualStacks {

    public static void main(String [] args){
	List<Integer> h1 = List.of(1,1,1,1,2);
	List<Integer> h2 = List.of(3,7);
	List<Integer> h3 = List.of(1,3,1);
	
	System.out.println(equalStacks(h1, h2, h3));
    }
    
public static Set<Integer> makeSums(List<Integer> h){
        var s = new HashSet<Integer>();
        int l = h.size();
        int t = 0;
        for (int i = l - 1; i >= 0; --i){
            int v = h.get(i);
            s.add(v + t);
            t += v;
        }
        return s;
    }
    
    public static int equalStacks(List<Integer> h1, List<Integer> h2, List<Integer> h3) {
    // Write your code here

    var s1 = makeSums(h1);
    var s2 = makeSums(h2);
    var s3 = makeSums(h3);

    s1.retainAll(s2);
    s1.retainAll(s3);
    
    try {
	return Collections.max(s1);
    }
    catch (NoSuchElementException e){
	return 0;
    }
}

}
