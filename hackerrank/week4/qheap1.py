#!/usr/bin/python3

import fileinput
from heapq import *

h = []
d = {}

for i, line in enumerate(fileinput.input()):
    if i > 0:
        q = line.split()
        if q[0] == '1':
            v = int(q[1]) # value
            r = [v, True] # record = [value, alive?]
            heappush(h, r)
            d[v] = r
        elif q[0] == '2':
            v = int(q[1]) # value
            r = d.pop(v)
            r[1] = False
        elif q[0] == '3':
            v, a = h[0]
            popped = False
            while not a:
                v, a = heappop(h)
                popped = True
            if popped:
                heappush(h, [v, a])
                
            print(v)
        else:
            print(h)

