#!/usr/bin/python3

# How about just
# r = [(1,0),(10,0)]
# Add [(1,3),(5,3)]
#
def add(q1, q2):
    a1, b1, k1 = q1
    a2, b2, k2 = q2
    if a2 > b1 or b2 < a1:
        return [q1, q2]
    
    # q3, q4, q5

    q3 = (a3, b3, k3) = (a1, a2, k1) if a1 <= a2 else (a2, a1, k2)
    q4 = (a4, b4, k4) = (b3, b1, k1 + k2) if b1 <= b2 else (b3, b2, k1 + k2)
    q5 = (a5, b5, k5) = (b1, b2, k2) if b1 <= b2 else (b2, b1, k1)
    
    return [q3, q4, q5]

#            [(1, 10, 0)]
# (1, 5, 3)  [(1, 5, 3), (6, 10, 0)]
# (4, 8, 7)  [(1, 3, 3), (4, 5, 10), (6, 8, 7), (9, 10, 0)]
# (6, 9, 1)  [(1, 3, 3), (4, 5, 10), (6, 8, 8), (9, 9, 1), (10, 10, 0)]

def add_to_range(r, e):
    new_r = []
    ae, be, ke = e

    added = False
    for p in r:
        ap, bp, kp = p

        if ae > bp:
            # If new entry is completely after this entry
            # Just push this entry
            new_r.append(p)
        elif be < ap:
            # If new entry is completely before this entry
            new_r.append(e)
            new_r.append(p)
            added = True
        else:
            # Intersection...woohoo!
            
            if ae == ap:
                # Start same, so max of two entries
                added = True
                if be == bp:
                    # End the same, so only one entry!  Woot!
                    new_r.append([ae, ap, ke + kp])
                elif be < bp:
                    # New entry ends before current
                    # (1, 5), (1,10) -> (1,5),(6,10)
                    new_r.append([ae, be, ke + kp])
                    new_r.append([be + 1, bp, kp])
                else:
                    assert be > bp
                    new_r.append([ae, bp, ke + kp])
                    new_r.append([bp + 1, be, ke])
                
            elif ae < ap:
                if be == bp:
                    # End the same, so only two entries
                    # (1,10),(5,10)
                    new_r.append([ae, ap - 1, ke])
                    new_r.append([ap, bp, ke + kp])
                    
                elif be < bp:
                    # (1,3,3), (2,4,4)
                    # (1,1,3),(2,3,7),(4,4,4)
                    new_r.append([ae, ap - 1, ke])
                    new_r.append([ap, be, ke + kp])
                    new_r.append([be + 1, bp, kp])
                else:
                    assert be > bp
                    new_r.append([ae, ap - 1, ke])
                    new_r.append([ap, bp, ke + kp])
                    new_r.append([bp + 1, be, ke])

            else:
                assert ae > ap

                if be == bp:
                    # End the same, so only two entries
                    # (1,10),(5,10)
                    new_r.append([ap, ae - 1, kp])
                    new_r.append([ae, be, ke + kp])
                    
                elif be < bp:
                    # (1,3,3), (2,4,4)
                    # (1,1,3),(2,3,7),(4,4,4)
                    new_r.append([ae, ap - 1, ke])
                    new_r.append([ap, be, ke + kp])
                    new_r.append([be + 1, bp, kp])
                else:
                    assert be > bp
                    new_r.append([ae, ap - 1, ke])
                    new_r.append([ap, bp, ke + kp])
                    new_r.append([bp + 1, be, ke])

        
        added = True

    if not added:
        new_r.append(e)
        
    return new_r

if __name__ == "__main__":
#    inputs = [
#        ([1,7,5], [2,8,3]),
#        ([1,10,0],[1,5,3]),
#        ([1, 5, 3],[4,8,7]),
#        ]
#    for input in inputs:
#        print(input, add(*input))
#
    inputs = [
        ([], [1,10,0]),
        ([[1,10,0]], [1,5,3]),        
        ]
    for input in inputs:
        print(input, add_to_range(*input))
    
