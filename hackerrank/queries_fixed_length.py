#!/usr/bin/python


def solve(arr, queries):
    # Write your code here
    l = len(arr)
    if not l: return 0
    
    mi = []
    for q in queries:
        rmin = cmax = max(arr[:query])
        for i in range(1, l - q + 1):
            cmax = max(arr[i:i + q]) if cmax == arr[i - 1] else max(arr[i + q - 1], cmax)
            rmin = min(cmax, rmin)

        mi.append(rmin)
        
    return mi
