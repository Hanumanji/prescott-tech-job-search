# long roadsAndLibraries(int n, int c_lib, int c_road, vector<vector<int>> cities) {
#     if (c_lib <= c_road) return (long) n * c_lib;
#     vector<vector<int>> graph(n);
#     for (const vector<int>& e : cities) {
#         const int i = e[0] - 1;
#         const int j = e[1] - 1;
#         graph[i].push_back(j);
#         graph[j].push_back(i);
#     }
#     vector<int> cc(n);
#     long cost = 0L;
#     int cnt = 1;
#     for (int i = 0; i < n; ++i) {
#         if (cc[i]) continue;
#         cc[i] = cnt;
#         queue<int> q{ { i } };
#         while (q.size()) {
#             for (int x : graph[q.front()])
#                 if (!cc[x]) {
#                     cc[x] = cnt;
#                     q.push(x);
#                     cost += c_road;
#                 }
#             q.pop();
#         }
#         cost += c_lib;
#         ++cnt;
#     }
#     return cost;
# }
