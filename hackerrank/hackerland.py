#!/usr/bin/python

import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)
        
def hackerland(x, k):
    """
    >>> hackerland([1,2,3,5,9],1)
    3

    >>> hackerland([1,2,3,4,5],1)
    2

    >>> hackerland([9, 5, 4, 2, 6, 15, 12], 2)
    4

    """
    x.sort()
    
    pt = (x[0], x[0])  # (radio_house, lowest_covered_house)
    n = 1
    for i in range(1, len(x)):
        ch = x[i]
        log(f"i {i} ch {ch} n {n} pt {pt}")
        
        rh, lh = pt
        if ch - rh > k:  # If current house not in range of radio house
            n += 1
            pt = (ch, ch)
        elif ch - lh <= k:  # If current house still in range of lowest house, update radio house
            pt = (ch, lh)
            
    return n


if __name__ == "__main__":
    import doctest
    doctest.testmod()
