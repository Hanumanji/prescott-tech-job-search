#!/usr/bin/python

from heapq import *

def cookies(k, A):
    # Write your code here
    heapify(A)
    o = 0
    while A[0] < k and len(A) >= 2:
        a, b = heappop(A), heappop(A)
        heappush(A, a + (2 * b))
        o += 1

    print(f"first {A[0]} k {k} o {o}")
    
    return -1 if A[0] < k else o

print(cookies(105823341, [1] * 100000))
