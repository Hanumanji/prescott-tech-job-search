#!/usr/bin/python

from bisect import *


def climbingLeaderboard(ranked, player):
    rank = []
    prev = None
    i = 0
    for r in ranked:
        if r != prev:
            i += 1
            rank.append(i)
            prev = r
        else:
            rank.append(i)

    #print(f"rank {rank} for ranked {ranked}")

    rank = list(reversed(sorted(rank)))
    ranked = list(sorted(ranked))
    
    #print(f"sorted rank {rank} for sorted ranked {ranked}")    

    pr = []
    for p in player:
        i = bisect(ranked, p)
        #print(f"player {p} index {i}")
        if i == len(rank):
            #print(f"rank 1!")
            pr.append(1)
        else:
            pe, pi = e = ranked[i], rank[i]
            #print(f"rank {e}")
            if p < pe:
                pr.append(pi + 1)
            elif p == pe:
                pr.append(pi)
                
    return pr








expects = [
    (([100, 100, 50, 40, 40, 20, 10], [5, 25, 50, 120]), [6, 4, 2, 1]),
    (([100, 90, 90, 80, 75, 60], [50, 65, 77, 90, 102]), [6, 5, 4, 2, 1]),
    ]
for input, expected in expects:
    actual = climbingLeaderboard(*input)
    assert actual == expected, f"Actual {actual} not equal to expected {expected}"    
