#!/usr/bin/python
def log(*input):
    print(*input)
    pass

class Path:
    def __init__(self, grid, start, goal):
        assert start and len(start) == 2, "Need (i, j) pos for start"
        assert goal and len(goal) == 2, "Need (i, j) pos for goal"
        
        self.grid = grid
        
        self.visited = set()
        self.visited.add(start)
        self.path = [start]
        self.moves = [] # Deltas from start

        self.goal = goal

    def __repr__(self):
        return f"S {self.start()} G {self.goal} P {self.path} M {self.moves}"
    
    def copy(self):
        c = Path(self.grid, self.start(), self.goal)
        c.visited = self.visited.copy()
        c.path = self.path.copy()
        c.moves = self.moves.copy()
        return c

    def start(self):
        return self.path[0]
    
    def current(self):
        return self.path[-1]

    def isBlocked(self, pos):
        i, j = pos
        if i < 0 or i >= len(self.grid) or j < 0 or j >= len(self.grid[0]):
            return True
        
        return self.grid[i][j] == 'X'

    def isDone(self):
        return self.current() == self.goal

    def lastMove(self):
        return None if not self.moves else self.moves[-1]

    def delta(self, p1, p2):
        i1, j1 = p1
        i2, j2 = p2
        return i2 - i1, j2 - j1

    def extendX(self, dir):
        assert(dir in (-1, 1))

        i, j = self.current()
        return self.extend((i + dir, j))

    def extendY(self, dir):
        assert(dir in (-1, 1))

        i, j = self.current()
        return self.extend((i, j + dir))
    
    # Return new Path if possible, else None
    def extend(self, pos):
        log(f"Extend {pos} path {self.path}")
        
        if self.isBlocked(pos):
            log(f"Blocked {pos}")
            return None

        if pos in self.visited:
            log(f"Already visited {pos}")
            return None

        p = self.copy()
        
        p.visited.add(pos)

        c = p.current()

        p.path.append(pos)
        log(f"Added {pos} to {p.path}")
        n = p.delta(c, pos)
        
        m = p.lastMove()
        if m is None:
            log(f"Move append {n}")            
            p.moves.append(n)
        else:
            if m[0] == 0 and n[0] == 0:
                # Replace j
                r = (0, m[1] + n[1])
                log(f"Move replace j {m} {n} r {r}")
                p.moves[-1] = r
            elif m[1] == 0 and n[1] == 0:
                # Replace i
                r = (m[0] + n[0], 0)                
                log(f"Move replace i {m} {n} r {r}")
                p.moves[-1] = r
            else:
                log(f"Move append {n}")
                p.moves.append(n)

        return p

    def find(self):
        log(f"Current {self.current()} goal {self.goal}")
        if self.isDone():
            log(f"Done! {self}")
            return self
        
        a = self.extendY(-1), self.extendY(1), self.extendX(-1), self.extendX(1)
        b = filter(lambda x: x is not None, a)
        c = map(lambda x: x.find(), b)
        d = list(filter(lambda x: x is not None, c))
        log(d)
        
        dir = min(d, key = lambda x: len(x.moves)) if d else None
        #dir = min(d, key = lambda x: len(x.moves)) # if d else None        

        log(dir)
        
        return dir
        
def minimumMoves(grid, startX, startY, goalX, goalY):
    p = Path(grid, (startX, startY), (goalX, goalY))

    g = p.find()

    return len(g.moves)


s = """
.X..XX...X
X.........
.X.......X
..........
........X.
.X...XXX..
.....X..XX
.....X.X..
..........
.....X..XX
"""
l = [line for line in s.splitlines() if line]
print(f"l {l}")

expects = [
#    ((['...','.X.','...'], 0, 0, 1, 2), 2),
#    ((['.X.','.X.','...'], 0, 0, 0, 2), 3),
    ((l, 9, 1, 9, 6), 3),
    ]
for input, expected in expects:
    actual = minimumMoves(*input)
    print("SUCCESS" if actual == expected else "FAILURE",
          f"actual {actual}",
          f"expected {expected}")
