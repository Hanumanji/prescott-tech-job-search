#!/usr/bin/python


def setAt(s, c, i):
    assert(i >= 0 and i < len(s))
    return s[:i] + c + s[i+1:]

def highestValuePalindrome(s, n, k):
    # Write your code here
    npd = set() # non-palindrome deltas from start/end
    # len 4 m = 4 // 2 = 2  1223   [0]
    # len 5 m = 5 // 2 = 2  12733  [0, 1]
    m = n // 2
    e = len(s) - 1
    if not e:
        # 1 char string
        return '9'
    
    
    for i in range(m):
        if s[i] != s[e - i]:
            npd.add(i)
        
    if k < len(npd):
        return '-1'
    
    # Need 1 from k for each member of npd
    # Now, what to do with extra k?
    # If have 2 k, then want to turn outside into 9s
    x = k - len(npd)    # This is extra

    # If d 0 exists, want to spend 2 k to switch to 9s (although might only cost 1k)
    # If d 0 does not exist, and have x >= 2, change both d 0 to 9s
    
    for i in range(m):
        a, b = s[i], s[e - i]
        if i in npd:
            maxed = False
            if a == '9' or b == '9':
                maxed = True
                
            if x and not maxed:
                # Have extra, so use it to set both to 9 if necessary
                s = setAt(s, '9', i)
                s = setAt(s, '9', e - i)
                x -= 1
            elif a > b:
                s = setAt(s, a, e - i)
            elif b > a:
                s = setAt(s, b, i)
            else:
                assert 0, "Should never get here!"
        elif x >= 2 and a != '9':
            # Have extra, so use it to set ends to 9s
            s = setAt(s, '9', i)
            s = setAt(s, '9', e - i)
            x -= 2

    if len(s) % 2 and x and s[m] != '9':
        s = setAt(s, '9', m)
        x -= 1  # Not really necessary but...true.
  
    return s

expects = [
    (('932239', 6, 2), '992299'),
    (('12321', 5, 1), '12921'),
]
for input, expected in expects:
    actual = highestValuePalindrome(*input)
    print(f"{'SUCCESS' if actual == expected else 'FAILURE'} actual {actual} expected {expected}")
    
