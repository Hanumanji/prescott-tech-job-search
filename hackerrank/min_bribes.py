#!/usr/bin/python

def minimumBribes(q):
    # Write your code here
    t = 0
    tc = False
    for i in reversed(range(len(q))):
        v = q[i]
#        print(f"Found {v} at index {i}")
        if v == i + 1:
            q.pop(i)
#            print(f"Popped {i} from q {q}")
        else:
            b = 0
            pi = i
            while q[pi] != i + 1:
                b += 1
                pi -= 1    
                if b > 2:
                    tc = True  
#                print(b, pi, tc)
            q.pop(pi)
            t += b
#    print(tc, t)
    print("Too chaotic" if tc else t)

minimumBribes([2,1,5,3,4])
minimumBribes([2,5,1,3,4])
