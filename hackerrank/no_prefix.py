#!/usr/bin/python

class Trie(object):
    children : dict
    isWord : bool = False
    
    def __repr__(self):
        return f"children {self.children.keys()} isWord {self.isWord}"
    
    def __init__(self):
        self.children = {}

    def add(self, word, prefixFail = False):
        n = self
        for c in word:
            if not c in n.children:
                n.children[c] = Trie()
                
            n = n.children[c]
            if prefixFail and n.isWord:
                return False
            
        n.isWord = True
        return not n.children if prefixFail else True

def noPrefix(words):
    # Write your code here
    root = Trie()
    bs = False
    for word in words:
        if not root.add(word, True):
            bs = True
            print("BAD SET")
            print(word)
            return
    if not bs:
        print("GOOD SET")


noPrefix(['abcd','bcd','abcde','bcde'])
