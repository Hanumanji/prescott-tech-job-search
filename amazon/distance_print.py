#!/usr/bin/python3.10

# Thanks for your time, Dmitry
# I know that wasn't my best coding interview.
#
# Once I calmed down a bit, I revisited, and realized:
# I can do the whole thing with O(n) space and O(2*n) traversals worst-case
# within one function. (You asked me to remove the O(logn) space requirement
# for the path)
#
# The idea that I started with (that I did not manage well in our communication)
# was to pass down a distance until we find the givenNode, then continue down knowing
# the distance, and also rolling back up with the return since we know the distance
# on the path.
#
# The other change was once we know a child has found the givenNode (because it returns
# a valid non-negative distance), then we can go ahead back down the other side with
# the now-found distance.
#
# So worst-case, we traverse the tree twice.
#
# You can just run the file from command line, and should give no output if doctests pass.
#
# $ ./distance_print.py

# ORIGINAL PROBLEM:
# You have a binary tree. Each node described by value and link for left and right child.
#
# Wright a function that takes tree root, node and distance as arguments and prints values
# of all nodes on given distance from a given node.

import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)
        
class TreeNode:
    def __init__(self, v, left = None, right = None):
        self.value = v
        self.left = left
        self.right = right

def distance(root: TreeNode, givenNode: TreeNode, distance: int):
    """
    >>> Tr = TreeNode
    >>> gn = Tr(8, Tr(13), Tr(-9))  # givenNode (children will be 1 away)
    >>> root = Tr(3, Tr(4), Tr(5, Tr(7, gn, Tr(9)), Tr(10, Tr(11), Tr(12))))
    >>> distance(root, gn, 0)  # make sure self is found
    8
    >>> distance(root, gn, 1)  # make sure children are found
    13
    -9
    7
    >>> distance(root, gn, 2)
    9
    5
    >>> distance(root, gn, 3)
    10
    3
    >>> distance(root, gn, 4)
    11
    12
    4
    >>> gn = root.right
    >>> distance(root, gn, 0)
    5
    >>> distance(root, gn, 1)
    7
    10
    3
    >>> distance(root, gn, 2)
    8
    9
    11
    12
    4
    >>> distance(root, gn, 3)
    13
    -9
    >>> distance(root, gn, 4)  # Nothing 4 or more away
    >>> distance(root, root, 0)
    3
    >>> distance(root, root, 3)
    8
    9
    11
    12
    >>> gn = root.right.right.right
    >>> distance(root, gn, 5)
    13
    -9
    """
    # once find givenNode, distance increases down,
    # can print children distance
    # when unwrap to top, can print distance

    # Only add to v when have a dist
    v = set()
    def dfsDist(node: TreeNode, dist: int = -1) -> int:
        myD = dist
        if not node or node in v:
            return myD

        if node == givenNode:
            myD = 0

        childD = dfsDist(node.left, myD + 1 if myD != -1 else -1)
        if myD == -1 and childD != -1:
            myD = childD + 1
            # Go back down now have distance, won't revisit thanks to v
            dfsDist(node.right, myD + 1)
            

        childD = dfsDist(node.right, myD + 1 if myD != -1 else -1)
        if myD == -1 and childD != -1:
            myD = childD + 1
            # Go back down now have distance, won't revisit thanks to v
            dfsDist(node.left, myD + 1)

        if myD != -1:
            # Only mark visited if had valid (not -1) distance
            v.add(node)
            
        if myD == distance:
            print(node.value)

        # Propagate distance back up for parents
        return myD

    dfsDist(root)

    
if __name__ == "__main__":
    import doctest
    doctest.testmod()
