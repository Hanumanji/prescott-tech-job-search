#!/usr/bin/python3.10

# The Amazon robotics team is developing a robot for use at a warehouse for the purpose of sorting packages such that
# all packages of the same size are adjacent and packages are sorted in descending order of their size.
# Think of a way to represent this and write a program to sort the packages according to the provided requirements.

# Valid assumptions:
# The robot knows the size of each package,
# the total number of packages before they’re placed on the sorting line, 
# and the line is not moving
# 

# 
# 3 sizes: b, m, s  

# after: [B] * 4, [M] * 2, [S] * 3 

# b > m > s

# before: [B, M, S, B, M ,S , B, B, S]

# ["b", "m", "s"]

# Building on quicksort (ie not mergesort or other...)

# Updating algoritm to binsort for 3 bins: b at beginning, s at end, m everywhere else
#
import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)
        
B, M, S = "b", "m", "s"

# O(n) algorithm, since bucketing at beginning for Big, end for Small
def robot_sort(input):
    """
    >>> input = [B, M, S, B, M ,S , B, B, S]
    >>> robot_sort(input)
    >>> input == [B] * 4 + [M] * 2 + [S] * 3
    True
    """
    bi, si = 0, len(input) - 1 # big and small indices: last b, first s

    while input[bi] == B:
        bi += 1
        
    while input[si] == S:
        si -= 1

    log(f"bi {bi} si {si} input {input}")

    i = bi
    while i <= si:
        p = input[i]
        # Might swap B with S or S with B, so put in loop...
        while p != M:
            if p == B:
                input[i], input[bi] = input[bi], input[i]
                bi += 1
            elif p == S:
                input[i], input[si] = input[si], input[i]
                si -= 1
            p = input[i]

            log(f"i {i} p {p} bi {bi} si {si} input {input}")            
        i += 1
        
if __name__ == "__main__":
    import doctest
    doctest.testmod()
