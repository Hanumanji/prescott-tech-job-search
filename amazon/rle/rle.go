package main

import (
	"fmt"
	"strings"
	"regexp"
	"errors"
	"strconv"
)

func rle(s string) string {
	runs := []string{}
	if len(s) != 0 {
		p := rune(s[0])
		n := 1
		add := func (){
			run := fmt.Sprintf("%d%c", n, p)
			runs = append(runs, run)
		}
		for _, c := range s[1:] {
			if c == p {
				n++
			} else {
				add()
				p = c
				n = 1
				
			}
		}
		add()
	}
	return strings.Join(runs, "")
}

func rld(d string) (string, error) {
	// data = re.split('(\D+)', encoded)
	re_chars := regexp.MustCompile(`(\d+)`)
	re_nums := regexp.MustCompile(`(\D+)`)	
	chars := re_chars.Split(d, -1)
	nums := re_nums.Split(d, -1)
	//fmt.Println("chars:", chars, "nums:", nums)
	
	if len(chars) != len(nums) {
		return "", errors.New(fmt.Sprintf("Invalid rle string %s", d))
	}
	runs := []string{}
	for i, c := range(chars[1:]) {
		if nums[i] == "" {
			continue
		}
		if n, err := strconv.Atoi(nums[i]); err != nil {
			return "", err
		} else {
			runs = append(runs, strings.Repeat(string(c), n))
		}
	}
	// fmt.Println("runs: ", runs)
	
	return strings.Join(runs, ""), nil
}

func main(){
	if s, err := rld(rle("rle/rld successful!")); err == nil {
		fmt.Println(s)
	} else {
		fmt.Println("err: ", err)
	}
}

