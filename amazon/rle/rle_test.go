package main

import (
	"testing"
)

var raw = "Hello"
var enc = "1H1e2l1o"

func TestRLE(t *testing.T){
	if out := rle(raw); out != enc {
		t.Fatalf("%q rle output %q not equal to expected %q", raw, out, enc)
	}
}

func TestRLD(t *testing.T){
	if out, _ := rld(enc); out != raw {
		t.Fatalf("%q rld output %q not equal to expected %q", enc, out, raw)
	}
}

func TestRLDErr(t *testing.T){
	if _, err := rld(raw); err == nil {
		t.Fatalf("%q rld err expected", raw)
	}
}
