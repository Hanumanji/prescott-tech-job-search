#!/usr/bin/python3.10

import re

# run length encoding
def rle(data: str) -> str:
    """
    >>> rle("a" * 4 + "b" * 2 + "c" * 3)
    '4a2b3c'
    >>> rle("a" for _ in range(1000))
    '1000a'
    """
    res = []
    p = None # Previous character
    n = 0    # count
    for c in data:
        if c == p:
            n += 1
        else:
            if p is not None:
                res.append(f"{n}{p}")
            # c != p, move on to next character

            p = c
            n = 1

    if p is not None:
        res.append(f"{n}{p}")

    return "".join(res)

# run length decoding
def rld(encoded: str) -> str:
    """
    >>> rld(rle("a" * 4 + "b" * 2 + "c" * 3))
    'aaaabbccc'
    """

    data = re.split('(\D+)', encoded)
    del data[-1] # Remove trailing empty string
    l = len(data)
    assert l % 2 == 0  # Must be even!

    res = []
    for i in range(0, l - 1, 2):
        n, c = data[i], data[i + 1]
        res.append(c * int(n))

    return "".join(res)

if __name__ == "__main__":
    import doctest
    doctest.testmod()
