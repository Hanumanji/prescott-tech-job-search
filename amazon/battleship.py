#!/usr/bin/python3.10

"""
To play a game of Battleship with your friends, just run this file:

$ ./battleship.py

If you want to run a TestGame with ships already in place, you can run:

$ ./battleship.py test

If you want to just jump right to the end game, with almost all ships sunk:

$ ./battleship.py sunk


NOTE: Since everything stored in hashes by position, game space is NOT determined by Grid size,
but rather by number of ships in shipSet for players, and then the total number of hits/misses 
within the grid per player (although this could approach Grid size for some games).

OTHERWISE: Thanks for the challenge!

Design the game Battleships

Battleship is a guessing game for two players. Each player owns one 10x10 grid and a fleet of ships of different lengths.
On the grid the player arranges ships and records the shots by the opponent. Design the game Battleship.

Before the game begins, each player secretly arranges their ships on their main grid. Each ship occupies a number of
consecutive squares on the grid, arranged either horizontally or vertically. The number of squares for each ship is
determined by the type of the ship. The ships cannot overlap (i.e., only one ship can occupy any given square in the grid).
The types and numbers of ships allowed are the same for each player.

After the ships have been positioned, the game proceeds in a series of rounds. In each round, each player takes a turn to
announce a target square in the opponent's grid which is to be shot at.

When all of the squares of a ship have been hit, the ship is sunk. If all of a player's ships have been sunk, the game is
over and their opponent wins.

Type of ship        Size    Quantity
-------------------------------------
Aircraft Carrier    5           1
Battleship          4           1
Submarine           3           1
Destroyer           3           2
Patrol boat         2           2
-------------------------------------

Classes/Objects
Game (Battleship)
  Current Player
  
Player
Ship
  Subclasses, or just data differences
  Size
  Damage per size increment
  Track Hits on Ships
ShipSet
  all the ships per player
Grid
   n*n (always square?)
   where the ships go
   per player
   Track Misses on grid 
Pos
   Used for Ship location on Grid
   Used for Misses on Grid
Orientation
    Horiz or Vertic H, V
Algo:
    Game starts
    2 players
    Player 1 puts all ships in shipset onto grid
    Player 2 ...sSAA.,..
    Player starts
      Chooses a location not shot before 
      Game evaluates shot, if Hit, damages ship
      If CompletelyDamaged, then we label as Sunk.
      If all shps are Sunk, then other Player wins.
"""      

from enum import Enum, auto

import os

def cls():
    os.system('cls' if os.name=='nt' else 'clear')

class Orientation(Enum):
    UNKNOWN = auto()
    HORIZONTAL = auto()
    VERTICAL = auto()
    
class Shot(Enum):
    UNKNOWN = auto()
    ALREADY_MISS = auto()
    HIT = auto()
    MISS = auto()
    ALREADY_HIT = auto()
    
class Ship:
    # How do we display ship in Grid?
    def __init__(self, size):
        assert size >= 2 and size <= 5  # Is this even necessary?
        self.size = size
        self.health = size

    def __repr__(self):
        return f"{type(self).__name__}({self.health}/{self.size})"
    
    def isSunk(self):
        return self.health == 0
        
    def hit(self):
        assert self.health > 0
        self.health -= 1

class AircraftCarrier(Ship):
    def __init__(self):
        super().__init__(5)
        
class Battleship(Ship):
    def __init__(self):
        super().__init__(4)

class Submarine(Ship):
    def __init__(self):
        super().__init__(3)

class Destroyer(Ship):
    def __init__(self):
        super().__init__(3)

class PatrolBoat(Ship):
    def __init__(self):
        super().__init__(2)

class Grid:
    def __init__(self, m = 10, n = 10):
        self.m = m
        self.n = n
        self.ships = {}  # {pos: Ship}
        self.shots = {} # {pos: Shot}
    
    def validPos(self, pos: tuple) -> bool:
        x, y = pos
        return x >= 0 and x <= self.m and y >= 0 and y <= self.n
    
    def _iGetShipPositions(self, pos:tuple, length:int, orientation: Orientation):
        assert orientation != Orientation.UNKNOWN
        x, y = pos
        yield pos
        for _ in range(length - 1):
            if orientation == Orientation.HORIZONTAL:
                x += 1
            else:
                y += 1
            yield x, y
    
    def getSize(self) -> tuple:
        return (self.m, self.n)
        
    def addShip(self, ship: Ship, initialPos: tuple, orientation) -> bool:
        # upper left corner initialPos, out to the right, or down
        # based on V or H

        # returns fals if can't put ship 
        # throw error if ship already on grid ?
        #
        # IMPL DETAIL
        # Add ALL Positions for ship to self.ships
        
        intended = list(self._iGetShipPositions(initialPos, ship.size, orientation))
        for pos in intended:
            if not self.validPos(pos) or self.shipAt(pos):
                return False
            
        for pos in intended:
            self.ships[pos] = ship

        return True
            
    def shoot(self, pos: tuple) -> Shot:
        assert self.validPos(pos)
        
        # Puts Damage on Ship, or puts Miss on Grid
        if pos in self.shots:
            return Shot.ALREADY_HIT if self.shots[pos] == Shot.HIT else Shot.ALREADY_MISS

        ship = self.shipAt(pos)
        shot = Shot.MISS
        if ship:
            ship.hit()
            shot = Shot.HIT

        self.shots[pos] = shot
        return shot
        
    def shipAt(self, pos: tuple) -> Ship:
        # returns ship at pos
        assert self.validPos(pos)
        return self.ships.get(pos, None)
        
class Player:
    def __init__(self, shipSet, grid = None):
        self.shipSet = shipSet
        self.grid = grid if grid else Grid()
        
class Game:
    def __init__(self):
        self.players = [Player(self.newShipSet(), self.newGrid()) for _ in range(2)]

    def newShipSet(self):
        return set((AircraftCarrier(),
                    Battleship(),
                    Submarine(),
                    Destroyer(),
                    Destroyer(),
                    PatrolBoat(),
                    PatrolBoat()))

    def newGrid(self):
        return Grid()

    def printPlayerGrid(self, index):
        cls()
        other = (index + 1) % 2
        
        print(f"Player Grid {index}")
        grid = self.players[index].grid
        m, n = grid.getSize()

        print("   ", end="")
        for x in range(m):
            print(f"{x:02d} ", end="")
        print("|", end="")
        for x in range(m):
            print(f"{x:02d} ", end="")
        print()
            
        for y in range(n):
            print(f"{y:02d} ", end="")

            # Print my grid, and their hits/misses on me
            grid = self.players[index].grid
            for x in range(m):
                c = "."
                pos = x, y
                ship = grid.shipAt(pos)
                if ship:
                    c = str(ship)[0]
                shot = grid.shots.get(pos, Shot.UNKNOWN)
                if shot == Shot.HIT:
                    c = "X"
                elif shot == Shot.MISS:
                    c = "M"
                    
                print(f" {c} ", end="")

            print("|", end="")

            # Print their grid, and my hits/misses on them
            # BUT NOT THEIR SHIPS!
            grid = self.players[other].grid            
            for x in range(m):
                c = "."
                pos = x, y
                shot = grid.shots.get(pos, Shot.UNKNOWN)
                if shot == Shot.HIT:
                    c = "X"
                elif shot == Shot.MISS:
                    c = "M"
                    
                print(f" {c} ", end="")
            print()
            
    def displayPlayer(self, index):
        self.printPlayerGrid(index)

    def setupPlayer(self, index):
        print(f"setupPlayer {index}")
        for ship in self.players[index].shipSet:
            self.placePlayerShip(index, ship)

    def placePlayerShip(self, index, ship):
        grid = self.players[index].grid
        valid = False
        while not valid:
            self.printPlayerGrid(index)
            p = input(f"Where would you like to place ship {ship}, and (H)orizontal/(V)ertical? (ie 5 3 H)  > ")
            x, y, o = p.split(" ")
            pos = int(x), int(y)
            
            if o == "H":
                o = Orientation.HORIZONTAL
            elif o == "V":
                o = Orientation.VERTICAL
            else:
                o = Orientation.UNKNOWN
                
            if grid.validPos(pos) and o != Orientation.UNKNOWN:
                success = grid.addShip(ship, pos, o)
                if not success:
                    input(f"Colllision or out of bounds at {pos}, {o} for {ship}...try again")
                else:
                    valid = True
            else:
                input(f"Invalid position {pos}, orientation {o} for {ship}...try again")

    def passPlayer(self):
        cls()
        input("PASS TO OTHER PLAYER")
        
    def playerShoot(self, index) -> Shot:
        print(f"playerShoot {index}")
        
        other = (index + 1) % 2
        
        grid = self.players[other].grid

        while True:
            p = input("Where would you like to shoot? (ie 5 3)  > ")
            if not p:
                continue
            try:
                pos = tuple(int(n) for n in p.split(" "))
            except:
                continue
            if len(pos) != 2:
                continue
            
            print(f"Shooting at {pos}")

            shot = grid.shoot(pos)
            if shot == Shot.ALREADY_HIT:
                input(f"Already hit at {pos}...try again")
            elif shot == Shot.ALREADY_MISS:
                input(f"Already miss at {pos}...try again")
            elif shot == Shot.HIT:
                self.printPlayerGrid(index)                
                input(f"HIT at {pos}...woohoo!")
                ship = grid.shipAt(pos)
                if ship.isSunk():
                    input(f"You sunk their {ship}!")
                return shot
            elif shot == Shot.MISS:
                self.printPlayerGrid(index)                
                input(f"Aw...you missed at {pos}")
                return shot
        
        return Shot.UNKNOWN

    def setupPlayers(self):
        self.setupPlayer(0)
        self.passPlayer()
        self.setupPlayer(1)
        self.passPlayer()

    def playerAllSunk(self, index):
        player = self.players[index]
        for ship in player.shipSet:
            if not ship.isSunk():
                return False
        return True

    def displayWinner(self, index):
        print(f"CONGRATULATIONS PLAYER {index}!!! You win!")
        
    def run(self):
        self.setupPlayers()
        winner = None
        current, other = 0, 1
        while winner is None:
            self.displayPlayer(current)
            if self.playerShoot(current) == Shot.HIT and self.playerAllSunk(other):
                winner = current
                self.displayWinner(winner)
                break
            else:
                self.passPlayer()
                
            current, other = other, current

        
class TestGame(Game):
    def __init__(self):
        super().__init__()
        self.setupPlayersFromData()
        
    def setupPlayerFromData(self, index, data):
        player = self.players[index]
        grid = player.grid
        shipSet = set()
        for ship, pos, o in data:
            # self.printPlayerGrid(index)
            assert grid.addShip(ship, pos, o), f"Invalid player {index} {ship} {pos} {o}"
            shipSet.add(ship)
        player.shipSet = shipSet

    def setupPlayers(self):
        # Do nothing, since will be done on initiailation for TestGame
        pass
    
    def setupPlayersFromData(self):
        H, V = Orientation.HORIZONTAL, Orientation.VERTICAL
        setupData0 = [
            (AircraftCarrier(), (0,0), H),
            (Battleship(), (6,0), V),
            (Submarine(), (0, 3), H),
            (Destroyer(), (8, 3), V),
            (Destroyer(), (0, 2), H),
            (PatrolBoat(), (0, 7), V),
            (PatrolBoat(), (6, 8), H),                                    
        ]
        setupData1 = [
            (AircraftCarrier(), (9, 4), V),
            (Battleship(), (1, 1), H),
            (Submarine(), (3, 3), V),
            (Destroyer(), (5, 5), H),
            (Destroyer(), (0, 0), V),
            (PatrolBoat(), (0, 5), H),
            (PatrolBoat(), (4, 6), V),                                    
        ]
        self.setupPlayerFromData(0, setupData0)
        self.setupPlayerFromData(1, setupData1)        

    def almostAllSunk(self):
        # Sink all ships but one, damage last ship but one, then output
        # position for last damage

        # We'll damage everything, keep last position, then undo the last damage
        for i in range(2):
            lastPos, lastShip = None, None
            grid = self.players[i].grid
            for pos, ship in grid.ships.items():
                assert grid.shoot(pos) == Shot.HIT
                lastPos, lastShip = pos, ship
                
            assert self.playerAllSunk(i)
            lastShip.health += 1
            del grid.shots[lastPos]
            assert not self.playerAllSunk(i)

            #self.printPlayerGrid(i)
            #input(f"All good {i}?")
            #self.passPlayer()
        
if __name__ == "__main__":
    import sys

    sunk = "sunk" in sys.argv
    test = sunk or "test" in sys.argv
    
    # Use 'test' for autosetup of players...
    game = TestGame() if test else Game()

    if sunk:
        game.almostAllSunk() # Use on TestGame to get to end game more quickly
    
    game.run()
