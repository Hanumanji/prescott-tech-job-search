
# neetcode

[Top 5 Dynamic Programming](https://www.youtube.com/watch?v=mBNrRy2_hVs)

[Spreadsheet](https://docs.google.com/spreadsheets/d/1pEzcVLdj7T4fv5mrNhsOvffBnsUH07GZk7c2jD-adE0/edit#gid=0)

# Fibonacci Numbers

F(0) = 0
F(1) = 1
F(n) = F(n-1) + F(n-2) for n >= 1


def fib_iter():
    a, b = 0, 1 # n - 2, n - 1
    while True:
    	  yield a
    	  a, b = b, a + b

# 0/1 Knapsack

coins = [1, 2, 3] target = 5
Use any coin 0 or 1 time

O(2**n) brute force

T/F pair (if use True vs if use False, where to go next)
Coins	Target	5     	 4		3	2	1	0
	1	(4,5)	 					T
	2	(3,5)	(2,4)					T
	3	(2,5)	(1,4)		(0,3)	(-1,2)		T

# Unbounded Knapsack

coins = [1, 2, 3] target = 5
Use any coin any # of times

T/F/N tuple (if use True vs if use False, where to go next, N is count of coins used)
Coins	Target	5     	 4		3	2	1	0
	1	(4,5,0) (3,4,1)		(2,3,2)	(1,2,3)	(0,1,4)	5
	2	(3,5,1)	(2,4,1)		(1,3,2)		(0,1,3)	4
	3	(2,5,1)	(1,4,1)		(0,3)	(-1,2)


Compute bottom up instead!

[coin change video](https://www.youtube.com/watch?v=H9bfqozjoqs&t=2s)

# [Longest Common Subsequence](https://www.youtube.com/watch?v=Ua0GhsJSlWM)

"abc" "ace"
      a		b	c
a				X
c				X
e				X
      X	        X       X	0

Either going diagonal, or down, or right

bottom up approach

# Palindromes

"racecar"

Start at any one character, then expand l/r one character
