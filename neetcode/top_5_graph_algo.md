# From https://www.youtube.com/watch?v=utDu3Q7Flrw

class TreeNode:
      def __init__(self, val, left = None, right = None):
      	  self.val = val
	  self.left = left
	  self.right = right

# Depth First Search (DFS)

Time: O(n)
DataStructures:
HashSet for detecting cycles

# Breadth First Search (BFS)

Time: O(n)
DataStructures:
Queue
HashSet for detecting cycles


# Union Find

Time: O(nlogn)
DataStructures:
Forest of Trees (parent array)


# Topological Sort

Time:
DataStructures:

# Dijkstra's Shortest Path Algo

Time: O(ElogV)
DataStructures:
Heap/PriorityQueue for finding minimum
HashSet for detecting cycles



-------------------------------------

# Prim's Minimum Spanning Tree

# Floyd Warshall's Algorithms