Prim's - minimum spanning tree
(leetcode/min_cost_connect.py)
O(V**2), but with adjacency list and binary heap, O(E log V)

Dijsktra's - shortest path from source vertex to all other vertices
(geeksforgeeks/dijsktras.py)
O(E+V log V)

Not good for at most k stops in graph traversal

topological sort
(geeksforgeeks/topological_sort.py)

boggle.py
(geeksforgeeks/boggle.py)

bridges.py
(geeksforgeeks/bridges.py)
u children v
low[u] = discovery[u] = step
if v not in visited:
  parent[v] = u
  bridge(v, step + 1)
  low[u] = min(low[u], low[v])

  if low[v] > discovery[u]:
     bridges.append((u, v))
     
elif parent[u] != v:
  low[u] = min(low[u], discovery[v])

unionfind
(leetcode/num_provinces.py)

bellman-ford
O(E*k) k stops
BFS
O(E*V)


# Data Structures

Trie
add(word)
contains(word)
prefix(word)