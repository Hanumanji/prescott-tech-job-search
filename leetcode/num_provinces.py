#!/usr/bin/python

# 547 on leetcode Number Of Provinces
# https://leetcode.com/problems/number-of-provinces/

# Use UnionFind
class UnionFind:
    def __init__(self, vals):
        self.pars = {val:val for val in vals}
        # union by rank
        self.rank = {val:1 for val in vals}
    def find(self, k):
        if self.pars[k] == k:
            return k
        p = self.find(self.pars[k])
        # path compression
        self.pars[k] = p
        return p
    def union(self, a, b):
        ap = self.find(a)
        bp = self.find(b)
        if self.rank[ap] > self.rank[bp]:
            self.pars[bp] = ap
        elif self.rank[ap] < self.rank[bp]:
            self.pars[ap] = bp
        else:
            # Same, so increment rank of parent
            self.pars[ap] = bp
            self.rank[bp] += 1
            return 0

        return 1
    def count(self):
        return sum(1 for val, par in self.pars.items() if val == par)
    
def findCircleNum(isConnected: list[list[int]]) -> int:
    # n x n isConnected
    n = len(isConnected)

    uf = UnionFind(range(n))
    
    # Use findUnion algorithm
    for r, row in enumerate(isConnected):
        for c, b in enumerate(row):
            if b:
                uf.union(r, c)

    return uf.count()
        

print(findCircleNum([[1,1,0],[1,1,0],[0,0,1]])) # 2
print(findCircleNum([[1,0,0],[0,1,0],[0,0,1]])) # 3
