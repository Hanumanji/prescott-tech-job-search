#!/usr/bin/python

import os
from heapq import *

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)

def swim(grid: list[list[int]]) -> int:
    """
    >>> swim([[0,2],[1,3]])
    3
    """
    N = len(grid)
    
    v = set() # visits
    mh = [(grid[0][0], 0, 0)] # minheap
    v.add((0, 0))
    
    while mh:
        h, r, c = heappop(mh)
        v.add((r, c))
        if r == N - 1 and c == N - 1:
            return h
        
        for dr, dc in (0,1), (0,-1), (1,0), (-1,0):
            nr, nc = r + dr, c + dc
            if nr >= 0 and nr < len(grid) and nc >= 0 and nc < len(grid[0]) and (nr, nc) not in v:
                heappush(mh, (max(h, grid[nr][nc]), nr, nc))
        
    assert 0, "Should never get here"
    return 0


if __name__ == "__main__":
    import doctest
    doctest.testmod()
