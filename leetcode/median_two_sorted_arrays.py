#!/usr/bin/python

import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)

def median(a: list[int], b: list[int]) -> int:
    """
                      M
    [0, 0, 1, 1, 2, 2, 3, 3, 4, 5, 6, 7]
    >>> median(list(range(4)), list(range(8)))
    2.5
    """
    la, lb = len(a), len(b)
    a, b = (a, b) if len(a) < len(b) else (b, a)
    total = la + lb
    half = total // 2
    
    l, r = 0, len(a) - 1
    while True:
        i = (l + r) // 2
        j = half - i - 2

        al = a[i] if i >= 0 else float("-infinity")
        ar = a[i + 1] if i + 1 < len(a) else float("infinity")
        bl = b[j] if j >= 0 else float("-infinity")
        br = b[j + 1] if j + 1 < len(b) else float("infinity")

        if al <= br and bl <= ar:
            # odd
            if total % 2:
                return min(ar, br)
            # even
            return (max(al, bl) + min(ar, br)) / 2.0
        elif al > br:
            r = i - 1
        else:
            l = i + 1

if __name__ == "__main__":
    import doctest
    doctest.testmod()
