#!/usr/bin/python

# Prim's algorithm -  https://www.youtube.com/watch?v=f7JOBJIC-NA
# leetcode 1584 Min Cost to Connect All Points

from heapq import *

# cost = manhattan distance = |xi - xj| + |yi - yj|
def min_cost_connect(points: list[list[int]]) -> int:
    """
    >>> min_cost_connect([[0,0],[2,2],[3,10],[5,2],[7,0]])
    20
    """

    N = len(points)

    def cost(a, b):
        ax, ay = a
        bx, by = b
        return abs(ax - bx) + abs(ay - by)

    # create adjacency list, hash[int,set], {pointIndex: {(cost, pointIndex), ...}}
    adj = {i:set() for i in range(N)}
    for i in range(N):
        for j in range(i + 1, N):
            dist = cost(points[i], points[j])
            adj[i].add((dist,j))
            adj[j].add((dist,i))

    res = 0
    v = set() # visits {pointIndex, ...}
    f = [(0, 0)] # frontier heap [(cost, pointIndex), ...], start with first point...

    while len(v) < N:
        cost, i = heappop(f)
        if i in v:
            continue
        
        res += cost
        v.add(i)

        for nc, ni in adj[i]:  # neighbor cost, index
            if ni not in v:
                heappush(f, (nc, ni))

    return res

if __name__ == "__main__":
    import doctest
    doctest.testmod()
