#!/usr/bin/python

import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)
        
# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x = 0, left = None, right = None):
        self.val = x
        self.left = left
        self.right = right
    def __repr__(self):
        return f"({self.val}, {self.left}, {self.right})"
    
tn=TreeNode

def ser(root: TreeNode) -> str:
    s = []
    def dfs(n):
        if n is None:
            s.append("None")
            return
        
        s.append(f"({n.val}, ")
        dfs(n.left)
        s.append(f", ")
        dfs(n.right)
        s.append(")")
    dfs(root)
    return "".join(s)

def deser(data: str) -> TreeNode:
    stack = []
    s = ""
    
    def push(s):
        if s == "None":
            stack.append(None)
        elif s:
            stack.append(int(s))
        return ""
        
    for c in data:
        log(f"c {c} stack {stack}")
        match c:
          case "(":
            stack.append("new")
          case ")":
            s = push(s)
            r = stack.pop()
            l = stack.pop()
            v = stack.pop()
            n = stack.pop()
            assert n == "new"
            stack.append(TreeNode(v, l, r))
          case ",":
            s = push(s)
          case " ":  pass
          case _:
            s += c
            
    return stack[0]

r = tn(1,tn(2),tn(3,tn(4),tn(5)))
print(r)
s = ser(r)
print(s)
n = deser(s)
print(n)


    
