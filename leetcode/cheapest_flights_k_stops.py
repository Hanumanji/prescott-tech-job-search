#!/usr/bin/python

import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)

# directed edge w/cost
# given src node a
# dst node b
# BFS k + 1 layers
# bellman-ford
# O(E*V)
def cheapest_flight_k_stops(n: int, k: int, src: int, dst: int, elw: list[list[int]]) -> int:
    """
    >>> cheapest_flight_k_stops(3, 1, 0, 2, [[0, 1, 100], [1, 2, 100], [0, 2, 500]])
    200
    >>> cheapest_flight_k_stops(3, 0, 0, 2, [[0, 1, 100], [1, 2, 100], [0, 2, 500]])
    -1
    """
    prices = [float("inf")] * n
    prices[src] = 0
    
    for i in range(k + 1):
        temp_prices = prices.copy()
        
        for a, b, c in elw:
            log(f"i {i} a {a} b {b} c {c}")
            if prices[a] == float("inf"):
                continue
            
            new_price = prices[a] + c
            if new_price < temp_prices[b]:
                temp_prices[b] = new_price

        log(f"temp_prices {temp_prices}")
        prices = temp_prices
#        for n, p in temp_prices.items():
#            prices[n] = p

        log(f"prices {prices}")
            
    return -1 if prices[dst] == float("inf") else prices[dst]


if __name__ == "__main__":
    import doctest
    doctest.testmod()
