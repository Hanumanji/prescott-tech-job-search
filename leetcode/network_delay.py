#!/usr/bin/python

import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)

        
# leetcode 743
# dijkstra's algo
# O(E * log V)

from heapq import *

def network_delay(times: list[list[int]], n: int, k: int) -> int:
    """
    >>> network_delay([[2,1,1],[2,3,1],[3,4,1]], 4, 2)
    2
    >>> network_delay([[1,2,1],[2,3,2],[1,3,2]], 3, 1)
    2
    >>> network_delay([[1,2,1],[2,3,2]], 4, 1)
    -1
    """
    # times[i] = (u, v, w) from u to v costs w

    # 1 - n, so shift everything in adj
    adj = {i + 1: {} for i in range(n)}
    for u, v, w in times:
        adj[u][v] = w
    log(f"adj {adj}")
    

    mh = [(0, k)] # minheap
    v = set() # visits
    t = 0
    
    while mh:
        p, x = heappop(mh)
        if x in v:
            continue
        
        v.add(x)
        t = max(t, p)
        
        for c in adj[x]:
            w = adj[x][c]
            log(f"c {c} w {w}")
            if c not in v:
                heappush(mh, (p + w, c))

    return t if len(v) == n else -1


if __name__ == "__main__":
    import doctest
    doctest.testmod()
