#!/usr/bin/python3

from typing import List  # REMOVE this and shebang for LeetCode Solution

from dataclasses import dataclass
import traceback
import os

@dataclass
class Logger(object):
    scope: str
    debug: bool = False
    
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, tb):
        if exc_type is not None:
            traceback.print_exception(exc_type, exc_value, tb)
        return

    def log(self, *args, **kwargs):
        if self.debug or "LOG" in os.environ:
            print(*args, **kwargs)

class Solution:
    def preprocess(self, input):
        return input

    def postprocess(self, output):
        return output
    

    def myFunction(self, *args):
        with Logger("myFunction") as logger:
            logger.log("something here")
            
        return None
    
if __name__ == "__main__":
    soln = Solution()
    fn_name = ""  # TODO Set fn_name
    assert fn_name, "Set fn_name to valid Solution method"
    fn = getattr(soln, fn_name)
    assert fn, f"No Solution method attr named {fn_name}"
    assert callable(fn), f"Solution attr {fn_name} is not callable"
    expects = []  # TODO Set expects [ (i, o), ... ]
    assert expects, "expects is empty list of (input, output) pairs, please set!"
    for input, output in expects:
        pinput = soln.preprocess(input)
        actual = fn(*pinput)
        pactual = soln.postprocess(actual)
        print("SUCCESS" if pactual == output else "FAILURE", input, "gave", pactual, "expected", output)
       
    
