#!/usr/bin/python

#   t:21
#  t:5   t:16     goto t:16 w/5
#       t:4   t:12  goto t:12 w/9
#            t:7  t:5  
class Node:
    value: int = 0
    count: int = 0
    total: int = 0
    left = None
    right = None
#    left: Node = None
#    right: Node = None
    
    def __init__(self, value):
        self.value = value
        self.count = 1
        self.total = 1
        
    def add(self, value):
        self.total += 1
        if value == self.value:
            self.count += 1
        elif value < self.value:
            if self.left:
                self.left.add(value)
            else:
                self.left = Node(value)
        elif value > self.value:
            if self.right:
                self.right.add(value)
            else:
                self.right = Node(value)

    def findMedian(self):

class MedianFinder:
    def __init__(self):
        self.l = []

    def addNum(self, num: int) -> None:
        self.l.append(num)

    def findMedian(self) -> float:
        self.l.sort()
        n = len(self.l)
        m = n // 2
        if n % 2 == 0:
            # Even so average two in the middle
            return sum(self.l[m-1:m+1])/2
        else:
            return self.l[m]
