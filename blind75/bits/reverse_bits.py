#!/usr/bin/python

def reverseBits(n: int) -> int:
    r = 0
    for i in range(32):
        if (1 << i) & n:
            r |= 1 << (31 -i)
    return r
