#!/usr/bin/python

import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)


def hammingWeight(n: int) -> int:
    w = 0
    for i in range(32):
        if (1 << i) & n:
            w += 1
    return w
