#!/usr/bin/python

import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)


def hammingWeight(n: int) -> int:
    return sum(1 for i in range(32) if (1 << i) & n)

def countBits(n: int) -> list[int]:
    return [hammingWeight(i) for i in range(n + 1)]


print(countBits(2))
