#!/usr/bin/python3

from typing import List

class Solution:
    def debug(self, *args):
        print(*args)
        pass
    def find_inflection(self, nums, s, e):
        self.debug("find_inflection", s, e)
        if s == e:
            return s if t == nums[s] else -1
        i = (s + e) // 2
        if nums[i + 1] < nums[i] and nums[i - 1] < nums[i]:
            # Found it!
            return i
            
        if i != e:
            x = self.find_inflection(nums, s, i)
            if x != -1:
                return x
        if i != s:
            y = self.find_inflection(nums, i, e)
            if y != -1:
                return y
        return -1

    def bs(self, t, nums, s, e):
        self.debug("bs", t, s, e)
        if s == e:
            return s if t == nums[s] else -1
        i = (s + e) // 2
            
        if i != e:
            x = self.bs(t, nums, s, i)
            if x != -1:
                return x
        if i + 1 != s:
            y = self.bs(t, nums, i + 1, e)
            if y != -1:
                return y
        return -1
    
    def between_indices(self, v, nums, s, e):
        sv = nums[s]
        ev = nums[e]
        return v >= sv and v <= ev
    
    def search(self, nums: List[int], target: int) -> int:
        e = len(nums) - 1
        if nums[0] < nums[-1]:
            # full rotation ie no rotation
            return self.bs(target, nums, 0, e)
        else:
            fi = self.find_inflection(nums, 0, e)
            self.debug("found inflection", fi)
            assert(fi != -1)

            # Now choose which array
            if self.between_indices(target, nums, 0, fi):
                return self.bs(target, nums, 0, fi)
            else:
                return self.bs(target, nums, fi, e)

if __name__ == "__main__":
   expects = [
       (([4,5,6,7,0,1,2], 0), 4),
       (([4,5,6,7,0,1,2], 5), 1),
       (([1,3], 3), 1),
        ]
   for input, output in expects:
       a = Solution().search(*input)
       print(*input, "gave", a, "expected", output)
       
