#!/usr/bin/python3

from typing import List

class Solution:
    debug = True
    def log(self, *args):
        if self.debug:
            print(*args)

    def bsearch(self, t, nums, b, e):
        self.log("bsearch", t, b, e)
        if b == e:
            return b if t == nums[b] else -1

        m = (b + e) // 2
        n = nums[m]

        if t == n:
            return m
        elif t < n:
            return self.bsearch(t, nums, b, m)
        else:
            return self.bsearch(t, nums, m + 1, e)            

    def find_inversion(self, nums, b, e):
        self.log("find_inversion", b, e)        
        if nums[b] <= nums[e]:
            return -1
        
        m = (b + e) // 2
        n = nums[m]
        if nums[m - 1] < n and nums[m + 1] < n:
            return m

        fi = self.find_inversion(nums, b, m)
        if fi == -1:
            return self.find_inversion(nums, m + 1, e)
        else:
            return fi
        
    def findMin(self, nums: List[int]) -> int:
        fi = self.find_inversion(nums, 0, len(nums) - 1)
        if fi == -1:
            # Not rotated so first element is smalled
            return nums[0]
        else:
            return nums[fi + 1]

if __name__ == "__main__":
    fn_name = "findMin"
    expects = [
        (([5, 6, 7, 0, 1, 2, 4],), 0),
        (([0, 1, 2, 4, 5, 6, 7],), 0),
        (([6, 7, 0, 1, 2, 4, 5],), 0),
        (([5, 6, 7, 0, 1, 2, 4],), 0),        
        (([4, 5, 6, 7, 0, 1, 2],), 0),                       
        (([1, 3],), 1),
    ]
    for input, output in expects:
        a = getattr(Solution(), fn_name)(*input)
        print(*input, "gave", a, "expected", output)
       
    
