#!/usr/bin/python

import os

def log(*args, **kwargs):
    if "LOG" in os.environ: print(*args, **kwargs)
    
def pacificAtlantic(heights: list[list[int]]) -> list[list[int]]:
    R, C = len(heights), len(heights[0])
    p, a = set(), set()

    def dfs(r, c, v, ph):
        if (r,c) in v or r < 0 or c < 0 or r == R or c == C or heights[r][c] < ph:
            return
        v.add((r,c))
        for dr, dc in (1,0),(-1,0),(0,1),(0,-1):
            dfs(r + dr, c + dc, v, heights[r][c])

    for c in range(C):
        dfs(0, c, p, heights[0][c])
        dfs(R - 1, c, a, heights[R-1][c])

    for r in range(R):
        dfs(r, 0, p, heights[r][0])
        dfs(r, C-1, a, heights[r][C-1])

    return [list(t) for t in p.intersection(a)]

print(pacificAtlantic([[1,2,2,3,5],[3,2,3,4,4],[2,4,5,3,1],[6,7,1,4,5],[5,1,1,2,4]]))
print(pacificAtlantic([[2,1],[1,2]]))
