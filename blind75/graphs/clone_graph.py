"""
# Definition for a Node.
class Node:
    def __init__(self, val = 0, neighbors = None):
        self.val = val
        self.neighbors = neighbors if neighbors is not None else []
"""

class Solution:
    def cloneGraph(self, node: 'Node') -> 'Node':
        
        nodes = {}
        
        def clone(n):
            c = None
            if n is None:
                return None
            
            if n.val in nodes:
                c = nodes[n.val]
            else:
                # Create and put in nodes lookup table first...
                c = Node(n.val)
                nodes[n.val] = c
                # ...then clone neighbors
                c.neighbors = [clone(i) for i in n.neighbors]
                
            return c
        
        return clone(node)
