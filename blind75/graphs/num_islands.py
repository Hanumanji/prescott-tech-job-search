class Solution:
    def getNeighbors(self, grid, ij):
        ns = set()
        i, j = ij
        d = ((0, 1), (0, -1), (1, 0), (-1, 0))
        for di, dj in d:
            ci, cj = i + di, j + dj
            if (ci >= 0 and ci < len(grid) and
                cj >= 0 and cj < len(grid[0])):
                    if grid[ci][cj] == '1':
                        ns.add((ci, cj))
        return ns
                        
    def numIslands(self, grid: List[List[str]]) -> int:
        c = 0
        ijs = set((i, j) for i in range(len(grid)) for j in range(len(grid[0])))
        v = set()   # visible
        td = set()  # todo on current island
        while True:
            while td:
                ij = td.pop()
                if ij in v:
                    continue
                    
                ns = self.getNeighbors(grid, ij)
                v.add(ij)
                td.update(ns)
                ijs.difference_update(ns)
                
            if ijs:
                ij = ijs.pop()
                if ij in v:
                    continue
                    
                i, j = ij
                if grid[i][j] == '1':
                    ns = self.getNeighbors(grid, ij)
                    v.add(ij)
                    td.update(ns)
                    ijs.difference_update(ns)
                    
                    c += 1
            else:
                break
                
        return c
