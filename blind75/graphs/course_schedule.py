#!/usr/bin/python

import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)
        
def canFinish(numCourses: int, prerequisites: list[list[int]]) -> bool:
    log(f"numCourses {numCourses} prerequisites {prerequisites}")

    r = {} # a:{b} for a, b in prerequisites}    
    for a, b in prerequisites:
        s = r.setdefault(a, set())
        s.add(b)

    log(f"r {r}")

    v = set()
    def dfs(k):
        if k in v:
            return False
        v.add(k)
        if k in r:
            for p in r[k]:
                if not dfs(p):
                    return False
                
            r[k].clear()
        v.remove(k)

        return True

    for k in range(numCourses):
        if not dfs(k):
            return False
        
    return True


print(canFinish(3, [[0,1],[0,2],[1,2]]))
#print(canFinish(7, [[1,0],[0,3],[0,2],[3,2],[2,5],[4,5],[5,6],[2,4]]))
#print(canFinish(1, []))
#print(canFinish(3, [[1,0],[1,2],[0,1]]))
#print(canFinish(2, [[1,0]]))
#print(canFinish(2, [[1,0],[0,1]]))
