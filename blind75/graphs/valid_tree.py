#!/usr/bin/python

import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)


def valid_tree(n: int, edges: list[list[int]]) -> bool:
    if n == 0: return True
    
    # Two issues:
    # Cycles
    #   If one side of edge in tree, other must not be in tree, or cycle
    # Islands
    #   If any 0..n-1 is not in an edge

    al = {}
    for a, b in edges:
        al.setdefault(a, set()).add(b)
        al.setdefault(b, set()).add(a)        

    v = set()
    def dfs(n, p):
        if n in v:
            return False
        v.add(n)
        for e in al[n]:
            if e != p:
                if not dfs(e, n):
                    return False
        return True

    return dfs(0, -1) and len(v) == n


print(valid_tree(5, [[0, 1], [0, 2], [0, 3], [1, 4]]))
print(valid_tree(5, [[0, 1], [1, 2], [2, 3], [1, 3], [1, 4]]))
print(valid_tree(4, [[0, 1], [2, 3]]))




        
