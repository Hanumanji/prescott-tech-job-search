#!/usr/bin/python

import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)

def conn_comp(n: int, edges: list[list[int]]) -> int:
    log(f"n {n} edges {edges}")

    h = {}
    for a, b in edges:
        if a not in h and b not in h:
            h[a] = h[b] = set((a,b))

        sa = h.get(a, set())
        sa.add(a)
        sb = h.get(b, set())
        sb.add(b)
        u = sa.union(sb)
        for v in u:
            h[v] = u

    s = set(tuple(s) for s in h.values())

    log(f"h {h}")
    
    return len(s) + (n - len(h.keys()))
        
#    adj = {}
#    for a, b in edges:
#        adj.setdefault(a, set()).add(b)
#        adj.setdefault(b, set()).add(a)

    return 

print(conn_comp(3, [])) # 3
print(conn_comp(5, [[0,1],[1,2],[3,4]])) # 2
print(conn_comp(4, [[0,1],[2,3],[1,2]])) # 1
print(conn_comp(5, [[0,1]])) # 4
print(conn_comp(6, [[0,1],[2,3],[4,5]])) # 3
