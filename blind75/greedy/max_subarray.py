#!/usr/bin/python

import os

class Env:
    def __init__(self, *args):
        assert len(args) % 2 == 0, "Need env key/value pairs"
        self.d = {}
        while args:
            k, v, *args = args
            self.d[k] = v
        
    def __enter__(self):
        self.p = {}
        self.r = []
        for k, v in self.d.items():
            if k in os.environ:
                p[k] = os.environ[k]
            else:
                self.r.append(k)
            os.environ[k] = v
    
    def __exit__(self, exc_type, exc_value, tb):
        for k, v in self.p:
            os.environ[k] = v
        for k in self.r:
            del os.environ[k]
        
def log(*args, **kwargs):
    """
    >>> log("duck")

    >>> with Env("LOG", "1"):
    ...   log("duck")
    duck
    """
    if "LOG" in os.environ:
        print(*args, **kwargs)


def maxSumArray(nums: list[int], s = 0) -> int:
    """
    >>> maxSumArray([1, 2, 3])
    6
    >>> maxSumArray([1, -2, 3])
    2
    >>> maxSumArray([1, -2, 3], 1)
    1
    """
    m = t = nums[s]
    for i in range(s + 1, len(nums)):
        v = nums[i]
        t += v
        if t > m:
            m = t
    return m
    
def maxSubArray2(nums: list[int]) -> int:
    """
    >>> maxSubArray2([-2,1,-3,4,-1,2,1,-5,4])
    6
    >>> maxSubArray2([1])
    1
    >>> maxSubArray2([5,4,-1,7,8])
    23
    """
    m = maxSumArray(nums)
    for i in range(1, len(nums)):
        c = maxSumArray(nums, i)
        if c > m:
            m = c
    return m

def maxSubArray(nums: list[int]) -> int:
    """
    >>> maxSubArray([-2,1,-3,4,-1,2,1,-5,4])
    6
    >>> maxSubArray([1])
    1
    >>> maxSubArray([5,4,-1,7,8])
    23
    >>> maxSubArray([-2, -1])
    -1
    """
    l = len(nums)
    m = nums[0]
    for i in range(l):
        for j in range(i + 1, l + 1):
            c = sum(nums[i:j])
            if c > m:
                m = c
    return m

#  i      m    s   e
#  0     -5    0   0
#  1      9    1   1
#  2

#  i  v   t |  s  e  m
#  0 -5  -5 |  0  0 -5
#  1  9   4 |  1  1  9
#  2 -8   1 |  1  1  9
#  3  3   4 |  1  1  9
#  4  6  10 |  1  4 10
#  5 -9   1 |  1  4 10

#  [-5, 9, -8, 3, 6, -9]
def maxsubarray(nums: list[int]) -> int:
    """
    >>> maxsubarray([-5, 9, -8, 3, 6, -9])
    10

    >>> maxsubarray([-2,1,-3,4,-1,2,1,-5,4])
    6
    >>> maxsubarray([1])
    1
    >>> maxsubarray([5,4,-1,7,8])
    23
    >>> maxsubarray([-2, -1])
    -1

    """
    # Find positive start
    l = len(nums)
    t = m = nums[0]  # total, max
#    s, e = 0, 0
    for i in range(1, l):
        v = nums[i]
        t += v
        if v > t:
            t = v
 #           s = e = i
            
        if t > m:
            m = t
#            e = i
            
    return m

if __name__ == "__main__":
    import doctest
    doctest.testmod()
