#!/usr/bin/python

import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)


def canJump(nums: list[int]) -> bool:
    """
    >>> canJump([2, 3, 1, 1, 4])
    True

    >>> canJump([3, 2, 1, 0, 4])
    False

    >>> canJump([2, 0])
    True
    """
    # 2, 3, 1, 1, 4
    # T  T  T  T  T

    # 3, 2, 1, 0, 4
    # F  F  F  F  T
    l = len(nums)
    r = [False] * l
    r[l - 1] = True
    for i in range(l - 2, -1, -1):
        mj = nums[i] + i  # Not jump length, max jump length

        for j in range(i + 1, mj + 1):
            if j < l and r[j]:
                r[i] = True
                break
    
    return r[0]
        
if __name__ == "__main__":
    import doctest
    doctest.testmod()        
