#!/usr/bin/python

import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)

def alien_order(words: list[str]) -> str:
    adj = {c:set() for w in words for c in w}
    
    def add(a, b):
        s = adj.setdefault(a, set())
        s.add(b)
            
    p = None
    for w in words:
        if p is None:
            p = w
            continue

        # compare w and p
        added = False
        for i in range(min(len(w), len(p))):
            if w[i] != p[i]:
                add(p[i], w[i])
                added = True
                break
        if not added and len(w) < len(p):
            return ""
        
        p = w

    log(f"adj {adj}")

    stack = []

    v = {} # False = visited, True = current path
    # Topological sort via DFS postorder stack
    def dfs(i):
        if i in v:
            return v[i]
        v[i] = True
        if i in adj:
            for c in adj[i]:
                if dfs(c):
                    return True 
        stack.append(i)
        v[i] = False
        return False
    
    for c in adj:
        if dfs(c):
            return ""

    log(f"stack {stack}")
    
    return "".join(stack[::-1])

print(alien_order(["wrt", "wrf", "er", "ett", "rftt"])) # "wertf"
print(alien_order(["z", "x"])) # "zx"
print(alien_order(["wrt", "wrf", "ert", "er", "ett", "rftt"])) # ""  PROBLEM ert !< er
print(alien_order(["a", "ba", "bc", "c"])) # abc
print(alien_order(["z", "x", "z"])) # ""
