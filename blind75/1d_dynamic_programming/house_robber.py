#!/usr/bin/python

#
# a = 1
# a,b = max(a,b)
# a,b,c = max(a+c,b)
# a,b,c,d = max(a+c,b+d,a+d)
# a,b,c,d,e = max(a+c+e,a+d,b+d,b+e)
#
# [1,2,3,1]
# [4,3,3,1]
#
# [ 2, 7, 9, 3, 1]
# [12,10,10, 3, 1]
#
# [ 1, 3, 5, 9, 2, 6, 7]
# [17,19,14,16, 9, 6, 7]
#

class Solution:
    def rob(self, nums: List[int]) -> int:
        n = len(nums)
        mx = [0] * n
        
        mx[-1] = nums[-1]
        if n == 1:
            return nums[0]

        mx[-2] = max(nums[-2:])
        if n == 2:
            return max(nums)
        
        mx[-3] = max(nums[-3] + nums[-1], nums[-2])
        if n == 3:
            return mx[-3]

        for i in reversed(range(n - 3)):
            mx[i] = nums[i] + max(mx[i+2], mx[i+3])
        
        #print(mx)
        return max(mx[:3])
    
