#!/usr/bin/python
import os

def log(*args, **kwargs):
  if "LOG" in os.environ:
    print(*args, **kwargs)
    
# Return fewest number of coins to make amount

def coinChangeDFS(coins: list[int], amount: int) -> int:
  def dfs(c, r): # count, remainder
    if r < 0:
      log(f"r {r} -1")
      return -1
      
    if r == 0:
      log(f"r {r} c {c}")
      return c
      
    m = -1
    for coin in coins:
      a = dfs(c + 1, r - coin)
      log(f"dfs c + 1 {c+1} r-coin {r-coin} -> {a}")
      if a != -1:
        if m == -1 or a < m:
          m = a
    return m

  return dfs(0, amount)
  
#     1  2  3  4  5  6  7  8  9  10 11
# 0- |0 |0 |0 |0 |0 |0 |0 |0 |0 |0 |0 |
# 1- |1 |2 |3 |4 |5 |6 |7 |8 |9 |10|11|
# 2- |1 |1 |2 |2 |5 |6 |7 |8 |9 |10|11|
#                    1        1  1
#     2        2  2     2  2
#  3     3  3  3  3  3  3

# |0 |1 |2 |3 |4 |5 |6 |7 |8 |9 |10|11|
#     1 |2 |3 |4 |5 |6 |7 |8 |9 |10|11|
#                 1  2  3  4  5  2  3
#        1  2  2              
#     
# |0 |1 |2 |3 |4 |5 |6 |7 |8 |9 |10|11|
#        1
# 
def coinChange(coins: list[int], amount: int) -> int:
  if amount == 0:
    return 0
  
  counts = [0] * (amount + 1)

  for coin in coins:
    if coin <= amount:
      counts[coin] = 1
    for i in range(coin + 1, amount + 1):
        if counts[i - coin]:
          d = counts[i - coin] + 1
          if not counts[i] or d < counts[i]:
            counts[i] = d

  log(f"counts {counts}")
  return counts[-1] if counts[-1] else -1

  
print(coinChange([1,2,5], 11))
print(coinChange([2], 3))
