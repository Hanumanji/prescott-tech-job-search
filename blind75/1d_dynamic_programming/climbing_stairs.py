#!/usr/bin/python

class Solution:
    def climbStairs(self, n: int) -> int:
        if n == 1:
            return 1
        if n == 2:
            return 2
        
        c = [0] * n
        c[n - 1] = 1
        c[n - 2] = 2
        for i in reversed(range(n - 2)):
            c[i] = c[i + 1] + c[i + 2]
            
        return c[0]
        # 5 stairs
        # #5 = 1
        # #4 = 2
        # #3 = #4 + #5 = 3
        # #2 = #3 + #4 = 3 + 2 = 5
        # #1 = #2 + #3 = 5 + 3 = 8
