#!/usr/bin/python3.10

import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)
        
def longestPalindrome(s: str) -> str:
    """
    >>> longestPalindrome("babad")
    'bab'
    >>> longestPalindrome("cbbd")
    'bb'
    >>> longestPalindrome("ccc")
    'ccc'
    >>> longestPalindrome("ccccc")
    'ccccc'
    >>> longestPalindrome("babab")
    'babab'
    """
    n = len(s)
    log(f"n {n} s {s}")

    longest = [-1, -1, 0] # longest beginning, ending, length
    
    def check(b, e):
        lb, le, ll = longest
        while b >= 0 and e < n and s[b] == s[e]:
            l = e - b + 1
            if l > ll:
                # Gaming the bound variables here...
                longest[:] = [b, e, l]
                
                # Funny, that following line is NOT needed,
                # because l > ll will still be true for all
                # incrementing l, so... ;-)
                # ll = l
                
            b -= 1
            e += 1
    
    for i in range(n):
        check(i, i)
        check(i, i + 1)        

    lb, le, _ = longest
    return s[lb:le + 1]

if __name__ == "__main__":
    import doctest
    doctest.testmod()
