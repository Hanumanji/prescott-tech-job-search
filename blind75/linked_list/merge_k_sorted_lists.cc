/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* mergeKLists(vector<ListNode*>& lists) {
        if (lists.size() == 0){
            return nullptr;
        }
        ListNode* curr = nullptr;
        ListNode* head = nullptr;
        int h = -1;
        
        for (int i = 0; i < lists.size(); i++){
            curr = lists[i];
            
            if (curr == nullptr){
                continue;
            }
            if (head == nullptr){
                head = curr;
                continue;
            }
            if (curr->val <= head->val){
                head = curr;
                h = i;
            }
        }
        if (head == nullptr){
            return nullptr;
        }
        
        // Remove the head
        ListNode* next = head->next;
        if (next == nullptr){
            // If no next, delete from lists
            lists.erase(lists.begin() + h);
        } else {
            // Otw set space in lists to next
            lists[h] = next;
        }
        // We have head now...
        ListNode* prev = head;
        next = nullptr;
        while (lists.size() > 0){
            h =0;
            for (int j = 0; j < lists.size(); j++){
                curr = lists[j];
                if (curr == nullptr){
                    continue;
                }
                if (next == nullptr){
                    next = curr;
                    continue;
                }
                if (curr->val <= next->val){
                    next = curr;
                    h = j;
                }
            }
            prev->next = next;
            prev = next;
            
            
            if (next->next == nullptr){
            // If no next, delete from lists
                lists.erase(lists.begin() + h);
            } else {
            // Otw set space in lists to next
                lists[h] = next->next;
            } 
        }
        return head;
    }
};
