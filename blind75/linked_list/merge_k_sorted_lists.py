#!/usr/bin/python3

from typing import List, Optional
from dataclasses import dataclass

@dataclass
class ListNode:
    val: int = 0
    next: 'ListNode' = None
    
#    def __init__(self, val=0, next=None):
#        self.val = val
#        self.next = next

class Solution:
    def preprocess(self, input):
        self.log("preprocess", input)
        lists,  = input
        return ([self.linked_list(a) for a in lists],)

    def postprocess(self, output):
        self.log("postprocess", output)
        return self.ll2a(output)
    
    debug = False
    def log(self, *args):
        if self.debug:
            print(*args)

    def ll2a(self, ll):
        a = []
        while ll:
            a.append(ll.val)
            ll = ll.next
        return a
    
    def linked_list(self, arr):
        h = None
        p = None
        for v in arr:
            self.log(v)
            l = ListNode(v)
            if h == None:
                h = l
                p = l
            else:
                p.next = l
                p = l
	
        assert(arr == self.ll2a(h))
        return h
            
    def mergeKLists(self, lists: List[Optional[ListNode]]) -> Optional[ListNode]:
        self.log(lists)
        lists = [l for l in lists if l]
        self.log(lists)
        
        if not lists:
            return None
        
        h = None
        p = None
        while lists:
            # lists.sort(key=lambda x: x.val)

            mi = min(range(len(lists)), key=lambda i: lists[i].val)
            m = lists[mi]
            
            self.log(mi, m, p)
            if h == None:
                assert(p == None)                
                h = p = m
            else:
                p.next = m
                p = m
                assert(m != m.next)
            
            if m.next:
                lists[mi] = m.next
                self.log("setting", mi, "was", m, "now", m.next)
            else:
                del lists[mi]
                self.log("deleting", mi, m)
            
        return h

            
if __name__ == "__main__":
    soln = Solution()
    fn_name = "mergeKLists"
    assert fn_name, "Set fn_name to valid Solution method"
    fn = getattr(soln, fn_name)
    assert fn, f"No Solution method attr named {fn_name}"
    assert callable(fn), f"Solution attr {fn_name} is not callable"
    expects = [
#        (([[]],), None),
        (([[1,4,5],[1,3,4],[2,6]],), [1,1,2,3,4,4,5,6]),
    ]
    assert expects, "expects is empty list of (input, output) pairs, please set!"
    for input, output in expects:
        pinput = soln.preprocess(input)
        actual = getattr(soln, fn_name)(*pinput)
        pactual = soln.postprocess(actual)
        print("SUCCESS" if pactual == output else "FAILURE", input, "gave", pactual, "expected", output)
    
