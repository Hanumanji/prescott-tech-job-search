#!/usr/bin/python3.10

from typing import List
from dataclasses import dataclass
import traceback

@dataclass
class Logger(object):
    scope: str
    debug: bool = False
    
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, tb):
        if exc_type is not None:
            traceback.print_exception(exc_type, exc_value, tb)
        return

    def log(self, *args):
        if self.debug:
            print(*args)

class Solution:
    def preprocess(self, input):
        return input

    def postprocess(self, output):
        return frozenset(frozenset(l) for l in output)
    
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
        with Logger("Solution") as logger:
            logger.log("groupAnagrams")
            
            h = {} # { frozenset(chars): [word1, ...] }
            for s in strs:
                fs = frozenset(s)
                if fs in h:
                    h[fs].append(s)
                else:
                    h[fs] = [s]
                    
            return list(h.values())
    
    def myFunction(self, *args):
        with Logger("myFunction") as logger:
            logger.log("something here")
            
        return None
    
if __name__ == "__main__":
    soln = Solution()
    fn_name = "groupAnagrams"
    assert fn_name, "Set fn_name to valid Solution method"
    fn = getattr(soln, fn_name)
    assert fn, f"No Solution method attr named {fn_name}"
    assert callable(fn), f"Solution attr {fn_name} is not callable"
    expects = [
        ((["eat", "tea", "tan", "ate", "nat", "bat"],),
         frozenset([frozenset(l) for l in [["bat"], ["nat", "tan"], ["ate", "eat", "tea"]]]))
    ]  
    assert expects, "expects is empty list of (input, output) pairs, please set!"
    for input, output in expects:
        pinput = soln.preprocess(input)
        actual = fn(*pinput)
        pactual = soln.postprocess(actual)
        print("SUCCESS" if pactual == output else "FAILURE", input, "gave", pactual, "expected", output)
       
    
