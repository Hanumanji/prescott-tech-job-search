package main

import (
       "fmt"
       "sort"
       "strings"
       )

func sortString(w string) string {
     s := strings.Split(w, "")
     sort.Strings(s)
     return strings.Join(s, "")
}

func groupAnagrams(strs []string) [][]string {
    m := make(map[string][]string)

    for _, str := range(strs) {
    	ss := sortString(str)
	if sl, ok := m[ss]; ok {
	   sl = append(sl, str)
	   m[ss] = sl
	} else {
	  m[ss] = []string{str}
	}
    }
    res := make([][]string, len(m))
    i := 0
    for _, v := range m {
    	res[i] = v
	i += 1
    }
    return res
}

func main(){
     fmt.Println(groupAnagrams([]string{"eat", "tea", "tan", "ate", "nat", "bat"}))
}
