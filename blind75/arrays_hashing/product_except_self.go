package main

import (
       "fmt"
       )

func productExceptSelf(nums [] int) []int {
//     fmt.Println("nums: ", nums)
     n := len(nums)
     res := make([]int, n)
     l := make([]int, n)
     r := make([]int, n)
     lt := 1
     rt := 1
     for i := 0; i < n; i++ {
     	 lt *= nums[i]
	 rt *= nums[(n - 1) - i]
	 l[i] = lt
	 r[(n - 1) - i] = rt
     }
//     fmt.Println("l: ", l)
//     fmt.Println("r: ", r)
     res[0], res[n - 1] = r[1], l[n - 2]
     for i := 1; i < n - 1; i++ {
     	 res[i] = l[i - 1] * r[i + 1]
     }
     return res
}

func main(){
     fmt.Println(productExceptSelf([]int{1,2,3,4})) // [24, 12, 8, 6]
     fmt.Println(productExceptSelf([]int{-1,1,0,-3,3})) // [0,0,9,0,0]
}