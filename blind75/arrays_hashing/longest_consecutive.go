package main

import (
       "fmt"
       )

type Interval struct {
     start, end int
}

func longestConsecutive(nums []int) int {
     m := make(map[int]Interval)

     for _, n := range nums {
     	 start, end := n, n
	 if r, ok := m[n - 1]; ok {
	    if r.start < start {
	       start = r.start
	    }
	    if r.end > end {
	       end = r.end
	    }
	    delete(m, r.start)
	    delete(m, r.end)
	 }
	 if r, ok := m[n + 1]; ok {
	    if r.start < start {
	       start = r.start
	    }
	    if r.end > end {
	       end = r.end
	    }
	    delete(m, r.start)
	    delete(m, r.end)
	 }
	 if _, ok := m[start]; !ok {
	 	 m[start] = Interval{start, end}
	}
	 if _, ok := m[end]; !ok {
	 	 m[end] = Interval{start, end}
	}
//	 fmt.Println("n: ", n, "m: ", m)
     }
     res := 0
     for _, i := range m {
     	 d := i.end - i.start + 1
	 if d > res {
	    res = d
	 }
     }
     return res
}



func main() {
	fmt.Println(longestConsecutive([]int{100, 4, 200, 1, 3, 2}))         // 4
	fmt.Println(longestConsecutive([]int{0, 3, 7, 2, 5, 8, 4, 6, 0, 1})) // 9
	fmt.Println(longestConsecutive([]int{-7, -1, 3, -9, -4, 7, -3, 2, 4, 9, 4, -9, 8, -7, 5, -1, -7})) // 4
	fmt.Println(longestConsecutive([]int{-3, 2, 8, 5, 1, 7, -8, 2, -8, -4, -1, 6, -6, 9, 6, 0, -7, 4, 5, -4, 8, 2, 0, -2, -6, 9, -4, -1})) // 7
	fmt.Println(longestConsecutive([]int{-6,6,-9,-7,0,3,4,-2,2,-1,9,-9,5,-3,6,1,5,-1,-2,9,-9,-4,-6,-5,6,-1,3})) // 14
}