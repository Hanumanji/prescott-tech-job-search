package main

import (
    "sort"
    "fmt"
)

type numCount struct {
    num int
    count int
}

func topKFrequent(nums []int, k int) []int {
//    fmt.Println("nums: ", nums, " k: ", k)
    
    m := make(map[int]int)
    
    for _, v := range nums {
        if c, ok := m[v]; ok {
            m[v] = c + 1
        } else {
            m[v] = 1
        }
    }
//    fmt.Println("m: ", m)
    
    counts := make([]numCount, len(m))
    i := 0
    for k, v := range m {
        counts[i] = numCount{k, v}
	i += 1
    }
//    fmt.Println("counts: ", counts)
    sort.Slice(counts, func(i, j int) bool {
        return counts[i].count > counts[j].count
    })
    
    res := make([]int, k)
    for i = 0; i < k; i++ {
        res[i] = counts[i].num
    }
    return res
}

func main(){
     fmt.Println(topKFrequent([]int{1,1,1,2,2,3}, 2))
}