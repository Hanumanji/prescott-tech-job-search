package main

import "fmt"

func twoSum(nums []int, target int) []int {
    res := []int{-1, -1}
    m := make(map[int]int)
    for i, v := range nums {
        if ci, ok := m[target - v]; ok {
            // Found compliement index
            res = []int{i, ci}
            break
        } else {
            m[v] = i
        }
    }
    return res
}

func main(){
     res := twoSum([]int{2, 7, 11, 15}, 9)
     fmt.Println("res: ", res) // [1, 0]
}