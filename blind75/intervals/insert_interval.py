#!/usr/bin/python3

from typing import List  # REMOVE this and shebang for LeetCode Solution

from dataclasses import dataclass
import traceback
import os

@dataclass
class Logger(object):
    scope: str
    debug: bool = False
    
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, tb):
        if exc_type is not None:
            traceback.print_exception(exc_type, exc_value, tb)
        return

    def log(self, *args, **kwargs):
        if self.debug or "LOG" in os.environ:
            print(*args, **kwargs)

class Solution:
    def preprocess(self, input):
        return input

    def postprocess(self, output):
        return output
    
    def insert(self, intervals: List[List[int]], newInterval: List[int]) -> List[List[int]]:
        new_intervals = []
        with Logger("insert") as logger:
            logger.log(f"intervals {intervals} newInterval {newInterval}")
            sni, eni = newInterval

            merging = False
            start = None
            previous = None
            for interval in intervals:
                si, ei = interval

                def inside(v, i):
                    s, e = i
                    return v >= s and v <= e
                
                def intersect(i, j):
                    si, ei = i
                    sj, ej = j
                    return (inside(si, j) or inside(ei, j) or
                            inside(sj, i) or inside(ej, i))

                def mi(i, j):
                    si, ei = i
                    sj, ej = j
                    return min(si, sj)

                def mx(i, j):
                    si, ei = i
                    sj, ej = j
                    return max(ei, ej)
                    
                xs = intersect(interval, newInterval)
                if not merging:
                    if xs:
                        merging = True
                        start = mi(interval, newInterval)
                        previous = interval
                        logger.log(f"not merging xs start {start}")
                    else:
                        new_intervals.append(interval)
                else:
                    if xs:
                        previous = interval
                        # Don't append since in merge, so skip...
                    else:
                        # Use previous to calculate end...
                        end = mx(previous, newInterval)
                        new_intervals.append([start, end])
                        new_intervals.append(interval)
                        # Done merging!
                        merging = False
                        logger.log(f"merging not xs end {end}")
            if merging:            
                # Use previous to calculate end...
                end = mx(previous, newInterval)
                new_intervals.append([start, end])
                # Done merging!
                merging = False
                logger.log(f"merging not xs end {end}")

            if new_intervals and not start:
                # Never had a merge, so left overs to deal with...
                if new_intervals[0][0] < newInterval[0]:
                    new_intervals.append(newInterval)
                else:
                    new_intervals.insert(0, newInterval)
                    
        return new_intervals if new_intervals else [newInterval]
    
if __name__ == "__main__":
    soln = Solution()
    fn_name = "insert"
    assert fn_name, "Set fn_name to valid Solution method"
    fn = getattr(soln, fn_name)
    assert fn, f"No Solution method attr named {fn_name}"
    assert callable(fn), f"Solution attr {fn_name} is not callable"
    expects = [
#        (([[1,3],[6,9]],[2,5]),[[1,5],[6,9]]),
#        (([[1,2],[3,5],[6,7],[8,10],[12,16]],[4,8]),[[1,2],[3,10],[12,16]]),
#        (([],[5,7]),[[5,7]]),
#        (([[1,5]],[2,3]),[[1,5]]),
        (([[1,5]],[6,8]), [[1,5],[6,8]]),
    ]  
    assert expects, "expects is empty list of (input, output) pairs, please set!"
    for input, output in expects:
        pinput = soln.preprocess(input)
        actual = fn(*pinput)
        pactual = soln.postprocess(actual)
        print("SUCCESS" if pactual == output else "FAILURE", input, "gave", pactual, "expected", output)
       
    
