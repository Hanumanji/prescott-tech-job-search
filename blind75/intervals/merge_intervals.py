#!/usr/bin/python

# intersects
# faster 11.01%
# smaller 51.66%
#
# xs
# faster 32.73%
# smaller 51.66%

from typing import List  # REMOVE this and shebang for LeetCode Solution
import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)
        
def inside(self, v):
    """
    >>> inside((3, 1), 2)
    Traceback (most recent call last):
    ...
    AssertionError: Start 3 not less than or equal to end 1

    >>> inside((1, 3), 2)
    True

    >>> inside((1, 3), 4)
    False
    """
    ss, se = self
    assert ss <= se, f"Start {ss} not less than or equal to end {se}"
    
    return v >= ss and v <= se

def intersects(self, other):
    """
    >>> intersects((1, 3), (2, 4))
    True

    >>> intersects((1, 4), (2, 3))
    True

    >>> intersects((0, 0), (1, 1))
    False
    """
    ss, se = self
    os, oe = other
    return (inside(self, os) or
            inside(self, oe) or
            inside(other, ss) or
            inside(other, se))

def xs(self, other):
    ss, se = self
    os, oe = other
    return ((os >= ss and os <= se) or
            (oe >= ss and oe <= se) or
            (ss >= os and ss <= oe) or
            (ss >= os and ss <= oe))

def union(self, other):
    """
    >>> None is union((1, 3), (4, 6))
    True

    >>> union((1, 3), (2, 4))
    [1, 4]

    >>> union((1, 7), (2, 4))
    [1, 7]
    """
    if not xs(self, other):
        return None
    else:
        ss, se = self
        os, oe = other        
        return [min(ss, os), max(se, oe)]

def merge(intervals: List[List[int]]) -> List[List[int]]:
    """
    >>> merge([[1, 3], [2, 6], [8, 10], [15, 18]])
    [[1, 6], [8, 10], [15, 18]]

    >>> merge([[1, 4], [4, 5]])
    [[1, 5]]

    >>> merge([[1, 9], [2, 6], [8, 10], [15, 18]])
    [[1, 10], [15, 18]]

    >>> merge([[2,3],[5,5],[2,2],[3,4],[3,4]])
    [[2, 4], [5, 5]]
    """
    new_intervals = []

    for i, interval in enumerate(intervals):
        log(f"i {i} interval {interval} new_intervals {new_intervals}")
        from bisect import bisect_left
        
        j = bisect_left(new_intervals, interval)
        p = j - 1
        u = None
        if p >= 0:
            u = union(interval, new_intervals[p])
            if u:
                log(f"p {p} new_interval {new_intervals[p]} union {u}")
                new_intervals.pop(p)
                interval = u
                j = p

        while j < len(new_intervals):
            u = union(interval, new_intervals[j])
            if u:
                log(f"j {j} new_interval {new_intervals[j]} union {u}")
                new_intervals.pop(j)
                interval = u
            else:
                log(f"break j {j}")
                break

        if u:
            log(f"insert j {j} union {u}")
            new_intervals.insert(j, u)
        else:
            log(f"insert j {j} interval {interval}")
            new_intervals.insert(j, interval)

    return new_intervals
                

if __name__ == "__main__":
    import doctest
    doctest.testmod()
