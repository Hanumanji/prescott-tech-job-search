package main

import (
	"fmt"
)

var ()

func intersect(a []int, b []int)(union []int, intersected bool){
	if ((a[0] >= b[0] && a[0] <= b[1]) || (a[1] >= b[0] && a[1] <= b[1]) || (b[0] >= a[0] && b[0] <= a[1]) || (b[1] >= a[0] && b[1] <= a[1])){
		if a[0] < b[0] {
			union = append(union, a[0])
		} else {
			union = append(union, b[0])
		}
		if a[1] > b[1] {
			union = append(union, a[1])
		} else {
			union = append(union, b[1])
		}		
		intersected = true
	}
	return
}

func insert(intervals [][]int, newInterval []int) [][]int {
	res := [][]int{}

	inserted := false
	for _, interval := range intervals {
		if inserted {
			res = append(res, interval)
			continue
		}
		if union, x := intersect(newInterval, interval); x {
			newInterval = union
		} else {
			if newInterval[1] < interval[0] {
				res = append(res, newInterval)
				inserted = true
			}
			res = append(res, interval)
		}
	}
	if !inserted { res = append(res, newInterval) }

	return res
}

func main(){
	fmt.Println(insert([][]int{[]int{1,3},[]int{6,9}}, []int{2,5}))
	fmt.Println(insert([][]int{[]int{1,2},[]int{3,5}, []int{6,7}, []int{8,10}, []int{12,16}}, []int{4,8}))
	fmt.Println(insert([][]int{},[]int{5,7}))
	fmt.Println(insert([][]int{[]int{1,5}},[]int{2,3}))
}
