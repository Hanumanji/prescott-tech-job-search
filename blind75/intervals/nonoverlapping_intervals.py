#!/usr/bin/python

from typing import List
import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)

def erase_overlaps(intervals: List[List[int]]) -> int:
    """
    >>> erase_overlaps([[1, 2], [2, 3], [3, 4], [1, 3]])
    1
    >>> erase_overlaps([[1, 2], [1, 2], [1, 2]])
    2
    >>> erase_overlaps([[1, 2], [2, 3]])
    0
    >>> erase_overlaps([[0,2],[1,3],[2,4],[3,5],[4,6]])
    2
    """
    count = 0
    intervals.sort(key = lambda x: x[1])
    start, end = intervals.pop(0)
    for s, e in intervals:
        if s < end:
            count += 1
        else:
            start, end = s, e
    return count

if __name__ == "__main__":
    import doctest
    doctest.testmod()

