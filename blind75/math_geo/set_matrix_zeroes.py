#!/usr/bin/python

import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)


# Just use the first zero to say which row and column holds the zeroes!

def set_zeroes(matrix: list[list[int]]) -> None:
    # Modify in place

    zrc = None

    log(f"matrix {matrix}")
    
    for i, r in enumerate(matrix):
        for j, c in enumerate(r):
            if c == 0:
                log(f"zero @ i {i} j {j} c {c}")
                if zrc is None:
                    zrc = (i, j)

                zr, zc = zrc
                    
                # Mark column/row as zeroes
                log(f"marking column i {i} zc {zc}")
                matrix[i][zc] = 0

                log(f"marking row zr {zr} j {j}")
                matrix[zr][j] = 0
                

    log(f"zrc {zrc}")
    if zrc:
        zr, zc = zrc    
    
        # Zero out rows except zero column
        for i, r in enumerate(matrix):
            if i == zr:
                continue
            
            if r[zc] == 0:
                r[:] = [0] * len(r)
                

        # Zero out columns except zero row
        for j, c in enumerate(matrix[zr]):
            if j == zc:
                continue
            if c == 0:
                for r in matrix:
                    r[j] = 0

        # Zero out zero row
        matrix[zr][:] = [0] * len(matrix[zr])

        # Zero out zero column
        for r in matrix:
            r[zc] = 0
        
    log(f"matrix {matrix}")
        
            
z = [[1,1,1],[1,0,1],[1,1,1]]
set_zeroes(z)
print(z)
z = [[0,1,2,0],[3,4,5,2],[1,3,1,5]]
set_zeroes(z)
print(z)
z = [[-1],[2],[3]]
set_zeroes(z)
print(z)
