#!/usr/bin/python

import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)


def rotate(matrix: list[list[int]]) -> None:
    # (0-2,0) -> (2, 0-2) -> (2-0,2) -> (0, 2-0)
    # t, b, l, r top, bottom, left, right
    m = matrix
    n = len(m)
    
    t = 0
    b = n - 1
    l = 0
    r = n - 1

    log(f"rotate n {n} t {t} b {b} l {l} {r}")
    while t < b:
        log(f"loop {b - t}")
        for i in range(r - l):
            log(f"i {i} t {t} b {b} l {l} r {r}")
            
            # Store top
            v = m[t][l + i]

            log(f"top v {v}@({t},{l + i})")
            
            # Swap right for top, keep right
            v, m[t + i][r] = m[t + i][r], v
            log(f"right v {v}@({t + i},{r})")
            
            # Swap bottom for right, keep bottom
            v, m[b][r - i] = m[b][r - i], v
            log(f"bottom v {v}@({b},{r - i})")
            
            # Swap left for bottom, keep left
            v, m[b - i][l] = m[b - i][l], v
            log(f"left v {v}@({b - i},{l})")
            
            # Swap left back to top
            m[t][l + i] = v

        log(f"end {b - t} m {m}")
        
        b -= 1
        t += 1
        l += 1
        r -= 1

expects = [
#    (([[1,2,3],[4,5,6],[7,8,9]],), [[7,4,1],[8,5,2],[9,6,3]]),
#    (([[5,1,9,11],[2,4,8,10],[13,3,6,7],[15,14,12,16]],),[[15,13,2,5],[14,3,4,1],[12,6,8,9],[16,7,10,11]]),
    (([[2,29,20,26,16,28],[12,27,9,25,13,21],[32,33,32,2,28,14],[13,14,32,27,22,26],[33,1,20,7,21,7],[4,24,1,6,32,34]],),[[4,33,13,32,12,2],[24,1,14,33,27,29],[1,20,32,32,9,20],[6,7,27,2,25,26],[32,21,22,28,13,16],[34,7,26,14,21,28]]),
    ]

for input, output in expects:
    rotate(*input)
    print("SUCCESS" if input[0] == output else "FAILURE", "actual", input[0], "expected", output)
