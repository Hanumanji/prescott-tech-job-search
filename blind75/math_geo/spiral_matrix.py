#!/usr/bin/python

import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)

def spiral_matrix(matrix : list[list[int]]) -> list[int]:
    r = []

    right = (0, 1)
    down = (1, 0)
    left = (0, -1)
    up = (-1, 0)

    h = {
        right: down,
        down: left,
        left: up,
        up: right,
    }

    s = set()
    
    def get(c):
        i, j = c
        if i < 0 or j < 0 or i >= len(matrix) or j >= len(matrix[0]) or c in s:
            return None
        
        return matrix[i][j]

    def step(p, dir):
        dj, di = dir
        j, i = p
        return j + dj, i + di
    
    p = (0,0)
    r.append(get(p))
    s.add(p)

    dir = right
        
    while True:
        n = step(p, dir)
        v = get(n)
        log(f"step p {p} dir {dir} n {n} v {v}")
        if v is None:
            # Rotate and try again...
            dir = h[dir]
            n = step(p, dir)
            v = get(n)
            log(f"change dir p {p} dir {dir} n {n} v {v}")            
            if v is None:
                break
        s.add(p)
        r.append(v)
        p = n

    return r

print(spiral_matrix([[1,2,3],[4,5,6],[7,8,9]]))
print(spiral_matrix([[1,2,3,4],[5,6,7,8],[9,10,11,12]]))
