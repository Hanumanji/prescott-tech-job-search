#!/usr/bin/python

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def level_order(root: TreeNode) -> list[list[int]]:
    levels = []
    def bfs(n, d = 0):
        if n is None:
            return
        
        if d == len(levels):
            levels.append([])
        levels[d].append(n.val)
        
        bfs(n.left, d + 1)
        bfs(n.right, d + 1)
        
    bfs(root)

    return levels

tn = TreeNode
print(level_order(tn(3,tn(9),tn(20,tn(15),tn(7)))))
