#!/usr/bin/python

class TreeNode:
    def __init__(self, val=0, left=None, right = None):
        self.val = val
        self.left = left
        self.right = right

def same_tree(p: TreeNode, q: TreeNode) -> bool:

    def st(p, q):
        if p is None and q is None:
            return True

        if p is None or q is None:
            return False
        
        return p.val == q.val and st(p.left, q.left) and st(p.right, q.right)

    return st(p, q)
