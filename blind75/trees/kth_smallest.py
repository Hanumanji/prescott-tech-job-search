#!/usr/bin/python

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def kth_smallest(root: TreeNode, k: int) -> int:
    vals = []
    def dfs(n):
        if n is None:
            return
        dfs(n.left)
        vals.append(n.val)
        if len(vals) == k:
            raise
        dfs(n.right)
    try:
        dfs(root)
    finally:
        return vals[k - 1]

tn = TreeNode
print(kth_smallest(tn(3, tn(1, None, tn(2)), tn(4)), 1))  # 1
print(kth_smallest(tn(5, tn(3, tn(2, tn(1)), tn(4)), tn(6)), 3)) # 3
print(kth_smallest(tn(3, tn(1, None, tn(2)), tn(4)), 4))  # 4

