#!/usr/bin/python

# Binary Tree Maximum Path Sum
# leetcode 124
# O(n)
# O(h)

class TreeNode:
    def __init__(self, val = 0, left = None, right = None):
        self.val = val
        self.left = left
        self.right = right

tn = TreeNode

def maxPathSum(root: TreeNode) -> int:
    """
    >>> maxPathSum(tn(1,tn(2),tn(3)))
    6
    >>> maxPathSum(tn(-10,tn(9),tn(20,tn(15),tn(7))))
    42
    """
    res = [root.val]

    # max path sum without splitting...
    def dfs(n):
        if n is None:
            return 0

        l = dfs(n.left)
        r = dfs(n.right)
        l = max(l, 0)
        r = max(r, 0)
        
        res[0] = max(res[0], n.val + l + r)
        
        return n.val + max(l, r)

    #dfs(root); return res[0]
    
    def dfs_res(n, res):
        if n is None:
            return (0, res)

        l, lres = dfs_res(n.left, res)
        r, rres = dfs_res(n.right, res)
        l = max(l, 0)
        r = max(r, 0)

        res = max(res, lres, rres, n.val + l + r)

        return n.val + max(l, r), res

    return dfs_res(root, root.val)[1]
    

if __name__ == "__main__":
    import doctest
    doctest.testmod()
