#!/usr/bin/python

class TreeNode:
    def __init__(self, val=0, left=None, right = None):
        self.val = val
        self.left = left
        self.right = right

def md(n, d = 0):
    if n is None:
        return d

    return max(md(n.left, d+1), md(n.right, d+1))
               
def maxDepth(root):
    return md(root)


tn = TreeNode
print(maxDepth(tn(3, tn(9), tn(20, tn(15), tn(7)))))
               
