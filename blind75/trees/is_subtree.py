#!/usr/bin/python

class TreeNode:
    def __init__(self, val=0, left=None, right = None):
        self.val = val
        self.left = left
        self.right = right

def isSubtree(root, subRoot):

    def same(p, q):
        if p is None and q is None:
            return True

        if p is None or q is None:
            return False
        
        return p.val == q.val and same(p.left, q.left) and same(p.right, q.right)

    def sub(r, s):
        if r is None and s is None:
            return True
        if r is None or s is None:
            return False
        
        return same(r, s) or sub(r.left, s) or sub(r.right, s)

    

    
