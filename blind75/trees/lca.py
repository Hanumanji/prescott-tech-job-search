#!/usr/bin/python

class TreeNode:
    def __init__(self, val=0, left=None, right = None):
        self.val = val
        self.left = left
        self.right = right

        
def lca(root, p, q):

    def path(n, v, p):
        if n is None:
            return None
        
        p.append(n.val)
        if n.val == v:
            return p

        cp = path(n.left, v, p)
        if cp:
            return cp

        cp = path(n.right, v, p)
        if cp:
            return cp
        p.pop()
        return None
        
    pp = path(root, p, [])
    qp = path(root, q, [])

    #print(f"pp {pp}")
    #print(f"qp {qp}")

    i = 0
    pl, ql = len(pp), len(qp)
    while True:
        if i < pl and i < ql and pp[i] == qp[i]:
            i += 1
        else:
            break

    return pp[i - 1]

tn = TreeNode
root = tn(6,tn(2,tn(0),tn(4,tn(3),tn(5))),tn(8,tn(7),tn(9)))
print(lca(root, 2, 8))

print(lca(root, 2, 4))

