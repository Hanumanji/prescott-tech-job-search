#!/usr/bin/python

import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)
        
class TreeNode:
    def __init__(self, val=0, left=None, right = None):
        self.val = val
        self.left = left
        self.right = right
    def __repr__(self):
        return f"{self.val}({self.left},{self.right})"

def buildTree(preorder: list[int], inorder: list[int]) -> TreeNode:
    root = TreeNode(preorder.pop(0))
    n = root
    for v in preorder:
        n.left = TreeNode(v)
        n = n.left

    log(f"preorder root {root}")
    
    def find(n, v):
        if n is None:
            return None
        
        if n.val == v:
            return n
        c = find(n.left, v)
        if c is not None:
            return c
        c = find(n.right, v)
        if c is not None:
            return c
        
        return None

    L,C,R = 0,1,2
    o = L
    
    t = root
    for v in inorder:
        n = find(t, v)
        log(f"n {n} find v {v} on t {t}")
        if o == L and t and n:
            t.right = n.left
            n.left = None
            o = C
        elif o == C:
            t = t.right
            # Go back to L
            o = L
        elif o == R:
            o = L
            

        
    return root

tn = TreeNode
#print(buildTree([3,9,20,15,7], [9,3,15,20,7]))
#print(buildTree([1,2],[1,2]))
#print(buildTree([1,2],[2,1]))
#print(buildTree([1,2,3],[3,2,1]))
print(buildTree([1,2,3],[2,3,1]))

