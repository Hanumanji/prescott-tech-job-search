#!/usr/bin/python

import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)
        
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def valid_bst(root: TreeNode) -> bool:

    def dfs(n, l = None, r = None):
        if n is None:
            return True
        log(f"n.val {n.val} l {l} r {r}")
        
        if l is not None and n.val <= l:
            return False
        if r is not None and n.val >= r:
            return False
            
        if n.left and n.left.val >= n.val:
            return False
        if n.right and n.right.val <= n.val:
            return False

        if not dfs(n.left, l = l, r = n.val):
            return False
        if not dfs(n.right, l = n.val, r = r):
            return False
        return True

    return dfs(root)

tn = TreeNode
print(valid_bst(tn(2, tn(1), tn(3)))) # True
print(valid_bst(tn(5, tn(1), tn(4, tn(3), tn(6))))) # False
print(valid_bst(tn(2, tn(2), tn(2)))) # False
print(valid_bst(tn(5, tn(4), tn(6, tn(3), tn(7))))) # False
