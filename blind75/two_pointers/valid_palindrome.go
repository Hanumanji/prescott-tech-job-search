package main

import (
       "regexp"
       "strings"
       "fmt"
)

func isPalindrome(s string) bool {
    res := true

    re := regexp.MustCompile("[^a-zA-Z0-9]+")

    ps := strings.ToLower(re.ReplaceAllString(s, ""))
    
    b, e := 0, len(ps) - 1
    for b <= e {
    	if ps[b] != ps[e] {
	   res = false
	   break
	   }
	   
    	b += 1
	e -= 1
    }
    return res
}

func main(){
     fmt.Println(isPalindrome("A man, a plan, a canal: Panama!")) // true
     fmt.Println(isPalindrome("0P")) // false
}