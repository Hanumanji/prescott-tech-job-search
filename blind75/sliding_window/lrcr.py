# Longest Repeating Character Replacement

# ABAB

def lrcr(s, k):
    print(s, k)
    
    h = {} # { c : [(i, n), ...] } i index, n num chars

    # ALTERNATE { c: [(i, n, ak), ...]} i index, n num chars, ak remaining k
    
    # ABAB
    # { A: [(0,1)], B: [(1,1)] }
    # { A: [(0,1),(2,1)], B:[(1,1)] }
    # { A: [(0,1),(2,1)], B:[(1,1), (3, 1)] }
    m = -1
    for i, c in enumerate(s):
        if c in h:
            fi, n = h[c][-1]
            if i == (fi + n):
                h[c][-1] = (fi, n + 1)
            else:
                h[c].append((i, 1))
        else:
            h[c] = [(i, 1)]

    print(h)
    
    for a in h.values():
        print(a)
        
        ak = k

        
        b = 0  # start index
        e = -1 # end index TBD on k
        # These let us move the window when we run out of k...

        bi, bn = a[b]
        l = bn # Calculating length        
        for j in range(1, len(a)):
            e = j
            ei, en = a[e]

            d = ei - (bi + bn)
            while d > ak:
                n = b + 1
                if n >= len(a):
                    print("END")
                    break
                
                ni, nn = a[n]
                
                nd = ni - (bi + bn)
                ak += nd

                l -= nd + nn
                b = n
                bi, bn = ni, nn


            if d <= ak:
                ak -= d
                l += d + en
                if l > m:
                    m = l
            else:
                break
            
            print(f"bi {bi}, bn {bn}, ei {ei}, en {en}, d {d}, ak {ak}, l {l}, m {m}")

        l += ak
        print(f"ak {ak} l {l}")
        if l > m:
            m = l
    
    return m


if __name__ == "__main__":
    inputs = [
        # Possible problem: A doesn't know there is a B at the end
#        ("ABAB", 2),
#        ("AABABBA", 1),
#        ("AABABBA", 2),        # Fixed
        ("ABAABBAAABBBABB", 2), # Problem?
        ]
    for input in inputs:
        print(lrcr(*input))
