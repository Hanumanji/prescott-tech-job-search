

def long_sub(s):
    seen = {} # c, i
    b = 0 # begin
    e = 0 # end
    m = 0
    for i, c in enumerate(s):
        if c in seen:
            new_b = seen[c] + 1
            if new_b > b:
                b = new_b
            del seen[c]
            if b == i:
                seen.clear()
                
        seen[c] = i
        e = i

        l = (e - b) + 1
        if l > m:
            m = l

        print(b, e, m)
        
    return m



if __name__ == "__main__":
    inputs = [
#        "abcabcbb",
#        "dvdf",
        "abba",
#        "dabcaefd",
        ]
    for input in inputs:
        print(long_sub(input))
