#!/usr/bin/python

import os

def log(*args, **kwargs):
    if "LOG" in os.environ:
        print(*args, **kwargs)


def combinationSum(candidates: list[int], target: int) -> list[list[int]]:
    """
    >>> combinationSum([2, 3, 6, 7], 7)
    [[2, 2, 3], [7]]

    >>> combinationSum([1,2], 4)
    [[1, 1, 1, 1], [1, 1, 2], [2, 2]]

    >>> combinationSum([100,200,4,12], 400)
    """

    f = set() # Found permutations
    def seek(v, m):
#        print(f"seek v {v} l {l}")
        for c in candidates:
            if not v % c:
                p = v // c
                for i in range(1, p):
                    m[c] += i
                    d = v - (c * i)
                    seek(d, m)
                    m[c] -= i
                    
                m[c] += p
                f.add(tuple(sorted(m.items())))
                m[c] -= p
            else:
                d = v - c
                if d > 0:
                    m[c] += 1
                    seek(d, m)
                    m[c] -= 1
                
    seek(target, {k:0 for k in candidates})

#    print(f"f {f}")
    
    # {2:2, 3:1, 6:0, 7:0}
    # (2,2), (3,1), (6,0), (7,0)
    
    return sorted([sum([[k] * v for k, v in p if v], []) for p in f])

if __name__ == "__main__":
    import doctest
    doctest.testmod()
