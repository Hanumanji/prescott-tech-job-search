#! /usr/bin/python

from typing import List
from dataclasses import dataclass
import traceback

# S......
# .......
# ......F
#
# S1  S = R(S) + D(S) = 1 + 1 = 2
# 1F
#
# S..  S31
# .21  321  S=3+3=6
# .1F  11F

# |S_|__|__|__|__|__|__|
# |__|__|__|__|__|__|__|
# |__|__|__|__|__|__|F_|

# |__|__|__|__|__|__|__|
# |S_|21|15|10|6_|3_|1_|
# |7_|6_|5_|4_|3_|2_|1_|  S = 21 + 7 = 28
# |1_|1_|1_|1_|1_|1_|F_|

# |S_|56|35|20|10|4_|1_|
# |28|21|15|10|6_|3_|1_|
# |7_|6_|5_|4_|3_|2_|1_|  S = 56 + 28 = 84
# |1_|1_|1_|1_|1_|1_|F_|

class Logger(object):
    scope: str
    debug: bool = False

    def __init__(self, scope):
        self.scope = scope
        
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, tb):
        if exc_type is not None:
            traceback.print_exception(exc_type, exc_value, tb)
        return

    def log(self, *args, **kwargs):
        if self.debug:
            print(*args, **kwargs)

class Solution:
    def preprocess(self, input):
        return input

    def postprocess(self, output):
        return output
    
    def getNeighbors(self, ij):
        i, j = ij
        s = set()
        if i - 1 >= 0:
            s.add((i-1,j))
        if j - 1 >= 0:
            s.add((i, j-1))
        return s

    def calculateValue(self, ij, v):
        t = 0
        i, j = ij

        r = (i + 1, j)
        d = (i, j + 1)
        if r in v:
            t += v[r]
        if d in v:
            t += v[d]
        return t if t else 1
    
    def uniquePathsOld(self, m: int, n: int) -> int:
        with Logger("uniquePaths") as logger:
            logger.log(f"m {m} n {n}")
            self.m, self.n = m, n
            
            s = (0,0)
            f = (m-1,n-1)
            v = {} # { pos : val }
            v[f] = 0
            q = [f]

            
            while q:
                ij = q.pop()
                logger.log(f"ij {ij} v {v}")
                if ij not in v:
                    v[ij] = self.calculateValue(ij, v)
                    
                ns = self.getNeighbors(ij)
                for ne in ns:
                    if ne not in v:
                        q.insert(0, ne)

            for i in range(m):
                for j in range(n):
                    logger.log(v[(i,j)], end="")
                logger.log()
        
        return v[s] if v[s] else 1

    def uniquePaths(self, m: int, n: int) -> int:
        with Logger("uniquePaths") as logger:
            logger.log(f"m {m} n {n}")
            
            s = (0,0)
            f = (m-1,n-1)
            v = { f: 0 } # { pos : val }
            q = [f]

            while True:
                tn = set()
                while q:
                    ij = q.pop()
                    logger.log(f"ij {ij} v {v}")
                    if ij not in v:
                        v[ij] = self.calculateValue(ij, v)
                    
                    ns = self.getNeighbors(ij)
                    tn.update(ns)

                if not tn:
                    break
                        
                q = tn
                
            for i in range(m):
                for j in range(n):
                    logger.log(v[(i,j)], end="")
                logger.log()
        
        return v[s] if v[s] else 1
    
    
if __name__ == "__main__":
    soln = Solution()
    fn_name = "uniquePaths"
    assert fn_name, "Set fn_name to valid Solution method"
    fn = getattr(soln, fn_name)
    assert fn, f"No Solution method attr named {fn_name}"
    assert callable(fn), f"Solution attr {fn_name} is not callable"
    expects = [
        ((3,7),28),
        ((3,2),3),
        ((1,1),1),
        ((9,9),12870),
        ((23,12),193536720),
    ]
    assert expects, "expects is empty list of (input, output) pairs, please set!"
    for input, output in expects:
        pinput = soln.preprocess(input)
        actual = fn(*pinput)
        pactual = soln.postprocess(actual)
        print("SUCCESS" if pactual == output else "FAILURE", input, "gave", pactual, "expected", output)
       
        


